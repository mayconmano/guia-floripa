<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'login', 'action' => 'index'));
	Router::connect('/home', array('controller' => 'home', 'action' => 'index'));
	Router::connect('/sair', array('controller' => 'login', 'action' => 'logout'));
	Router::connect('/recuperacao', array('controller' => 'login', 'action' => 'recuperacao'));

	Router::connect('/usuarios', array('controller' => 'users', 'action' => 'index'));
	Router::connect('/usuarios/editar/*', array('controller' => 'users', 'action' => 'edit'));
	Router::connect('/usuarios/adicionar', array('controller' => 'users', 'action' => 'add'));

	Router::connect('/pois', array('controller' => 'poies', 'action' => 'index'));
	Router::connect('/pois/editar/*', array('controller' => 'poies', 'action' => 'edit'));
	Router::connect('/pois/adicionar', array('controller' => 'poies', 'action' => 'add'));

	Router::connect('/noticias', array('controller' => 'news', 'action' => 'index'));
	Router::connect('/noticias/editar/*', array('controller' => 'news', 'action' => 'edit'));
	Router::connect('/noticias/adicionar', array('controller' => 'news', 'action' => 'add'));

	Router::connect('/agendas', array('controller' => 'agendas', 'action' => 'index'));
	Router::connect('/agendas/editar/*', array('controller' => 'agendas', 'action' => 'edit'));
	Router::connect('/agendas/adicionar', array('controller' => 'agendas', 'action' => 'add'));

	Router::connect('/top10', array('controller' => 'tops', 'action' => 'index'));
	Router::connect('/top10/editar/*', array('controller' => 'tops', 'action' => 'edit'));
	Router::connect('/top10/adicionar', array('controller' => 'tops', 'action' => 'add'));

	Router::connect('/clima', array('controller' => 'climates', 'action' => 'index'));
	Router::connect('/ssah', array('controller' => 'secures', 'action' => 'index'));
	Router::connect('/historia_cidade', array('controller' => 'citystories', 'action' => 'index'));
	Router::connect('/leis_locais', array('controller' => 'locallaws', 'action' => 'index'));
	Router::connect('/horarios', array('controller' => 'schedules', 'action' => 'index'));
	Router::connect('/fuso_horario', array('controller' => 'timezones', 'action' => 'index'));
	Router::connect('/telefonias', array('controller' => 'telephones', 'action' => 'index'));
	Router::connect('/telefones_uteis', array('controller' => 'usefulphones', 'action' => 'index'));
	Router::connect('/telefones_uteis/adicionar', array('controller' => 'usefulphones', 'action' => 'add'));
	Router::connect('/telefones_uteis/editar/*', array('controller' => 'usefulphones', 'action' => 'edit'));
	Router::connect('/operadoras', array('controller' => 'mobileoperators', 'action' => 'index'));
	Router::connect('/operadoras/adicionar', array('controller' => 'mobileoperators', 'action' => 'add'));
	Router::connect('/operadoras/editar/*', array('controller' => 'mobileoperators', 'action' => 'edit'));
	Router::connect('/tomadas', array('controller' => 'takens', 'action' => 'index'));
	Router::connect('/tipo_ligacoes', array('controller' => 'connectiontypes', 'action' => 'index'));
	Router::connect('/visto_passaporte', array('controller' => 'seenpassports', 'action' => 'index'));
	Router::connect('/hospedagem', array('controller' => 'hosts', 'action' => 'index'));
	Router::connect('/alimentacao', array('controller' => 'feeds', 'action' => 'index'));
        Router::connect('/roteiro_auto_guiado', array('controller' => 'selfguiededs', 'action' => 'index'));
	
	Router::connect('/bicicleta', array('controller' => 'bicycles', 'action' => 'index'));
	Router::connect('/ape', array('controller' => 'walks', 'action' => 'index'));
	Router::connect('/taxi', array('controller' => 'cabs', 'action' => 'index'));
	Router::connect('/transporte_hidroviario', array('controller' => 'watertransports', 'action' => 'index'));
	Router::connect('/onibus', array('controller' => 'publictransports', 'action' => 'index'));

	Router::connect('/onde-fica', array('controller' => 'whereis', 'action' => 'index'));
	Router::connect('/aviao', array('controller' => 'planes', 'action' => 'index'));
	Router::connect('/carro', array('controller' => 'cars', 'action' => 'index'));
	Router::connect('/ir-onibus', array('controller' => 'gobybues', 'action' => 'index'));
	Router::connect('/embarcacao', array('controller' => 'vessels', 'action' => 'index'));

	Router::connect('/perguntas', array('controller' => 'questions', 'action' => 'index'));
	Router::connect('/perguntas/editar/*', array('controller' => 'questions', 'action' => 'edit'));
	Router::connect('/perguntas/adicionar', array('controller' => 'questions', 'action' => 'add'));
	Router::connect('/perguntas/relatorio/*', array('controller' => 'questions', 'action' => 'relatorio'));
	
	Router::connect('/contato', array('controller' => 'contacts', 'action' => 'index'));
	Router::connect('/contato/visualizar/*', array('controller' => 'contacts', 'action' => 'edit'));

	Router::connect('/menu', array('controller' => 'menus', 'action' => 'index'));
	Router::connect('/menu/editar/*', array('controller' => 'menus', 'action' => 'edit'));
	Router::connect('/menu/adicionar', array('controller' => 'menus', 'action' => 'add'));

/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
