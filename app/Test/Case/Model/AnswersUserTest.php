<?php
App::uses('AnswersUser', 'Model');

/**
 * AnswersUser Test Case
 *
 */
class AnswersUserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.answers_user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AnswersUser = ClassRegistry::init('AnswersUser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AnswersUser);

		parent::tearDown();
	}

}
