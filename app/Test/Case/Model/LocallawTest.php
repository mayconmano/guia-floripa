<?php
App::uses('Locallaw', 'Model');

/**
 * Locallaw Test Case
 *
 */
class LocallawTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.locallaw'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Locallaw = ClassRegistry::init('Locallaw');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Locallaw);

		parent::tearDown();
	}

}
