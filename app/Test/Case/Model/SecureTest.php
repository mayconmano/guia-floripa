<?php
App::uses('Secure', 'Model');

/**
 * Secure Test Case
 *
 */
class SecureTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.secure'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Secure = ClassRegistry::init('Secure');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Secure);

		parent::tearDown();
	}

}
