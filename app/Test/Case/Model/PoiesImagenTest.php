<?php
App::uses('PoiesImagen', 'Model');

/**
 * PoiesImagen Test Case
 *
 */
class PoiesImagenTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.poies_imagen',
		'app.poie',
		'app.category'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PoiesImagen = ClassRegistry::init('PoiesImagen');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PoiesImagen);

		parent::tearDown();
	}

}
