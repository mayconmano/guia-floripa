<?php
App::uses('Publictransport', 'Model');

/**
 * Publictransport Test Case
 *
 */
class PublictransportTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.publictransport'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Publictransport = ClassRegistry::init('Publictransport');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Publictransport);

		parent::tearDown();
	}

}
