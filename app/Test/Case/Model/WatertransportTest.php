<?php
App::uses('Watertransport', 'Model');

/**
 * Watertransport Test Case
 *
 */
class WatertransportTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.watertransport'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Watertransport = ClassRegistry::init('Watertransport');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Watertransport);

		parent::tearDown();
	}

}
