<?php
App::uses('Top', 'Model');

/**
 * Top Test Case
 *
 */
class TopTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.top',
		'app.poie',
		'app.category'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Top = ClassRegistry::init('Top');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Top);

		parent::tearDown();
	}

}
