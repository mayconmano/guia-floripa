<?php
App::uses('Seenpassport', 'Model');

/**
 * Seenpassport Test Case
 *
 */
class SeenpassportTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.seenpassport'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Seenpassport = ClassRegistry::init('Seenpassport');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Seenpassport);

		parent::tearDown();
	}

}
