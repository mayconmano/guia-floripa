<?php
App::uses('Taken', 'Model');

/**
 * Taken Test Case
 *
 */
class TakenTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.taken'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Taken = ClassRegistry::init('Taken');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Taken);

		parent::tearDown();
	}

}
