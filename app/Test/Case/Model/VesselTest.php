<?php
App::uses('Vessel', 'Model');

/**
 * Vessel Test Case
 *
 */
class VesselTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.vessel'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Vessel = ClassRegistry::init('Vessel');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Vessel);

		parent::tearDown();
	}

}
