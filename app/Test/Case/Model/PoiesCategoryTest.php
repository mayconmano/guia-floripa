<?php
App::uses('PoiesCategory', 'Model');

/**
 * PoiesCategory Test Case
 *
 */
class PoiesCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.poies_category',
		'app.poie',
		'app.category'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PoiesCategory = ClassRegistry::init('PoiesCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PoiesCategory);

		parent::tearDown();
	}

}
