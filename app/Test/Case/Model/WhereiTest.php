<?php
App::uses('Wherei', 'Model');

/**
 * Wherei Test Case
 *
 */
class WhereiTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.wherei'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Wherei = ClassRegistry::init('Wherei');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Wherei);

		parent::tearDown();
	}

}
