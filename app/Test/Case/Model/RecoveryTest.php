<?php
App::uses('Recovery', 'Model');

/**
 * Recovery Test Case
 *
 */
class RecoveryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.recovery'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Recovery = ClassRegistry::init('Recovery');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Recovery);

		parent::tearDown();
	}

}
