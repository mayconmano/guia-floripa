<?php
App::uses('Bicycle', 'Model');

/**
 * Bicycle Test Case
 *
 */
class BicycleTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.bicycle'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Bicycle = ClassRegistry::init('Bicycle');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Bicycle);

		parent::tearDown();
	}

}
