<?php
App::uses('Connectiontype', 'Model');

/**
 * Connectiontype Test Case
 *
 */
class ConnectiontypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.connectiontype'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Connectiontype = ClassRegistry::init('Connectiontype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Connectiontype);

		parent::tearDown();
	}

}
