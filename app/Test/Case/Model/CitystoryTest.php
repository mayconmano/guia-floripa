<?php
App::uses('Citystory', 'Model');

/**
 * Citystory Test Case
 *
 */
class CitystoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.citystory'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Citystory = ClassRegistry::init('Citystory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Citystory);

		parent::tearDown();
	}

}
