<?php
App::uses('Timezone', 'Model');

/**
 * Timezone Test Case
 *
 */
class TimezoneTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.timezone'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Timezone = ClassRegistry::init('Timezone');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Timezone);

		parent::tearDown();
	}

}
