<?php
App::uses('Usefulphone', 'Model');

/**
 * Usefulphone Test Case
 *
 */
class UsefulphoneTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.usefulphone'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Usefulphone = ClassRegistry::init('Usefulphone');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Usefulphone);

		parent::tearDown();
	}

}
