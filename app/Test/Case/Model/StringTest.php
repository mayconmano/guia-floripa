<?php
App::uses('String', 'Model');

/**
 * String Test Case
 *
 */
class StringTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->String = ClassRegistry::init('String');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->String);

		parent::tearDown();
	}

}
