<?php
App::uses('Poie', 'Model');

/**
 * Poie Test Case
 *
 */
class PoieTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.poie',
		'app.category'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Poie = ClassRegistry::init('Poie');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Poie);

		parent::tearDown();
	}

}
