<?php
App::uses('Mobileoperator', 'Model');

/**
 * Mobileoperator Test Case
 *
 */
class MobileoperatorTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.mobileoperator'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Mobileoperator = ClassRegistry::init('Mobileoperator');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Mobileoperator);

		parent::tearDown();
	}

}
