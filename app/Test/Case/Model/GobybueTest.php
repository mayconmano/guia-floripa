<?php
App::uses('Gobybue', 'Model');

/**
 * Gobybue Test Case
 *
 */
class GobybueTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.gobybue'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Gobybue = ClassRegistry::init('Gobybue');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Gobybue);

		parent::tearDown();
	}

}
