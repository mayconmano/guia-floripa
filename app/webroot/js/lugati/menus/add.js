$(document).ready(function(){
	
	window.ParsleyValidator.setLocale('pt');

	$('#form_menu').parsley();

	tinymce.init({
    selector: "textarea",    
    width: 500,
    height: 300,        
    toolbar: "insertfile undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview fullpage | forecolor backcolor",     
  });

  $('#fileupload').fileupload({
      url: config_cidade+'/menus/uploadHandler',
      maxNumberOfFiles: 1
  });

    App.wizard();

    var item = 1;
      var notAllow = [];    
      $('#wizard1').on('change', function(e, data) {

        if(data.step == 1){
          var validate_idiomas = false;
          $('.idiomas:checked').each(function(){
            validate_idiomas = true;   
          });
          if(!validate_idiomas) e.preventDefault();
        }     

        if(notAllow.indexOf(data.step) !== -1)
        {
          if (false === $('#form_menu').parsley().validate('block' + data.step))
            e.preventDefault();  
        }    
              
        item = $('#wizard1').wizard('selectedItem').step;   
      });

      
      $('#wizard1').on('changed', function(e, data) {
        selectedItem = $('#wizard1').wizard('selectedItem');    
        if(notAllow.indexOf(selectedItem.step) == -1){
          if(item > selectedItem.step)
            $('#wizard1').wizard('previous');
          else
          {
            if(selectedItem.step != 5)
              $('#wizard1').wizard('next');
          }
        }

      });

      $('#wizard1').on('finished', function(e){    
        $('#form_menu').submit();        
      });

      $('.idiomas').on('ifChecked', function(event){
        var val = $(this).val();
        $('.steps li').each(function(i,v){        
            if($(this).attr('idioma') == val) 
            {
              $(this).removeClass('hide'); 
              switch($(this).attr('idioma'))
              {
                case 'pt':
                  notAllow.push(2);              
                break;
                case 'en':
                  notAllow.push(3);              
                break;
                case 'es':
                  notAllow.push(4);        
                break;
              }          
            }
          });  
          notAllow.sort();
      });

      $('.idiomas').on('ifUnchecked', function(event){
         var val = $(this).val();
         $('.steps li').each(function(i, v){        
            if($(this).attr('idioma') == val) 
            {
              $(this).addClass('hide'); 
              switch($(this).attr('idioma'))
              {
                case 'pt':
                  index = notAllow.indexOf(2);
                  notAllow.splice(index,1);  
                break;
                case 'en':
                  index = notAllow.indexOf(3);
                  notAllow.splice(index,1);
                break;
                case 'es':              
                  index = notAllow.indexOf(4);
                  notAllow.splice(index,1);
                break;
              }  
            }
          });
          notAllow.sort(); 
      });

      $('#colorSelector').ColorPicker({
        color: '#0000ff',
        onShow: function (colpkr) {
          $(colpkr).fadeIn(500);
          return false;
        },
        onHide: function (colpkr) {
          $(colpkr).fadeOut(500);
          return false;
        },
        onChange: function (hsb, hex, rgb) {
          $('#colorSelector div').css('backgroundColor', '#' + hex);
          $('.fa-hover i').css('color', '#' + hex);
          $('#icone_cor').val('#' + hex);
        }
      });

      $('.fa-hover').hover(function(){
        $(this).css('backgroundColor', '#8a8a8a');
      }, function(){
        $(this).css('backgroundColor', '');
      });

      $('.fa-hover').click(function(){        
        $('.fa-hover').removeClass('icon-selected');
        $(this).addClass('icon-selected');
        $('#icone').val($(this).find('i').attr('class'));
      });


 
});

function rawurlencode(str) {

  str = (str + '').toString();

  return encodeURIComponent(str)
    .replace(/!/g, '%21')
    .replace(/'/g, '%27')
    .replace(/\(/g, '%28')
    .
  replace(/\)/g, '%29')
    .replace(/\*/g, '%2A');
}