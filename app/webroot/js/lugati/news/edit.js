$(document).ready(function(){
	
	window.ParsleyValidator.setLocale('pt');

	$('form').parsley();

    tinymce.init({
        selector: "textarea",    
        width: 500,
        height: 300,        
        toolbar: "insertfile undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview fullpage | forecolor backcolor",     
    });

    $('#fileupload').fileupload({    
        url: config_cidade+'/news/uploadHandlerEdit/'+$('input[name="id"]').val(),
        maxNumberOfFiles: 1
    });
    
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

   
    $('#fileupload').addClass('fileupload-processing');

    $.ajax({
        url: $('#fileupload').fileupload('option', 'url'),
        dataType: 'json',
        context: $('#fileupload')[0]
    }).always(function () {
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
        $(this).fileupload('option', 'done')
            .call(this, $.Event('done'), {result: result});
    });

    $('textarea[name="resumo"]').keypress(function(e) {
        var tval = $('textarea[name="resumo"]').val(),
            tlength = tval.length,
            set = 140,
            remain = parseInt(set - tlength);
        $('#caracteres').text(remain);
        if (remain <= 0 && e.which !== 0 && e.charCode !== 0) {
            $('textarea[name="resumo"]').val((tval).substring(0, tlength - 1))
        }
    });

 
});

function rawurlencode(str) {

  str = (str + '').toString();

  return encodeURIComponent(str)
    .replace(/!/g, '%21')
    .replace(/'/g, '%27')
    .replace(/\(/g, '%28')
    .replace(/\)/g, '%29')
    .replace(/\*/g, '%2A');
}