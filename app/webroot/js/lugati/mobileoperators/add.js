$(document).ready(function(){

  $('#fileupload').fileupload({
      url: config_cidade+'/mobileoperators/uploadHandler',
      maxNumberOfFiles: 1
  });

});

function rawurlencode(str) {

  str = (str + '').toString();

  return encodeURIComponent(str)
    .replace(/!/g, '%21')
    .replace(/'/g, '%27')
    .replace(/\(/g, '%28')
    .
  replace(/\)/g, '%29')
    .replace(/\*/g, '%2A');
}