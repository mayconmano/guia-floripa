var functions = $('<div class="btn-group"><button class="btn btn-default btn-md" type="button">Ações</button><button data-toggle="dropdown" class="btn btn-md btn-primary dropdown-toggle" type="button"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul role="menu" class="dropdown-menu pull-right"><li><a href="" class="editar">Editar</a></li><li><a href="" class="respostas">Resposta(s)</a></li><li><a href="" class="deletar">Deletar</a></li></ul></div>');
$("#datatable tbody tr td:last-child").each(function(){
  $(this).html("");
  functions.clone().appendTo(this);
});


$(document).ready(function(){
  
  $('#datatable').dataTable();
  $('.dataTables_length select').addClass('form-control'); 

  $( "#datatable tbody" ).sortable({
    update: function( event, ui ) { 
      ids = [];
      $('#datatable tbody tr').each(function(i, v){        
          ids.push($(this).attr('id'));
      });

      $.ajax({
        url: 'http://'+window.location.host+config_cidade+'/questions/order',
        type: 'post',
        dataType: 'json',
        data: {id: JSON.stringify(ids)},
        success: function(data){
          var html = '';
          $.each(data, function(i, v){
            if(i % 2 == 0) $class = "old"; else $class = "even";
            html += '<tr id="'+v['Question']['id']+'" class="'+$class+'">';
            html += '<td>'+v['Question']['ordem']+'</td>';
            html += '<td>'+v['Question']['pergunta_pt']+'</td>';
            html += '<td>'+(v['Question']['status'] == 1 ? '<span class="label label-success inativo" style="cursor: pointer;">Ativo</span>' : '<span class="label label-default ativo" style="cursor: pointer;">Inativo</span>')+'</td>';
            html += '<td><div class="btn-group"><button class="btn btn-default btn-md" type="button">Ações</button><button data-toggle="dropdown" class="btn btn-md btn-primary dropdown-toggle" type="button"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul role="menu" class="dropdown-menu pull-right"><li><a href="" class="editar">Editar</a></li><li><a href="" class="respostas">Resposta(s)</a></li><li><a href="" class="deletar">Deletar</a></li></ul></div></td>';
            html += '</tr>';
          });         
          $('table tbody').html(html);          
        }
      });
    }
  });

  $(document).on('click', '.editar', function(e){
    e.preventDefault();
    var id = $(this).parents().eq(4).attr('id');
    window.location = 'http://'+window.location.host+config_cidade+'/perguntas/editar/'+id;
  });

  $(document).on('click', '.deletar', function(e){
    e.preventDefault();
    var id = $(this).parents().eq(4).attr('id');
    window.location = 'http://'+window.location.host+config_cidade+'/questions/delete/'+id;
  });  

  $(document).on('click', '.ativo', function(e){
    e.preventDefault();
    var id = $(this).parent().parent().attr('id');
    var elemt = $(this);
    $.ajax({
      url: 'http://'+window.location.host+config_cidade+'/questions/status/'+id+'/1',
      dataType: 'json',
      success: function(data){
        elemt.removeClass('label-default ativo').addClass('label-success inativo').html('Ativo');
        $.gritter.add({
          title: data.title,
          text: data.text,
          class_name: data.class_name
        });
      }
    });
  });  

  $(document).on('click', '.inativo', function(e){
    e.preventDefault();
    var id = $(this).parent().parent().attr('id');
    var elemt = $(this);
    $.ajax({
      url: 'http://'+window.location.host+config_cidade+'/questions/status/'+id+'/0',
      dataType: 'json',
      success: function(data){  
        elemt.removeClass('label-success inativo').addClass('label-default ativo').html('Inativo');      
        $.gritter.add({
          title: data.title,
          text: data.text,
          class_name: data.class_name
        });
      }
    });
  });

  $(document).on('click', '.respostas', function(e){
    e.preventDefault();
    var id = $(this).parents().eq(4).attr('id');
    window.location = 'http://'+window.location.host+config_cidade+'/perguntas/relatorio/'+id;
  });

});