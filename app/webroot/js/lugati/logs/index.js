$(document).ready(function(){
  
  	$('#datatable').dataTable({
  		"aaSorting": [[ 3, "desc" ]]
	});

  	$('.dataTables_length select').addClass('form-control');  

  	var opts = {
	  lines: 13, // The number of lines to draw
	  length: 20, // The length of each line
	  width: 10, // The line thickness
	  radius: 30, // The radius of the inner circle
	  corners: 1, // Corner roundness (0..1)
	  rotate: 0, // The rotation offset
	  direction: 1, // 1: clockwise, -1: counterclockwise
	  color: '#4D90FD', // #rgb or #rrggbb or array of colors
	  speed: 1, // Rounds per second
	  trail: 60, // Afterglow percentage
	  shadow: true, // Whether to render a shadow
	  hwaccel: false, // Whether to use hardware acceleration
	  className: 'spinner', // The CSS class to assign to the spinner
	  zIndex: 2e9, // The z-index (defaults to 2000000000)
	  top: '50%', // Top position relative to parent
	  left: '50%' // Left position relative to parent
	};
 

	$('.log_button').click(function(e){
		e.preventDefault();
		var tipo = $(this).attr('id');

		$('.log_button').attr('disabled', false);
		$(this).attr('disabled', true);  

		var thead = '';
		var tbody = '';

		if(tipo == 'log_dados'){
			$.ajax({
				url: 'http://'+window.location.host+config_cidade+'/logs/getLogDados',				
          		dataType: "json",
				success: function(data){

					thead +=	'<th>Usuário</th>';
					thead +=	'<th>Tabela</th>';
					thead +=	'<th>Titulo</th>';						
					thead +=	'<th>Data</th>';
					thead +=	'<th>Status</th>';

					$.each(data, function(i, v){						
						if(i % 2 == 0) estilo = "old"; else estilo = "even";
						tbody += '<tr class="'+estilo+'">'; 
						tbody += '<td>'+v.usuario+'</td>'; 
						tbody += '<td>'+v.tabelaPt+'</td>'; 
						tbody += '<td>'+v.titulo+'</td>'; 
						tbody += '<td>'+v.data+'</td>'; 
						if(v.status == 1){
							tbody += '<td>Inserido</td>';	
						}else if(v.status == 2){
							tbody += '<td>Alterado</td>';
						}else{
							if(v.restaurado == 1){
								tbody += '<td>Excluido <span class="label label-success inativo">Restaurado</span> </td>';
							}else{
								tbody += '<td>Excluido <a href="'+config_cidade+'/logs/restaurar/'+v.tabelaEn+'/'+v.tabelaId+'/'+v.logId+'">Restaurar</a> </td>';
							}
						}						
						tbody += '</tr>'; 
					});	

					$('.table-responsive').html(
					'<table class="table table-bordered" id="datatable">'+
						'<thead>'+
							'<tr>'+thead+'</tr>'+
						'</thead>'+
						'<tbody>'+tbody+'</tbody>'+
					'</table>');

					$('#datatable').dataTable({
				  		"aaSorting": [[ 3, "desc" ]]
					});

					$('.dataTables_length select').addClass('form-control');
				}
			});			
		}else{
			$.ajax({
				url: 'http://'+window.location.host+config_cidade+'/logs/getLogArquivos',				
          		dataType: "json",
				success: function(data){

					thead +=	'<th>Arquivo</th>';
					thead +=	'<th>Usuário</th>';
					thead +=	'<th>Tabela</th>';
					thead +=	'<th>Titulo</th>';						
					thead +=	'<th>Data</th>';
					thead +=	'<th>Status</th>';

					$.each(data, function(i, v){
						console.log(v.file_path);
						if(i % 2 == 0) estilo = "old"; else estilo = "even";
						tbody += '<tr class="'+estilo+'">'; 
						tbody += '<td><img width="100" src='+v.file_path+'></td>'; 						
						tbody += '<td>'+v.usuario+'</td>'; 
						tbody += '<td>'+v.tabelaPt+'</td>'; 
						tbody += '<td>'+v.titulo+'</td>'; 
						tbody += '<td>'+v.data+'</td>'; 
						if(v.status == 1){
							tbody += '<td>Inserido</td>';	
						}else if(v.status == 2){
							tbody += '<td>Alterado</td>';
						}else{
							if(v.restaurado == 1){
								tbody += '<td>Excluido <span class="label label-success inativo">Restaurado</span> </td>';
							}else{
								tbody += '<td>Excluido <a href="'+config_cidade+'/logs/restaurarArquivo/'+v.tabelaEn+'/'+v.tabelaId+'/'+encodeURIComponent(v.file_name)+'/'+v.logId+'">Restaurar</a> </td>';
							}
						}						
						tbody += '</tr>';  
					});	

					$('.table-responsive').html(
					'<table class="table table-bordered" id="datatable">'+
						'<thead>'+
							'<tr>'+thead+'</tr>'+
						'</thead>'+
						'<tbody>'+tbody+'</tbody>'+
					'</table>');

					$('#datatable').dataTable({
				  		"aaSorting": [[ 4, "desc" ]]
					});

					$('.dataTables_length select').addClass('form-control');
				}
			});	
		}				

		
	
		//var spinner = new Spinner(opts).spin(document.getElementById('loading'));
	});

});