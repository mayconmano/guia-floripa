$(document).ready(function(){

	$('#pre-selected-options').multiSelect({
      selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Procurar'>",
      selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Procurar'>",
      afterInit: function(ms){
        var that = this,
            $selectableSearch = that.$selectableUl.prev(),
            $selectionSearch = that.$selectionUl.prev(),
            selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
            selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

        that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
        .on('keydown', function(e){
          if (e.which === 40){
            that.$selectableUl.focus();
            return false;
          }
        });

        that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
        .on('keydown', function(e){
          if (e.which == 40){
            that.$selectionUl.focus();
            return false;
          }
        });        
      },
      afterSelect: function(){
        this.qs1.cache();
        this.qs2.cache();
      },
      afterDeselect: function(){
        this.qs1.cache();
        this.qs2.cache();
      }      
    });
	$('#ms-pre-selected-options').css('width', '100%');

	var adjustment

	$("ul.ms-list").sortable({
	  group: 'simple_with_animation',
	  pullPlaceholder: false,
	  // animation on drop
	  onDrop: function  (item, targetContainer, _super) {
	    var clonedItem = $('<li/>').css({height: 0})
	    item.before(clonedItem)
	    clonedItem.animate({'height': item.height()})
	    
	    item.animate(clonedItem.position(), function  () {
	      clonedItem.detach()
	      _super(item)
	    })
	  },

	  // set item relative to cursor position
	  onDragStart: function ($item, container, _super) {
	    var offset = $item.offset(),
	    pointer = container.rootGroup.pointer

	    adjustment = {
	      left: pointer.left - offset.left,
	      top: pointer.top - offset.top
	    }

	    _super($item, container)
	  },
	  onDrag: function ($item, position) {
	    $item.css({
	      left: position.left - adjustment.left,
	      top: position.top - adjustment.top
	    })
	  }
	});

	$('form').submit(function(e){
		e.preventDefault();

		var top10 = new Array();

		$('#pre-selected-options option:selected').each(function(i, v){	
			top10.push({'poie_id': $(this).val()});			
		});
		
		dados = JSON.stringify(top10);		

		$.ajax({
			url: 'http://'+window.location.host+config_cidade+'/top10/adicionar',
			data: {dados:dados},
			type: 'POST',
			dataType: 'json',
			success: function(data){
				if(data == 'sucesso')
				{					
			    	$.gritter.add({
			        	title: 'Sucesso',
			        	text: 'Top 10 alterado.',
			        	class_name: 'success'
			      	});				    
				}
				else
				{
					$.gritter.add({
				        title: 'Erro',
				        text: 'Não foi possivel alterar o Top 10, por favor tente mais tarde.',
				        class_name: 'danger'
				      });
				}
			}
		});

	});

});