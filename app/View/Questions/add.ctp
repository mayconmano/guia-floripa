<div class="cl-mcont">    
  <div class="row wizard-row">
    <div class="col-md-12 fuelux">
      <div class="block-wizard">
        <div id="wizard1" class="wizard wizard-ux">
          <ul class="steps">
            <li data-target="#step1" class="active">Idioma(s)<span class="chevron"></span></li>
            <li data-target="#step2" class="hide" idioma="pt">Português<span class="chevron"></span></li>
            <li data-target="#step3" class="hide" idioma="en">Inglês<span class="chevron"></span></li>
            <li data-target="#step4" class="hide" idioma="es">Espanhol<span class="chevron"></span></li>            
            <li data-target="#step5">Informações<span class="chevron"></span></li>
          </ul>
          <div class="actions">
            <button type="button" class="btn btn-xs btn-prev btn-default"> <i class="icon-arrow-left"></i>Anterior</button>
            <button type="button" class="btn btn-xs btn-next btn-default" data-last="Concluir">Próximo<i class="icon-arrow-right"></i></button>
          </div>
        </div>
        <div class="step-content">
          <form id="form_question" class="form-horizontal group-border-dashed" action="" method="post" > 
            
            <div class="step-pane block1 active" id="step1">
              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Escolha o(s) idioma(s)</h3>
                </div>
              </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">Idiomas</label>
                <div class="col-sm-6">
                  <label class="checkbox-inline"> <input type="checkbox" name="idioma_pt" value="pt" class="icheck idiomas"> Português</label> 
                  <label class="checkbox-inline"> <input type="checkbox" name="idioma_en" value="en" class="icheck idiomas"> Inglês</label>
                  <label class="checkbox-inline"> <input type="checkbox" name="idioma_es" value="es" class="icheck idiomas"> Espanhol</label>
                </div>
              </div> 

               <div class="form-group">
                <label class="col-sm-3 control-label">Tipo de Resposta</label>
                <div class="col-sm-6">
                  <div class="radio">                
                   <label class="radio-inline"> <input type="radio" name="tipo_resposta" value="1" class="icheck" checked> Uma resposta</label>
                   <label class="radio-inline"> <input type="radio" name="tipo_resposta" value="2" class="icheck"> Uma ou mais resposta(s)</label> 
                  </div>                    
                </div>
              </div>

              
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">                  
                  <button data-wizard="#wizard1" class="btn btn-primary wizard-next" data-current-block="1" data-next-block="2">Próximo <i class="fa fa-caret-right"></i></button>
                </div>
              </div>                  
            </div>

            <div class="step-pane block2" id="step2">

              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Português</h3>
                </div>
              </div>              

              <div class="form-group">
                <label class="col-sm-3 control-label">Pergunta</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="pergunta_pt" style="height: 180px;" data-parsley-group="block2" ></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Resposta(s)</label>
                <div id="respostas" class="col-sm-6">             
                  <input type="text" name="resposta_pt[]" class="form-control" placeholder="Resposta" />                            
                </div>
                <button id="pt" type="button" class="btn btn-success mais_resposta">Mais+</button>  
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                  <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Anterior</button>
                  <button data-wizard="#wizard1" class="btn btn-primary wizard-next" >Próximo <i class="fa fa-caret-right"></i></button>
                </div>
              </div>  

            </div>

            <div class="step-pane block2" id="step3">
              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Inglês</h3>
                </div>
              </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">Pergunta</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="pergunta_en" style="height: 180px;" data-parsley-group="block2" ></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Resposta(s)</label>
                <div id="respostas" class="col-sm-6">             
                  <input type="text" name="resposta_en[]" class="form-control" placeholder="Resposta"  />                            
                </div>
                <button id="en" type="button" class="btn btn-success mais_resposta">Mais+</button>  
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                  <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Anterior</button>
                  <button data-wizard="#wizard1" class="btn btn-primary wizard-next">Próximo <i class="fa fa-caret-right"></i></button>
                </div>
              </div>  
            </div>

            <div class="step-pane block4" id="step4">
              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Espanhol</h3>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Pergunta</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="pergunta_es" style="height: 180px;" data-parsley-group="block2" ></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Resposta(s)</label>
                <div id="respostas" class="col-sm-6">             
                  <input type="text" name="resposta_es[]" class="form-control" placeholder="Resposta"  />                            
                </div>
                <button id="es" type="button" class="btn btn-success mais_resposta">Mais+</button>  
              </div>
              
              
              <div class="form-group">
                <div class="col-sm-12">
                  <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Anterior</button>
                  <button data-wizard="#wizard1" class="btn btn-primary wizard-next">Próximo <i class="fa fa-caret-right"></i></button>
                </div>
              </div>  
            </div>

            <div class="step-pane" id="step5">
              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Informações</h3>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Situação</label>
                <div class="col-sm-6">
                  <div class="radio">                
                   <label class="radio-inline"> <input type="radio" name="status" class="icheck" value="1" checked> Ativo</label>
                   <label class="radio-inline"> <input type="radio" name="status" class="icheck" value="0"> Inativo</label> 
                  </div>                    
                </div>
              </div> 
              
              <div class="form-group">
                <div class="col-sm-12">
                  <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Anterior</button>
                  <button data-wizard="#wizard1" class="btn btn-success wizard-next"><i class="fa fa-check"></i> Concluir</button>
                </div>
              </div> 
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>