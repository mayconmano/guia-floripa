<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3>Perguntas</h3>
			</div>
			<div class="content">
				<div class="table-responsive">
					<table class="table table-bordered" id="datatable" >
						<thead>
							<tr>								
								<th>Ordem</th>								
								<th>Pergunta</th>								
								<th>Situação</th>
								<th>Opções</th>
							</tr>
						</thead>
						<tbody>
							<?php						
								foreach ($perguntas as $key => $pergunta) {
									if($key % 2 == 0) $class = "old"; else $class = "even";
									echo '<tr id="'.$pergunta['Question']['id'].'" class="'.$class.'">';
										echo '<td>'.$pergunta['Question']['ordem'].'</td>';
										echo '<td>'.$pergunta['Question']['pergunta_pt'].'</td>';
										echo '<td>'.($pergunta['Question']['status'] == 1 ? '<span class="label label-success inativo" style="cursor: pointer;">Ativo</span>' : '<span class="label label-default ativo" style="cursor: pointer;">Inativo</span>').'</td>';
										echo '<td></td>';
									echo '</tr>';								
								}		
							?>											
						</tbody>
					</table>							
				</div>
			</div>
		</div>				
	</div>
</div>