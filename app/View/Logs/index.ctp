<div class="row"> 
  <div class="col-md-12">
  
    <div class="block-flat">
      <div class="header">							
        <h3>Logs</h3>
        <button class="btn btn-primary log_button" id="log_dados" disabled="">Log de dados</button>
        <button class="btn btn-primary log_button" id="log_arquivos">Log de arquivos</button>
      </div>
      <div class="content">
		<div class="table-responsive">
			<table class="table table-bordered" id="datatable" >
				<thead>
					<tr>														
						<th>Usuário</th>
						<th>Tabela</th>
						<th>Titulo</th>						
						<th>Data</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<?php											
						foreach ($logs as $key => $log) {							
							if($key % 2 == 0) $class = "old"; else $class = "even";
							echo '<tr class="'.$class.'">';
								echo '<td>'.$log['usuario'].'</td>';
								echo '<td>'.$log['tabelaPt'].'</td>';
								echo '<td>'.$log['titulo'].'</td>';
								echo '<td>'.$log['data'].'</td>';
								if($log['status'] == 1){
									echo '<td>Inserido</td>';	
								}else if($log['status'] == 2){
									echo '<td>Alterado</td>';
								}else{
									if($log['restaurado'] == 1){
										echo '<td>Excluido <span class="label label-success inativo">Restaurado</span> </td>';
									}else{
										echo '<td>Excluido <a href="'.$config_cidade.'/logs/restaurar/'.$log['tabelaEn'].'/'.$log['tabelaId'].'/'.$log['logId'].'">Restaurar</a> </td>';
									}
								}								
							echo '</tr>';
						}		
					?>											
				</tbody>
			</table>							
		</div>
      </div>
    </div>
    
  </div>
</div>