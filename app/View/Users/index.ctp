<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3>Usuários</h3>
			</div>
			<div class="content">
				<div class="table-responsive">
					<table class="table table-bordered" id="datatable" >
						<thead>
							<tr>								
								<th>Nome</th>
								<th>E-mail</th>
								<th>Situação</th>
								<th>Opções</th>
							</tr>
						</thead>
						<tbody>
							<?php							
								foreach ($users as $key => $user) {
									if($key % 2 == 0) $class = "old"; else $class = "even";
									echo '<tr id="'.$user['User']['id'].'" class="'.$class.'">';
										echo '<td>'.$user['User']['nome'].'</td>';
										echo '<td>'.$user['User']['email'].'</td>';
										echo '<td>'.($user['User']['status'] == 1 ? '<span class="label label-success inativo" style="cursor: pointer;">Ativo</span>' : '<span class="label label-default ativo" style="cursor: pointer;">Inativo</span>').'</td>';
										echo '<td></td>';
									echo '</tr>';								
								}		
							?>											
						</tbody>
					</table>							
				</div>
			</div>
		</div>				
	</div>
</div>