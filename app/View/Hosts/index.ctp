<div class="row">
  <div class="col-md-12">
  
    <div class="block-flat">
      <div class="header">							
        <h3>Hospedagem</h3>
      </div>
      <div class="content">
        <form class="form-horizontal group-border-dashed" action="" method="post">
        
        	<input type="hidden" name="id" value="<?php echo $hosts['Host']['id'] ?>">
	        <div class="form-group">	
				    <label class="col-sm-3 control-label">Link</label>
			      <div class="col-sm-6">
              <input type="text" name="link" class="form-control" placeholder="Link" value="<?php echo $hosts['Host']['link'] ?>">
            </div>
		      </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Resumo</label>
            <div class="col-sm-6">
              <textarea class="form-control" name="resumo"><?php echo $hosts['Host']['resumo'] ?></textarea>
            </div>            
          </div> 

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary">Salvar</button>                  
            </div>
          </div>
        </form>
      </div>
    </div>
    
  </div>
</div>