<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3>Agenda</h3>
			</div>
			<div class="content">
				<div class="table-responsive">
					<table class="table table-bordered" id="datatable" >
						<thead>
							<tr>								
								<th>Titulo</th>								
								<th>Data de Publicação</th>								
								<th>Situação</th>
								<th>Opções</th>
							</tr>
						</thead>
						<tbody>
							<?php							
								foreach ($agendas as $key => $agenda) {
									if($key % 2 == 0) $class = "old"; else $class = "even";
									echo '<tr id="'.$agenda['Agenda']['id'].'" class="'.$class.'">';
										echo '<td>'.$agenda['Agenda']['titulo'].'</td>';
										echo '<td>'.$this->Time->format($agenda['Agenda']['data_publicacao'], '%d/%m/%Y').'</td>';
										echo '<td>'.($agenda['Agenda']['status'] == 1 ? '<span class="label label-success inativo" style="cursor: pointer;">Ativo</span>' : '<span class="label label-default ativo" style="cursor: pointer;">Inativo</span>').'</td>';
										echo '<td></td>';
									echo '</tr>';								
								}		
							?>											
						</tbody>
					</table>							
				</div>
			</div>
		</div>				
	</div>
</div>