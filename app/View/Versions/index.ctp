<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3>Versões</h3>
			</div>
			<div class="content">
				<div class="table-responsive">
					<table class="table table-bordered" id="datatable" >
						<thead>
							<tr>														
								<th>Versão</th>		
								<th>Data</th>																
								<th>Remover</th>																
							</tr>
						</thead>
						<tbody>
							<?php							
								foreach ($versions as $key => $version) {
									if($key % 2 == 0) $class = "old"; else $class = "even";
									echo '<tr id="'.$version['Version']['id'].'" class="'.$class.'">';		
										echo '<td>'.$version['Version']['versao'].'</td>';
										echo '<td>'.$this->Time->format($version['Version']['data'], '%d/%m/%Y').'</td>';									
										echo '<td></td>';									
									echo '</tr>';								
								}		
							?>											
						</tbody>
					</table>							
				</div>
				<a href="<?php echo $config_cidade; ?>/versions/add"><button class="btn btn-primary">Adicionar</button></a>
			</div>
		</div>				
	</div>
</div>