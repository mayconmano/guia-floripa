<div class="row">
  <div class="col-md-12">
  
    <div class="block-flat">
      <div class="header">							
        <h3>Cadastrar Notícias</h3>
      </div>
      <div class="content">
        <form class="form-horizontal group-border-dashed" action="" method="post">

          <div class="form-group">
            <label class="col-sm-3 control-label">Titulo</label>
            <div class="col-sm-6">
              <input type="text" name="titulo" class="form-control" required placeholder="Titulo" />
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label">Resumo</label>
            <div class="col-sm-6">
              <textarea class="form-control" name="resumo" data-parsley-maxlength="140" required></textarea>
              <p>Limite: <span id="caracteres">140</span> caracteres</p>
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label">Descrição</label>
            <div class="col-sm-6">
              <textarea class="form-control editor_html" name="descricao" style="height: 180px;" data-parsley-group="block2" ></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Hiperlink</label>
            <div class="col-sm-6">
              <input type="text" name="link" class="form-control" placeholder="Hiperlink"  required />
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <div class="col-sm-6">
              <button type="button" class="btn btn-primary btn-flat md-trigger" data-modal="gerenciador-imagem">Gerenciador de imagens</button>    
              <ul id="lista-imagens" style="display: none;">                  
              </ul>    
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label"> Data de Publicação </label>
            <div class="col-sm-6">
              <div class="input-group date datetime col-md-5 col-xs-7" data-min-view="2" data-date-format="dd/mm/yyyy">
                <input class="form-control" name="data_publicacao" size="16" type="text" value="" required readonly>
                <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
              </div>          
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label"> Data de Expiração </label>
            <div class="col-sm-6">
              <div class="input-group date datetime col-md-5 col-xs-7" data-min-view="2" data-date-format="dd/mm/yyyy">
                <input class="form-control" name="data_expiracao" size="16" type="text" value="" required readonly>
                <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
              </div>          
            </div>
          </div>                   

          <div class="form-group">
            <label class="col-sm-3 control-label">Situação</label>
            <div class="col-sm-6">
              <div class="radio">                
               <label class="radio-inline"> <input type="radio" name="status" class="icheck" value="1" checked> Ativo</label>
               <label class="radio-inline"> <input type="radio" name="status" class="icheck" value="0"> Inativo</label> 
              </div>                    
            </div>
          </div> 

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary">Cadastrar</button>                  
            </div>
          </div>
        </form>
      </div>
    </div>
    
  </div>
</div>

<div class="md-modal colored-header custom-width md-effect-9" id="gerenciador-imagem">
    <div class="md-content">
      <div class="modal-header">
        <h3>Gerenciador de imagens</h3>
        <button type="button" class="close md-close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body form">
       
        
        <form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
            
            <noscript><input type="hidden" name="redirect" value="http://blueimp.github.io/jQuery-File-Upload/"></noscript>
           
            <div class="row fileupload-buttonbar">
                <div class="col-lg-10">
                   
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Adicionar Imagens</span>
                        <input type="file" name="files">
                    </span>
                    <button type="submit" class="btn btn-primary start">
                        <i class="glyphicon glyphicon-upload"></i>
                        <span>Start upload</span>
                    </button>
                    <button type="reset" class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancelar upload</span>
                    </button>
                    <button type="button" class="btn btn-danger delete">
                        <i class="glyphicon glyphicon-trash"></i>
                        <span>Excluir</span>
                    </button>
                    <input type="checkbox" class="toggle">
                    
                    <span class="fileupload-process"></span>
                </div>
                
                <div class="col-lg-5 fileupload-progress fade">
                    
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                    </div>
                    
                    <div class="progress-extended">&nbsp;</div>
                </div>
            </div>
            
            <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
        </form> 
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat md-close" data-dismiss="modal">Fechar</button>
      </div>
    </div>
</div>

<div class="md-overlay"></div>

<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processando...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancelar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>

<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) {
    $('#lista-imagens').append('<li> <input type="text" name="imagem[]" value="'+file.name+'"> </li>');
 %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>Ampliar imagem</a>
                {% } else { %}
                    <span>Ampliar imagem</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Erro</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
            {% link_delete = rawurlencode(file.name); %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=config_cidade%}/news/uploadHandler/?file={%=link_delete%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Excluir</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancelar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>