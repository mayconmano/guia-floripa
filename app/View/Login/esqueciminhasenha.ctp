<div id="cl-wrapper" class="forgotpassword-container">

	<div class="middle">
		<div class="block-flat">
			<div class="header">							
				<h3 class="text-center">
					<?php echo $this->Html->image('vivendo_floripa_pt_interna.png', array('alt' => 'loho', 'class' => 'logo-img')); ?>
				Guia Floripa</h3>
			</div>
			<div>
				<div id="foo"></div>
				<form id="recuperaSenha" style="margin-bottom: 0px !important;" class="form-horizontal" data-parsley-validate novalidate >
					<div class="content">
						<h5 class="title text-center"><strong>Esqueceu de sua senha?</strong></h5>
            <p class="text-center">Não se preocupe, nós enviaremos um e-mail para restaurar sua senha.</p>
              <hr/>              
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
										<input type="email" name="email" parsley-trigger="change" data-parsley-errors-container=".mensagemErro" required placeholder="Seu E-mail" class="form-control">
									</div>
                  					<div class="mensagemErro"></div>
								</div>
							</div>
             <!-- <p class="spacer text-center">Don't remember your email? <a href="#">Contact Support</a>.</p> -->
            <button class="btn btn-block btn-primary btn-rad btn-lg" type="submit">Restaurar Senha</button>
            <a href="/" class="btn btn-block btn-default btn-rad btn-lg">Login</a>							
					</div>
			  </form>
			</div>
		</div>
		<div class="text-center out-links"><a href="#">Desenvolvimento LUGATI. Direitos reservados.</a></div>
	</div> 
	
</div>



<div id="sucesso" class="modal fade" id="mod-info" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		</div>
		<div class="modal-body">
			<div class="text-center">
				<div class="i-circle primary"><i class="fa fa-check"></i></div>
				<h4>Sucesso!</h4>
				<p>Um e-mail foi enviado para você!</p>
			</div>
		</div>
		<div class="modal-footer">	  
		  <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
		</div>
	  </div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

<div id="erro" class="modal fade" id="mod-error" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <div class="modal-body">
      <div class="text-center">
        <div class="i-circle danger"><i class="fa fa-times"></i></div>
        <h4>Erro!</h4>
        <p></p>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
    </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>