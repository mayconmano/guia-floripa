<div id="cl-wrapper" class="forgotpassword-container">

	<div class="middle">
		<div class="block-flat">
			<div class="header">							
				<h3 class="text-center">
					<?php echo $this->Html->image('vivendo_floripa_pt_interna.png', array('alt' => 'loho', 'class' => 'logo-img')); ?>
				Guia Floripa</h3>
			</div>
			<div>
				<?php if($sucesso){ ?>
				<div id="foo"></div>
				<form id="alterarSenha" style="margin-bottom: 0px !important;" class="form-horizontal" data-parsley-validate novalidate >					
					<div class="content">
						<h5 class="title text-center"><strong>Alterar a senha!</strong></h5>     <hr/>              
							<input type="hidden" name="email" value="<?php echo $_GET['email'] ?>">
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<input type="password" name="senha" parsley-trigger="change" required placeholder="Nova Senha" class="form-control" id="senha">
									</div>                  					
								</div>
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<input type="password" name="repita_senha" parsley-trigger="change" required placeholder="Repita a nova senha" class="form-control" data-parsley-equalto="#senha">
									</div>                  					
								</div>
							</div>
             
            				<button class="btn btn-block btn-primary btn-rad btn-lg" type="submit">Alterar</button>            				
					</div>
			  </form>
			  <?php }else{ ?>
			  		<div class="content">
						<h5 class="title text-center"><strong>Alterar a senha!</strong></h5>     
						<hr/>						
						<p class="text-center">Não é possível alterar a senha.</p>					
						<a href="/" class="btn btn-block btn-primary btn-rad btn-lg">Login</a>	
					</div>					
				<?php } ?>
			</div>
		</div>
		<div class="text-center out-links"><a href="#">Desenvolvimento LUGATI. Direitos reservados.</a></div>
	</div> 
	
</div>


<div id="sucesso" class="modal fade" id="mod-info" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		</div>
		<div class="modal-body">
			<div class="text-center">
				<div class="i-circle primary"><i class="fa fa-check"></i></div>
				<h4>Sucesso!</h4>
				<p>Sua senha foi alterada!</p>
			</div>
		</div>
		<div class="modal-footer">	  
		  <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
		</div>
	  </div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

<div id="erro" class="modal fade" id="mod-error" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <div class="modal-body">
      <div class="text-center">
        <div class="i-circle danger"><i class="fa fa-times"></i></div>
        <h4>Erro!</h4>
        <p></p>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
    </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>