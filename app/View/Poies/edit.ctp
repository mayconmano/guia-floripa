<div class="cl-mcont">    
  <div class="row wizard-row">
    <div class="col-md-12 fuelux">
      <div class="block-wizard">
        <div id="wizard1" class="wizard wizard-ux">
          <ul class="steps">
            <li data-target="#step1" class="active">Idioma(s)<span class="chevron"></span></li>
            <li data-target="#step2" class="<?php echo ($poie['Poie']['idioma_pt'] != 'pt' ? 'hide' : ''); ?>" idioma="pt">Português<span class="chevron"></span></li>
            <li data-target="#step3" class="<?php echo ($poie['Poie']['idioma_en'] != 'en' ? 'hide' : ''); ?>" idioma="en">Inglês<span class="chevron"></span></li>
            <li data-target="#step4" class="<?php echo ($poie['Poie']['idioma_es'] != 'es' ? 'hide' : ''); ?>" idioma="es">Espanhol<span class="chevron"></span></li>                        
            <li data-target="#step5">Informações<span class="chevron"></span></li>
          </ul>
          <div class="actions">
            <button type="button" class="btn btn-xs btn-prev btn-default"> <i class="icon-arrow-left"></i>Anterior</button>
            <button type="button" class="btn btn-xs btn-next btn-default" data-last="Concluir">Próximo<i class="icon-arrow-right"></i></button>
          </div>
        </div>
        <div class="step-content">
          <form id="form_poi" class="form-horizontal group-border-dashed" action="" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate novalidate> 
            <input type="hidden" name="id" value="<?php echo $poie['Poie']['id']; ?>">
            <div class="step-pane block1 active" id="step1">
              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Escolha o(s) idioma(s)</h3>
                </div>
              </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">Idiomas</label>
                <div class="col-sm-6">
                  <label class="checkbox-inline"> <input type="checkbox" name="idioma_pt" value="pt" class="icheck idiomas" <?php echo ($poie['Poie']['idioma_pt'] == 'pt' ? 'checked' : ''); ?> > Português</label> 
                  <label class="checkbox-inline"> <input type="checkbox" name="idioma_en" value="en" class="icheck idiomas" <?php echo ($poie['Poie']['idioma_en'] == 'en' ? 'checked' : ''); ?>> Inglês</label>
                  <label class="checkbox-inline"> <input type="checkbox" name="idioma_es" value="es" class="icheck idiomas" <?php echo ($poie['Poie']['idioma_es'] == 'es' ? 'checked' : ''); ?>> Espanhol</label>
                </div>
              </div>               

              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">                  
                  <button data-wizard="#wizard1" class="btn btn-primary wizard-next" data-current-block="1" data-next-block="2">Próximo <i class="fa fa-caret-right"></i></button>
                </div>
              </div>                  
            </div>

            <div class="step-pane block2" id="step2">

              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Português</h3>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Nome</label>
                <div class="col-sm-6">
                  <input type="text" name="nome_pt" class="form-control" placeholder="Nome" data-parsley-group="block2" value="<?php echo $poie['Poie']['nome_pt']; ?>" />
                </div>
              </div> 

              <div class="form-group">
                <label class="col-sm-3 control-label">Endereço</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="endereco_pt" style="height: 180px;" data-parsley-group="block2"><?php echo $poie['Poie']['endereco_pt']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Descrição</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="descricao_pt" style="height: 180px;" data-parsley-group="block2"><?php echo $poie['Poie']['descricao_pt']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Curiosidade</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="curiosidade_pt" style="height: 180px;" data-parsley-group="block2"><?php echo $poie['Poie']['curiosidade_pt']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Periodo de Visitação</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="periodo_visitacao_pt" style="height: 180px;"><?php echo $poie['Poie']['periodo_visitacao_pt']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Preço</label>
                <div class="col-sm-6">
                 <textarea class="form-control editor_html" name="preco_pt" style="height: 180px;"><?php echo $poie['Poie']['preco_pt']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                  <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Anterior</button>
                  <button data-wizard="#wizard1" class="btn btn-primary wizard-next" >Próximo <i class="fa fa-caret-right"></i></button>
                </div>
              </div>  

            </div>

            <div class="step-pane block2" id="step3">
              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Inglês</h3>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Nome</label>
                <div class="col-sm-6">
                  <input type="text" name="nome_en" class="form-control" placeholder="Nome" data-parsley-group="block3" value="<?php echo $poie['Poie']['nome_en']; ?>" />
                </div>
              </div> 

              <div class="form-group">
                <label class="col-sm-3 control-label">Endereço</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="endereco_en" style="height: 180px;" data-parsley-group="block3" ><?php echo $poie['Poie']['endereco_en']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Descrição</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="descricao_en" style="height: 180px;" data-parsley-group="block3" ><?php echo $poie['Poie']['descricao_en']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Curiosidade</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="curiosidade_en" style="height: 180px;" data-parsley-group="block3" ><?php echo $poie['Poie']['curiosidade_en']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Periodo de Visitação</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="periodo_visitacao_en" style="height: 180px;"><?php echo $poie['Poie']['periodo_visitacao_en']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Preço</label>
                <div class="col-sm-6">
                 <textarea class="form-control editor_html" name="preco_en" style="height: 180px;"><?php echo $poie['Poie']['preco_en']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                  <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Anterior</button>
                  <button data-wizard="#wizard1" class="btn btn-primary wizard-next">Próximo <i class="fa fa-caret-right"></i></button>
                </div>
              </div>  
            </div>

            <div class="step-pane block4" id="step4">
              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Espanhol</h3>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Nome</label>
                <div class="col-sm-6">
                  <input type="text" name="nome_es" class="form-control" placeholder="Nome" data-parsley-group="block4" value="<?php echo $poie['Poie']['nome_es']; ?>" />
                </div>
              </div> 

              <div class="form-group">
                <label class="col-sm-3 control-label">Endereço</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="endereco_es" style="height: 180px;" data-parsley-group="block4" ><?php echo $poie['Poie']['endereco_es']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Descrição</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="descricao_es" style="height: 180px;" data-parsley-group="block4" ><?php echo $poie['Poie']['descricao_es']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Curiosidade</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="curiosidade_es" style="height: 180px;" data-parsley-group="block4" ><?php echo $poie['Poie']['curiosidade_es']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Periodo de Visitação</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="periodo_visitacao_es" style="height: 180px;"><?php echo $poie['Poie']['periodo_visitacao_es']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Preço</label>
                <div class="col-sm-6">
                 <textarea class="form-control editor_html" name="preco_es" style="height: 180px;"><?php echo $poie['Poie']['preco_es']; ?></textarea>
                </div>
              </div>
              
              
              <div class="form-group">
                <div class="col-sm-12">
                  <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Anterior</button>
                  <button data-wizard="#wizard1" class="btn btn-primary wizard-next">Próximo <i class="fa fa-caret-right"></i></button>
                </div>
              </div>  
            </div>

            <div class="step-pane" id="step5">
              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Informações</h3>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Telefone</label>
                <div class="col-sm-6">
                  <input type="text" name="telefone" class="form-control" data-mask="telefone" placeholder="Telefone" value="<?php echo $poie['Poie']['telefone']; ?>" />
                </div>
              </div> 

              <div class="form-group">
                <label class="col-sm-3 control-label">Segundo Telefone</label>
                <div class="col-sm-6">
                  <input type="text" name="telefone2" class="form-control" data-mask="telefone" placeholder="Segundo telefone" value="<?php echo $poie['Poie']['telefone2']; ?>" />
                </div>
              </div> 

              <div class="form-group categorias">
                <label class="col-sm-3 control-label">Categoria</label>
                <div class="col-sm-6">
                  <select id="categorias" class="form-control"  name="category_id[]" multiple="multiple">
                    <?php                       
                      foreach ($categories as $key_c => $subcategorias) {
                        echo '<optgroup label="'.$subcategorias[$key_c].'">';                    
                          foreach ($subcategorias as $key_s => $subcategoria) {                            
                            echo in_array($key_s, $categoria_id);
                            if(in_array($key_s, $categoria_id)){
                              echo '<option value="'.$key_s.'" selected>'.$subcategoria.'</option>';    
                            }else{
                              echo '<option value="'.$key_s.'">'.$subcategoria.'</option>';    
                            }
                          }
                        echo '<optgroup>';                   
                      }
                    ?>                
                  </select>  
                              
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Link do site</label>
                <div class="col-sm-6">
                  <input type="text" name="link_site" class="form-control" placeholder="Link do site" value="<?php echo $poie['Poie']['link_site']; ?>" />
                </div>
              </div>  

              <div class="form-group">
                <label class="col-sm-3 control-label">Coordenada Georeferenciada</label>
                <div class="col-sm-6">
                  <div class="input-group">
                    <input type="text" id="geocomplete" class="form-control" placeholder="Localização">
                    <span class="input-group-btn">
                    <button id="find" class="btn btn-primary" type="button">Pesquisar</button>
                    </span>
                  </div>                            
                  <div class="map_canvas" style="width: 440px; height: 250px;"></div>              
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Lat/Long</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" data-geo="lat" name="latitude" value="<?php echo $poie['Poie']['latitude']; ?>"/>
                    </div>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" data-geo="lng" name="longitude" value="<?php echo $poie['Poie']['longitude']; ?>"/>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">QR Code</label>
                <div class="col-sm-6">
                  <?php 
                  if(isset($poie['Poie']['link_site']) && !empty($poie['Poie']['link_site'])) {
                      echo '<a href="'.$config_cidade.'/poies/qrcode/'.$poie['Poie']['id'].'" target="_blank"><img src="'.$config_cidade.'/uploads/qrcode/qrcode_poie_'.$poie['Poie']['id'].'.png" /></a>' ;
                    }else{
                      echo '<p>Não há QR Code</p>';
                    }
                  ?>                   
                </div>                                
              </div>

              <div class="form-group">
	            <label class="col-sm-3 control-label">Fotos</label>
	            <div class="col-sm-6">
	              <button type="button" class="btn btn-primary btn-flat md-trigger" data-modal="gerenciador-imagem">Gerenciador de imagens</button>     
	            </div>
	          </div>  
              
              <?php
              
              $radios = array();
              $radios['Serviços']['Alimentação'] = 'fl_possui_alimentacao';
              $radios['Serviços']['Banheiro'] = 'fl_banheiro';
              $radios['Serviços']['Bar'] = 'fl_bar';
              $radios['Serviços']['Bebida Alcoolica'] = 'fl_bebida_alcoolica';
              $radios['Serviços']['Compras'] = 'fl_compas';
              $radios['Serviços']['Estacionamento Pago'] = 'fl_estacionamento_pago';
              $radios['Serviços']['Estacionamento Gratuito'] = 'fl_estacionamento_gratuito';
              $radios['Serviços']['Loja Souvenir'] = 'fl_loja_souvenir';
              $radios['Serviços']['Restaurante'] = 'fl_restaurante';
              $radios['Serviços']['Aluguel de cadeiras e guarda sol'] = 'fl_aluguel_cadeira_guardasol';
              $radios['Serviços']['Esporte de areia'] = 'fl_esporte_areia';
              $radios['Serviços']['Esporte aquático'] = 'fl_esporte_aquatico';
              $radios['Serviços']['Guarda vidas'] = 'fl_guarda_vidas';

              $radios['Formas de Acesso']['A pé'] = 'fl_acesso_pe';
              $radios['Formas de Acesso']['Carro'] = 'fl_acesso_carro';
              $radios['Formas de Acesso']['Ônibus'] = 'fl_acesso_onibus';
              $radios['Formas de Acesso']['Embarcação'] = 'fl_embarcacao';
              $radios['Formas de Acesso']['Bicicleta'] = 'fl_bicicleta';
              
              $radios['Outros']['Área de Fumante'] = 'fl_possui_area_fumante';              
              $radios['Outros']['Brinquedoteca'] = 'fl_briquedoteca';
              $radios['Outros']['Cinema'] = 'fl_cinema';
              $radios['Outros']['Espetáculo'] = 'fl_espetaculo';
              $radios['Outros']['Exposição'] = 'fl_exposicao';
              $radios['Outros']['Fraldário'] = 'fl_fraldario';
              $radios['Outros']['Gay Frieldly'] = 'fl_gay_frieldly';
              $radios['Outros']['Noite'] = 'fl_noite';
              $radios['Outros']['Shows/Eventos'] = 'fl_shows_eventos';
              $radios['Outros']['Visita Guiada'] = 'fl_visita_guiada';              
              $radios['Outros']['Roteiro'] = 'fl_roteiro';
              
              foreach ($radios as $label => $name) {
                echo '<h3>'.$label.'</h3>';                                                  
                foreach ($name as $key => $value) {
                  if($value == 'ic_acessibilidade')
                  {
                    echo '
                    <div class="form-group">
                      <label class="col-sm-3 control-label">'.$key.'</label>
                      <div class="col-sm-6">
                        <label class="radio-inline"> <input type="radio" name="'.$value.'" value="1" class="icheck" '.($poie['Poie'][$value] == 1 ? 'checked' : '').'> Nivel 1</label> 
                        <label class="radio-inline"> <input type="radio" name="'.$value.'" value="2" class="icheck" '.($poie['Poie'][$value] == 2 ? 'checked' : '').'> Nivel 2</label>
                        <label class="radio-inline"> <input type="radio" name="'.$value.'" value="3" class="icheck" '.($poie['Poie'][$value] == 3 ? 'checked' : '').'> Nivel 3</label>
                      </div>
                    </div> 
                    ';  
                  }
                  else
                  {
                    echo '
                    <div class="form-group">
                      <label class="col-sm-3 control-label">'.$key.'</label>
                      <div class="col-sm-6">
                        <label class="radio-inline"> <input type="radio" name="'.$value.'" value="1" class="icheck" '.($poie['Poie'][$value] == 1 ? 'checked' : '').'> Sim</label> 
                        <label class="radio-inline"> <input type="radio" name="'.$value.'" value="0" class="icheck" '.($poie['Poie'][$value] == 0 ? 'checked' : '').'> Não</label>
                      </div>
                    </div> 
                    ';  
                  }
                }                              
              } 

              ?> 

              <h3>Status</h3>
              <div class="form-group">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-6">
                  <label class="radio-inline"> <input type="radio" name="status" value="1" class="icheck" <?php echo ($poie['Poie']['status'] == 1 ? 'checked' : '') ?>> Ativo</label>  
                  <label class="radio-inline"> <input type="radio" name="status" value="0" class="icheck" <?php echo ($poie['Poie']['status'] == 0 ? 'checked' : '') ?>> Inativo</label>
                </div>
              </div>
              
              <div class="form-group">
                <div class="col-sm-12">
                  <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Anterior</button>
                  <button data-wizard="#wizard1" class="btn btn-success wizard-next"><i class="fa fa-check"></i> Concluir</button>
                </div>
              </div> 
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="md-modal colored-header custom-width md-effect-9" id="gerenciador-imagem">
    <div class="md-content">
      <div class="modal-header">
        <h3>Gerenciador de imagens</h3>
        <button type="button" class="close md-close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body form">
       
        
        <form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
            
            <noscript><input type="hidden" name="redirect" value="http://blueimp.github.io/jQuery-File-Upload/"></noscript>
           
            <div class="row fileupload-buttonbar">
                <div class="col-lg-10">
                   
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Adicionar Imagens</span>
                        <input type="file" name="files[]" multiple>
                    </span>
                    <button type="submit" class="btn btn-primary start">
                        <i class="glyphicon glyphicon-upload"></i>
                        <span>Start upload</span>
                    </button>
                    <button type="reset" class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancelar upload</span>
                    </button>
                    <button type="button" class="btn btn-danger delete">
                        <i class="glyphicon glyphicon-trash"></i>
                        <span>Excluir</span>
                    </button>
                    <input type="checkbox" class="toggle">
                    
                    <span class="fileupload-process"></span>
                </div>
                
                <div class="col-lg-5 fileupload-progress fade">
                    
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                    </div>
                    
                    <div class="progress-extended">&nbsp;</div>
                </div>
            </div>
            
            <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
        </form> 
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat md-close" data-dismiss="modal">Fechar</button>
      </div>
    </div>
</div>
<div class="md-overlay"></div>

<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processando...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancelar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>

<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="http://{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="http://{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>Ampliar imagem</a>
                {% } else { %}
                    <span>Ampliar imagem</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Erro</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
            {% 
              link_delete = rawurlencode(file.name);
              id = $('input[name="id"]').val();
             %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=config_cidade%}/poies/uploadHandlerEdit/{%=id%}/?file={%=link_delete%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Excluir</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancelar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>

