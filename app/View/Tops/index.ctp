<div class="row">
  <div class="col-md-12">
  
    <div class="block-flat">
      <div class="header">							
        <h3>Top 10</h3>
      </div>
      <div class="content">

      	<form class="form-horizontal group-border-dashed">
	      	

      		<div class="form-group">
	            <label class="col-sm-3 control-label"></label>
	            <div class="col-sm-6">
	              <select id='pre-selected-options' multiple='multiple'>
			    	<?php 
			    		foreach ($poies as $key => $poie) {
			    			echo '<option value="'.$key.'" '.(in_array($key, $tops) == true ? 'selected' : '').'>'.$poie.'</option>';
			    		}
			    	?>
			    </select>
	            </div>
          	</div>

		    <div class="form-group">
		        <div class="col-sm-offset-6 col-sm-8">
		          <button type="submit" class="btn btn-primary">Cadastrar</button>                  
		        </div>
	      	</div>
		</form>
      </div>
    </div>
    
  </div>
</div>