<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3><?php echo AuthComponent::user('User.nome'); ?>, seja bem-vindo.</h3>
			</div>
			<div class="content">
				<br />
                <br />
                <p style="font-weight:600; font-size:14px;">
                	Você está acessando o painel de controle do aplicativo Vivendo Floripa,<br />
                    o guia oficial da Prefeitura Municipal de Florianópolis, a PMF.
                </p>
                <p>Navegue por todo o sistema pelo menu ao lado esquerdo.</p>
			</div>
		</div>				
	</div>
</div>