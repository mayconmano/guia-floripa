<div class="row">
  <div class="col-md-12">
  
    <div class="block-flat">
      <div class="header">							
        <h3>Roteiro auto guiado  ( Explicação )</h3>
      </div>
      <div class="content">
        <form class="form-horizontal group-border-dashed" action="" method="post">
        
        	<input type="hidden" name="id" value="<?php echo $selfguiededs['Selfguieded']['id'] ?>">
	        <div class="form-group">	
				<label class="col-sm-3 control-label">Descrição em Português</label>
				<div class="col-sm-6">
				  <textarea class="form-control editor_html" name="descricao_pt" style="height: 180px;" data-parsley-group="block2" ><?php echo $selfguiededs['Selfguieded']['descricao_pt'] ?></textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Descrição em Inglês</label>
				<div class="col-sm-6">
				  <textarea class="form-control editor_html" name="descricao_en" style="height: 180px;" data-parsley-group="block2" ><?php echo $selfguiededs['Selfguieded']['descricao_en'] ?></textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Descrição em Espanhol</label>
				<div class="col-sm-6">
				  <textarea class="form-control editor_html" name="descricao_es" style="height: 180px;" data-parsley-group="block2" ><?php echo $selfguiededs['Selfguieded']['descricao_es'] ?></textarea>
				</div>
			</div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary">Salvar</button>                  
            </div>
          </div>
        </form>
      </div>
    </div>
    
  </div>
</div>