<div class="row">
  <div class="col-md-12">
  
    <div class="block-flat">
      <div class="header">							
        <h3>Visualizar</h3>
      </div>
      <div class="content">
        <form class="form-horizontal group-border-dashed" action="" method="post"> 
          <input type="hidden" name="id" value="<?php echo $contacts['Contact']['id']; ?>"/>
          <div class="form-group">
            <label class="col-sm-3 control-label">Nome</label>
            <div class="col-sm-6">
              <input type="text" value="<?php echo $contacts['Contact']['nome']; ?>" class="form-control" disabled/>
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label">Email</label>
            <div class="col-sm-6">
              <input type="text" value="<?php echo $contacts['Contact']['email']; ?>" class="form-control" disabled/>
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label">Assunto</label>
            <div class="col-sm-6">
              <input type="text" value="<?php echo $contacts['Contact']['assunto']; ?>" class="form-control" disabled/>
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label">Mensagem</label>
            <div class="col-sm-6">
              <textarea class="form-control" style="height: 180px;" disabled><?php echo $contacts['Contact']['mensagem']; ?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Histórico de resposta</label>
            <div class="col-sm-6">
              <textarea class="form-control" name="resposta" style="height: 180px;"><?php echo $contacts['Contact']['resposta']; ?></textarea>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary">Salvar</button>                  
            </div>
          </div>

        </form>
      </div>
    </div>
    
  </div>
</div>