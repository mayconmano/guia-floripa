<!-- Fixed navbar -->
  <div id="head-nav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="fa fa-gear"></span>
        </button>
        <a class="navbar-brand" href="#">Vivendo Floripa</a>
      </div>
      <div class="navbar-collapse collapse">
        
    <ul class="nav navbar-nav navbar-right user-nav">
      <li class="dropdown profile_menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo AuthComponent::user('User.nome'); ?> <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="usuarios/editar/<?php echo AuthComponent::user('User.id'); ?>">Minha Conta</a></li>
          <li class="divider"></li>
          <li><a href="<?php echo $config_cidade; ?>/sair">Sair</a></li>
        </ul>
      </li>
    </ul>			
    
      </div><!--/.nav-collapse -->
    </div>
  </div>