<div class="cl-sidebar">
	<div class="cl-toggle"><i class="fa fa-bars"></i></div>
	<div class="cl-navblock">
    <div class="menu-space">
      <div class="content">
        <div class="side-user">
          <div class="avatar"></div>
          <div class="info">
            <a href="#"><?php echo AuthComponent::user('User.nome'); ?></a>            
          </div>
        </div>
          <ul class="cl-vnavigation">
            <li><a href="#"><i class="fa fa-smile-o"></i><span>Usuários</span></a>
              <ul class="sub-menu">
                <li><a href="<?php echo $config_cidade; ?>/usuarios"><i class="fa fa-angle-right"></i>Listar Usuários</a></li>
                <li><a href="<?php echo $config_cidade; ?>/usuarios/adicionar"><i class="fa fa-angle-right"></i>Adicionar Usuários</a></li>              
              </ul>
            </li>  
            <li><a href="#"><i class="fa fa-map-marker nav-icon"></i><span>POI</span></a>
              <ul class="sub-menu">
                <li><a href="<?php echo $config_cidade; ?>/pois"><i class="fa fa-angle-right"></i>Listar POI's</a></li>
                <li><a href="<?php echo $config_cidade; ?>/pois/adicionar"><i class="fa fa-angle-right"></i>Adicionar POI's</a></li>  
                <li><a href="<?php echo $config_cidade; ?>/top10"><i class="fa fa-angle-right"></i>Top 10</a></li>            
              </ul>
            </li> 
            <li><a href="#"><i class="fa fa-file-text-o"></i><span>Notícias</span></a>
              <ul class="sub-menu">
                <li><a href="<?php echo $config_cidade; ?>/noticias"><i class="fa fa-angle-right"></i>Listar Notícias</a></li>
                <li><a href="<?php echo $config_cidade; ?>/noticias/adicionar"><i class="fa fa-angle-right"></i>Adicionar Notícias</a></li>              
              </ul>
            </li> 
            <li><a href="#"><i class="fa fa-calendar"></i><span>Agenda</span></a>
              <ul class="sub-menu">
                <li><a href="<?php echo $config_cidade; ?>/agendas"><i class="fa fa-angle-right"></i>Listar Agenda</a></li>
                <li><a href="<?php echo $config_cidade; ?>/agendas/adicionar"><i class="fa fa-angle-right"></i>Adicionar Agenda</a></li>              
              </ul>
            </li>  
            <li><a href="#"><i class="fa fa-building-o"></i><span>Tudo sobre floripa</span></a>
              <ul class="sub-menu">
                <li><a href="<?php echo $config_cidade; ?>/clima"><i class="fa fa-angle-right"></i>Clima</a></li>
                <li><a href="<?php echo $config_cidade; ?>/ssah"><i class="fa fa-angle-right"></i>Seguro Saúde e Assitência Hospitalar</a></li>
                <li><a href="<?php echo $config_cidade; ?>/historia_cidade"><i class="fa fa-angle-right"></i>História da cidade</a></li>
                <li><a href="<?php echo $config_cidade; ?>/leis_locais"><i class="fa fa-angle-right"></i>Leis locais</a></li>
                <li><a href="<?php echo $config_cidade; ?>/horarios"><i class="fa fa-angle-right"></i>Horário comercial e feriados</a></li>
                <li><a href="<?php echo $config_cidade; ?>/fuso_horario"><i class="fa fa-angle-right"></i>Fuso horário</a></li>                
                <li><a href="<?php echo $config_cidade; ?>/telefonias"><i class="fa fa-angle-right"></i>Telefonia</a></li>
                <li><a href="<?php echo $config_cidade; ?>/telefones_uteis"><i class="fa fa-angle-right"></i>Telefones úteis</a></li>
                <li><a href="<?php echo $config_cidade; ?>/operadoras"><i class="fa fa-angle-right"></i>Operadoras de celular</a></li>
                <li><a href="<?php echo $config_cidade; ?>/tomadas"><i class="fa fa-angle-right"></i>Tomadas</a></li>
                <li><a href="<?php echo $config_cidade; ?>/tipo_ligacoes"><i class="fa fa-angle-right"></i>Tipos de ligações</a></li>
                <li><a href="<?php echo $config_cidade; ?>/visto_passaporte"><i class="fa fa-angle-right"></i>Visto e passaporte</a></li>
                <li><a href="<?php echo $config_cidade; ?>/hospedagem"><i class="fa fa-angle-right"></i>Hospedagem</a></li>
                <li><a href="<?php echo $config_cidade; ?>/alimentacao"><i class="fa fa-angle-right"></i>Alimentação</a></li>                
              </ul>
            </li>  
            <li><a href="#"><i class="fa fa-road"></i><span>Circulando em floripa</span></a>
              <ul class="sub-menu">
                <li><a href="<?php echo $config_cidade; ?>/bicicleta"><i class="fa fa-angle-right"></i>Bicicleta</a></li>
                <li><a href="<?php echo $config_cidade; ?>/ape"><i class="fa fa-angle-right"></i>A pé</a></li>
                <li><a href="<?php echo $config_cidade; ?>/taxi"><i class="fa fa-angle-right"></i>Táxi</a></li>
                <li><a href="<?php echo $config_cidade; ?>/transporte_hidroviario"><i class="fa fa-angle-right"></i>Transporte Hidroviário</a></li>
                <li><a href="<?php echo $config_cidade; ?>/onibus"><i class="fa fa-angle-right"></i>Ônibus</a></li>                          
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-location-arrow"></i><span>Chegando em floripa</span></a>
              <ul class="sub-menu">
                <li><a href="<?php echo $config_cidade; ?>/onde-fica"><i class="fa fa-angle-right"></i>Onde fica floripa no mundo</a></li>
                <li><a href="#"><span>Vou para floripa de:</span></a>
                  <ul class="sub-menu">                    
                    <li><a href="<?php echo $config_cidade; ?>/aviao"><i class="fa fa-angle-right"></i>Avião</a></li>
                    <li><a href="<?php echo $config_cidade; ?>/carro"><i class="fa fa-angle-right"></i>Carro</a></li>
                    <li><a href="<?php echo $config_cidade; ?>/ir-onibus"><i class="fa fa-angle-right"></i>Ônibus</a></li>
                    <li><a href="<?php echo $config_cidade; ?>/embarcacao"><i class="fa fa-angle-right"></i>Embarcação</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-location-arrow"></i><span>Questionários</span></a>
              <ul class="sub-menu">
                <li><a href="<?php echo $config_cidade; ?>/perguntas"><i class="fa fa-angle-right"></i>Listar Perguntas</a></li>
                <li><a href="<?php echo $config_cidade; ?>/perguntas/adicionar"><i class="fa fa-angle-right"></i>Adicionar Perguntas</a></li>                
              </ul>
            </li>      
            <li><a href="#"><i class="fa fa-location-arrow"></i><span>Gerencidor de menu</span></a>
              <ul class="sub-menu">
                <li><a href="<?php echo $config_cidade; ?>/menu"><i class="fa fa-angle-right"></i>Listar Menu</a></li>
                <li><a href="<?php echo $config_cidade; ?>/menu/adicionar"><i class="fa fa-angle-right"></i>Adicionar Menu</a></li>                
              </ul>
            </li>        
            <li><a href="<?php echo $config_cidade; ?>/roteiro_auto_guiado"><i class="fa fa-angle-right"></i>Roteiro auto guiado ( Explicação )</a></li> 
            <li><a href="<?php echo $config_cidade; ?>/contato"><i class="fa fa-angle-right"></i>Contato SETUR</a></li>              
            <li><a href="<?php echo $config_cidade; ?>/versions"><i class="fa fa-angle-right"></i>Atualizar versão Mobile</a></li>     
            <?php if($_SESSION['Auth']['User']['User']['is_admin']){ ?>
            <li><a href="<?php echo $config_cidade; ?>/logs"><i class="fa fa-angle-right"></i>Log</a></li>              
            <?php } ?>
          </ul>                  
      </div>
    </div>
    <div class="text-right collapse-button" style="padding:7px 9px;">      
      <button id="sidebar-collapse" class="btn btn-default" style=""><i style="color:#fff;" class="fa fa-angle-left"></i></button>
    </div>
	</div>
</div>