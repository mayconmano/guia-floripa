<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3>Telefones Uteis</h3>				
			</div>
			<div class="content">
				<div class="table-responsive">
					<table class="table table-bordered" id="datatable" >
						<thead>
							<tr>								
								<th>Nome do local</th>								
								<th>Telefone</th>								
								<th>Situação</th>								
								<th>Opções</th>
							</tr>
						</thead>
						<tbody>
							<?php							
								foreach ($usefulphones as $key => $usefulphone) {
									if($key % 2 == 0) $class = "old"; else $class = "even";
									echo '<tr id="'.$usefulphone['Usefulphone']['id'].'" class="'.$class.'">';
										echo '<td>'.$usefulphone['Usefulphone']['nome_local_pt'].'</td>';
										echo '<td>'.$usefulphone['Usefulphone']['telefone'].'</td>';
										echo '<td>'.($usefulphone['Usefulphone']['status'] == 1 ? '<span class="label label-success inativo" style="cursor: pointer;">Ativo</span>' : '<span class="label label-default ativo" style="cursor: pointer;">Inativo</span>').'</td>';
										echo '<td></td>';
									echo '</tr>';								
								}		
							?>											
						</tbody>
					</table>							
				</div>
				<a href="<?php echo $config_cidade; ?>/telefones_uteis/adicionar"><button class="btn btn-primary">Adicionar</button></a>
			</div>
		</div>				
	</div>
</div>