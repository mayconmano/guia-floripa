<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="images/favicon.png">

	<title>Vivendo Floripa</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

	<?php
	echo $this->Html->css('/js/bootstrap/dist/css/bootstrap');
	echo $this->Html->css('/js/jquery.gritter/css/jquery.gritter');
	echo $this->Html->css('/fonts/font-awesome-4/css/font-awesome.min');
	echo $this->Html->css('/js/jquery.nanoscroller/nanoscroller');
	echo $this->Html->css('/js/jquery.easypiechart/jquery.easy-pie-chart');
	echo $this->Html->css('/js/bootstrap.switch/bootstrap-switch');
	echo $this->Html->css('/js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min');
	echo $this->Html->css('/js/jquery.select2/select2');
	echo $this->Html->css('/js/bootstrap.slider/css/slider');
	echo $this->Html->css('font-awesome.min.css');	
	?>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="../../assets/js/html5shiv.js"></script>
	  <script src="../../assets/js/respond.min.js"></script>
	<![endif]-->
	<?php 
		if(isset($css))
		{
			foreach ($css as $key => $value) {
				echo $this->Html->css($value);			
			}
		}
	?>		
	<?php echo $this->Html->css('style');?>	
</head>

<body id="loading">

  <?php echo $this->element('header'); ?>
	
<div id="cl-wrapper">
		
	<?php echo $this->element('sidebar'); ?>

	<div class="container-fluid" id="pcont">

		<div class="cl-mcont">
			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
			
		</div>
	
	</div> 
	
</div>
	
	<?php 
		echo $this->Html->script('jquery');
		echo $this->Html->script('jquery.nanoscroller/jquery.nanoscroller');
		echo $this->Html->script('jquery.sparkline/jquery.sparkline.min');
		echo $this->Html->script('jquery.easypiechart/jquery.easy-pie-chart');
		echo $this->Html->script('jquery.ui/jquery-ui');
		echo $this->Html->script('jquery.nestable/jquery.nestable');
		echo $this->Html->script('bootstrap.switch/bootstrap-switch.min');
		echo $this->Html->script('bootstrap.datetimepicker/js/bootstrap-datetimepicker.min');
		echo $this->Html->script('jquery.select2/select2.min');
		echo $this->Html->script('bootstrap.slider/js/bootstrap-slider');
		echo $this->Html->script('jquery.gritter/js/jquery.gritter');
		echo $this->Html->script('behaviour/general');		
	?>

    <script type="text/javascript">
      $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.charts();
      });
    </script>

    <?php
		echo $this->Html->script('behaviour/voice-commands');		
		echo $this->Html->script('bootstrap/dist/js/bootstrap.min');		
		echo $this->Html->script('jquery.flot/jquery.flot');		
		echo $this->Html->script('jquery.flot/jquery.flot.pie');		
		echo $this->Html->script('jquery.flot/jquery.flot.resize');		
		echo $this->Html->script('jquery.flot/jquery.flot.labels');	
		if(isset($js))
		{
			foreach ($js as $key => $value) {
				echo $this->Html->script($value);					
			}
		} 				
    ?>

</body>
</html>
