<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3>Gerencciador de Menu</h3>
			</div>
			<div class="content">
				<div class="table-responsive">
					<table class="table table-bordered" id="datatable" >
						<thead>
							<tr>								
								<th>Ordem</th>								
								<th>Icone</th>								
								<th>Titulo</th>								
								<th>Situação</th>
								<th>Opções</th>
							</tr>
						</thead>
						<tbody>
							<?php						
								foreach ($menus as $key => $menu) {
									if($key % 2 == 0) $class = "old"; else $class = "even";
									echo '<tr id="'.$menu['Menu']['id'].'" class="'.$class.'">';
										echo '<td>'.$menu['Menu']['ordem'].'</td>';
										echo '<td><i class="'.$menu['Menu']['icone'].'" style="color: '.$menu['Menu']['icone_cor'].'; font-size: 2em;"></i></td>';
										echo '<td>'.$menu['Menu']['titulo_pt'].'</td>';
										echo '<td>'.($menu['Menu']['status'] == 1 ? '<span class="label label-success inativo" style="cursor: pointer;">Ativo</span>' : '<span class="label label-default ativo" style="cursor: pointer;">Inativo</span>').'</td>';
										echo '<td></td>';
									echo '</tr>';								
								}		
							?>											
						</tbody>
					</table>							
				</div>
			</div>
		</div>				
	</div>
</div>