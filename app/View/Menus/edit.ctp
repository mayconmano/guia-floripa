<div class="cl-mcont">    
  <div class="row wizard-row">
    <div class="col-md-12 fuelux">
      <div class="block-wizard">
        <div id="wizard1" class="wizard wizard-ux">
          <ul class="steps">
            <li data-target="#step1" class="active">Idioma(s)<span class="chevron"></span></li>
            <li data-target="#step2" class="<?php echo ($menus['Menu']['idioma_pt'] != 'pt' ? 'hide' : ''); ?>" idioma="pt">Português<span class="chevron"></span></li>
            <li data-target="#step3" class="<?php echo ($menus['Menu']['idioma_en'] != 'en' ? 'hide' : ''); ?>" idioma="en">Inglês<span class="chevron"></span></li>
            <li data-target="#step4" class="<?php echo ($menus['Menu']['idioma_es'] != 'es' ? 'hide' : ''); ?>" idioma="es">Espanhol<span class="chevron"></span></li>            
            <li data-target="#step5">Informações<span class="chevron"></span></li>
          </ul>
          <div class="actions">
            <button type="button" class="btn btn-xs btn-prev btn-default"> <i class="icon-arrow-left"></i>Anterior</button>
            <button type="button" class="btn btn-xs btn-next btn-default" data-last="Concluir">Próximo<i class="icon-arrow-right"></i></button>
          </div>
        </div>
        <div class="step-content">
          <form id="form_menu" class="form-horizontal group-border-dashed" action="" method="post" > 
            <input type="hidden" name="id" value="<?php echo $menus['Menu']['id']; ?>">
            <div class="step-pane block1 active" id="step1">
              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Escolha o(s) idioma(s)</h3>
                </div>
              </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">Idiomas</label>
                <div class="col-sm-6">
                  <label class="checkbox-inline"> <input type="checkbox" name="idioma_pt" value="pt" class="icheck idiomas" <?php echo ($menus['Menu']['idioma_pt'] == 'pt' ? 'checked' : ''); ?> > Português</label> 
                  <label class="checkbox-inline"> <input type="checkbox" name="idioma_en" value="en" class="icheck idiomas" <?php echo ($menus['Menu']['idioma_en'] == 'en' ? 'checked' : ''); ?>> Inglês</label>
                  <label class="checkbox-inline"> <input type="checkbox" name="idioma_es" value="es" class="icheck idiomas" <?php echo ($menus['Menu']['idioma_es'] == 'es' ? 'checked' : ''); ?>> Espanhol</label>
                </div>
              </div> 

              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">                  
                  <button data-wizard="#wizard1" class="btn btn-primary wizard-next" data-current-block="1" data-next-block="2">Próximo <i class="fa fa-caret-right"></i></button>
                </div>
              </div>                  
            </div>

            <div class="step-pane block2" id="step2">

              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Português</h3>
                </div>
              </div>       

              <div class="form-group">
                <label class="col-sm-3 control-label">Titulo</label>
                <div class="col-sm-6">
                  <input class="form-control" name="titulo_pt" value="<?php echo $menus['Menu']['titulo_pt']; ?>" />
                </div>
              </div>       

              <div class="form-group">
                <label class="col-sm-3 control-label">Descrição</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="descricao_pt" style="height: 180px;" data-parsley-group="block2" ><?php echo $menus['Menu']['descricao_pt']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                  <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Anterior</button>
                  <button data-wizard="#wizard1" class="btn btn-primary wizard-next" >Próximo <i class="fa fa-caret-right"></i></button>
                </div>
              </div>  

            </div>

            <div class="step-pane block2" id="step3">
              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Inglês</h3>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Titulo</label>
                <div class="col-sm-6">
                  <input class="form-control" name="titulo_en" value="<?php echo $menus['Menu']['titulo_en']; ?>"/>
                </div>
              </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">Descrição</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="descricao_en" style="height: 180px;" data-parsley-group="block2" ><?php echo $menus['Menu']['descricao_en']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                  <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Anterior</button>
                  <button data-wizard="#wizard1" class="btn btn-primary wizard-next">Próximo <i class="fa fa-caret-right"></i></button>
                </div>
              </div>  
            </div>

            <div class="step-pane block4" id="step4">
              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Espanhol</h3>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Titulo</label>
                <div class="col-sm-6">
                  <input class="form-control" name="titulo_es" value="<?php echo $menus['Menu']['titulo_es']; ?>"/>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Descrição</label>
                <div class="col-sm-6">
                  <textarea class="form-control editor_html" name="descricao_es" style="height: 180px;" data-parsley-group="block2" ><?php echo $menus['Menu']['descricao_es']; ?></textarea>
                </div>
              </div>
              
              <div class="form-group">
                <div class="col-sm-12">
                  <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Anterior</button>
                  <button data-wizard="#wizard1" class="btn btn-primary wizard-next">Próximo <i class="fa fa-caret-right"></i></button>
                </div>
              </div>  
            </div>

            <div class="step-pane" id="step5">
              <div class="form-group no-padding">
                <div class="col-sm-7">
                  <h3 class="hthin">Informações</h3>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Situação</label>
                <div class="col-sm-6">
                  <div class="radio">                
                   <label class="radio-inline"> <input type="radio" name="status" class="icheck" value="1" <?php echo ($menus['Menu']['status'] == 1 ? 'checked' : ''); ?>> Ativo</label>
                   <label class="radio-inline"> <input type="radio" name="status" class="icheck" value="0" <?php echo ($menus['Menu']['status'] == 0 ? 'checked' : ''); ?>> Inativo</label> 
                  </div>                    
                </div>
              </div> 

            <div class="form-group">
              <label class="col-sm-3 control-label"></label>
              <div class="col-sm-6">
                <button type="button" class="btn btn-primary btn-flat md-trigger" data-modal="gerenciador-imagem">Gerenciador de imagens</button>     
              </div>
            </div> 

              <div class="form-group">
                <label class="col-sm-3 control-label">Cor do icone</label>
                <div class="col-sm-6">
                  <div id="colorSelector"><div style="background-color: <?php echo ($menus['Menu']['icone_cor'] == '' ? '#000000' : $menus['Menu']['icone_cor']); ?>"></div></div>                              
                </div>
              </div> 

              <div class="form-group">
                <label class="col-sm-3 control-label">Icones</label>
                <div class="col-sm-6">
                <input type="hidden" id="icone_cor" name="icone_cor" value="<?php echo $menus['Menu']['icone_cor']; ?>">                            
                <input type="hidden" id="icone" name="icone" value="<?php echo $menus['Menu']['icone']; ?>">  
                <?php 
                $icone_verifica = false;                
                if($menus['Menu']['icone'] != ''){
                  $icone_verifica = true;
                }
                foreach ($icones as $key => $icone) {
                  $selected = '';
                  if($icone_verifica){                                    
                    if(explode(' ', $icone)[1] == explode(' ', $menus['Menu']['icone'])[2]){ $selected = 'icon-selected'; }  
                  }
                  echo '<div class="fa-hover col-md-2 col-sm-4 '.$selected.'"><i class="fa '.$icone.'" style="font-size: 2em !important; color: #000000;"></i></div>';
                }
                ?>                                              
                </div>
              </div> 
              
              <div class="form-group">
                <div class="col-sm-12">
                  <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Anterior</button>
                  <button data-wizard="#wizard1" class="btn btn-success wizard-next"><i class="fa fa-check"></i> Concluir</button>
                </div>
              </div> 
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="md-modal colored-header custom-width md-effect-9" id="gerenciador-imagem">
    <div class="md-content">
      <div class="modal-header">
        <h3>Gerenciador de imagens</h3>
        <button type="button" class="close md-close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body form">
       
        
        <form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
            
            <noscript><input type="hidden" name="redirect" value="http://blueimp.github.io/jQuery-File-Upload/"></noscript>
           
            <div class="row fileupload-buttonbar">
                <div class="col-lg-10">
                   
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Adicionar Imagens</span>
                        <input type="file" name="files">
                    </span>
                    <button type="submit" class="btn btn-primary start">
                        <i class="glyphicon glyphicon-upload"></i>
                        <span>Start upload</span>
                    </button>
                    <button type="reset" class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancelar upload</span>
                    </button>
                    <button type="button" class="btn btn-danger delete">
                        <i class="glyphicon glyphicon-trash"></i>
                        <span>Excluir</span>
                    </button>
                    <input type="checkbox" class="toggle">
                    
                    <span class="fileupload-process"></span>
                </div>
                
                <div class="col-lg-5 fileupload-progress fade">
                    
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                    </div>
                    
                    <div class="progress-extended">&nbsp;</div>
                </div>
            </div>
            
            <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
        </form> 
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat md-close" data-dismiss="modal">Fechar</button>
      </div>
    </div>
</div>
<div class="md-overlay"></div>

<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processando...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancelar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>

<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="http://{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="http://{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>Ampliar imagem</a>
                {% } else { %}
                    <span>Ampliar imagem</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Erro</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
            {% 
              link_delete = rawurlencode(file.name);
              id = $('input[name="id"]').val();
             %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=config_cidade%}/menus/uploadHandlerEdit/{%=id%}/?file={%=link_delete%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Excluir</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancelar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>