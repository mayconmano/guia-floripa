<?php

header('Access-Control-Allow-Origin: *');
App::uses('AppController', 'Controller');

class WebserviceController extends AppController {

    function beforeFilter() {
        $this->Auth->allow();
    }

    public function versao() {
        $this->autoRender = false;
        $this->loadModel('Version');
        $versao = $this->Version->find('first', array('order' => array('versao' => 'DESC')));

        return json_encode($versao);
    }

    public function atualizacao() {
        $this->autoRender = false;

        $tabelas = json_decode($this->request->data['tabelas'], true);

        $resultado = array();
        foreach ($tabelas as $key => $tabela) {
            if (strstr($tabela, "/")) {
                $explode = explode('/', $tabela);
                $funcao = $explode[0];
                $parametro = $explode[1];
                $resultado[$tabela] = $this->$funcao($parametro);
            } else {
                $resultado[$tabela] = $this->$tabela();
            }
        }

        echo json_encode($resultado);
    }

    public function agenda() {

        $this->autoRender = false;
        $this->loadModel('Agenda');
        $this->Agenda->recursive = -1;
        $agendas = $this->Agenda->query(
                "SELECT agenda.id, agenda.titulo, agenda.resumo, agenda.descricao, agenda.link, agenda.news_id, news.link, agenda.interno 
			FROM agendas AS agenda
			LEFT JOIN news AS news ON (news.id = agenda.news_id)
			WHERE agenda.status = 1 AND agenda.data_publicacao <= CURDATE() AND agenda.data_expiracao > CURDATE()");

        foreach ($agendas as $key => $agenda) {
            $agendas[$key]['imagem'] = $this->getImagens($this->config_cidade . '/app/webroot/uploads/agenda/', $agenda['agenda']['id']);
        }

        return json_encode($agendas);
    }

    public function noticias() {
        $this->autoRender = false;
        $this->loadModel('News');
        $this->News->recursive = 0;
        $news = $this->News->query(
                "SELECT news.id, news.titulo, news.resumo, news.descricao, news.link 
			FROM news AS news			
			WHERE news.status = 1 AND news.data_publicacao <= CURDATE() AND news.data_expiracao > CURDATE()");

        foreach ($news as $key => $new) {
            $news[$key]['imagem'] = $this->getImagens($this->config_cidade . '/app/webroot/uploads/news/', $new['news']['id']);
        }

        return json_encode($news);
    }

    public function categorias($id) {
        $this->autoRender = false;
        $this->loadModel('Category');
        return json_encode($this->Category->query(
                        "SELECT id, nome as nome_pt, nome_en, nome_es FROM categories WHERE parent_id = $id "));
    }

    public function poie() {
        $this->autoRender = false;
        $this->loadModel('Poie');
        return json_encode($this->Poie->query(
                        'SELECT p.id, pc.category_id, p.nome_pt, p.nome_en, p.nome_es
			FROM poies p
			LEFT JOIN poies_categories pc ON pc.poie_id = p.id
			WHERE p.idioma_pt =  "pt"
			AND p.status = 1'));
    }

    public function poi_dados() {
        $this->autoRender = false;
        $this->loadModel('Poie');
        $poies = array();
        $poies = $this->Poie->query(
                'SELECT id, 
			nome_pt,
			nome_en,
			nome_es,
			descricao_pt,
			descricao_en,
			descricao_es,
			curiosidade_pt,
			curiosidade_en,
			curiosidade_es,
			periodo_visitacao_pt,
			periodo_visitacao_en,
			periodo_visitacao_es,
			preco_pt,
			preco_en,
			preco_es,
			endereco_pt,
			endereco_en,
			endereco_es,
			telefone,
			telefone2,
			link_site,
			latitude,
			longitude,
			fl_possui_alimentacao,
			fl_possui_area_fumante,
			fl_banheiro,
			fl_bar,
			fl_bebida_alcoolica,
			fl_briquedoteca,
			fl_cinema,
			fl_compas,
			fl_espetaculo,
			fl_estacionamento_pago,
			fl_estacionamento_gratuito,
			fl_exposicao,
			fl_fraldario,
			fl_gay_frieldly,
			fl_loja_souvenir,
			fl_noite,
			fl_restaurante,
			fl_shows_eventos,
			fl_visita_guiada,
			ic_acessibilidade,
			fl_acesso_pe,
			fl_acesso_carro,
			fl_acesso_onibus,
			fl_embarcacao,
			fl_bicicleta,
			fl_guia_audio,
			fl_esporte_areia,
			fl_aluguel_cadeira_guardasol,
			fl_esporte_aquatico,
			fl_guarda_vidas,
			fl_roteiro
			FROM poies WHERE status = 1 ');

        foreach ($poies as $key => $poie) {
            $poies[$key]['imagem'] = $this->getImagens($this->config_cidade . '/app/webroot/uploads/poies/', $poie['poies']['id']);
        }

        return json_encode($poies);
    }

    public function getImagens($path, $id) {
        $this->autoRender = false;

        $diretorio = $_SERVER['DOCUMENT_ROOT'] . $path . $id;

        $result = array();
        if (file_exists($diretorio)) {
            $arquivos = scandir($diretorio);

            if (false !== $arquivos) {
                foreach ($arquivos as $arquivo) {
                    if ('.' != $arquivo && '..' != $arquivo && 'thumbnail' != $arquivo) {
                        $obj['name'] = $arquivo;
                        $obj['size'] = filesize($diretorio . '/' . $arquivo);
                        $result[] = $obj;
                    }
                }
            }
        }

        return $result;
    }

    /* TUDO SOBRE FLORIPA */

    public function clima() {
        $this->autoRender = false;
        $this->loadModel('Climate');
        return json_encode($this->Climate->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM climates'));
    }

    public function seguro() {
        $this->autoRender = false;
        $this->loadModel('Secure');
        return json_encode($this->Secure->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM secures'));
    }

    public function historia_cidade() {
        $this->autoRender = false;
        $this->loadModel('Citystory');
        return json_encode($this->Citystory->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM citystories'));
    }

    public function leis_locais() {
        $this->autoRender = false;
        $this->loadModel('Locallaw');
        return json_encode($this->Locallaw->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM locallaws'));
    }

    public function horario_comercial() {
        $this->autoRender = false;
        $this->loadModel('Schedule');
        return json_encode($this->Schedule->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM schedules'));
    }

    public function fuso_horario() {
        $this->autoRender = false;
        $this->loadModel('Timezone');
        return json_encode($this->Timezone->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM timezones'));
    }

    public function telefonia() {
        $this->autoRender = false;
        $this->loadModel('Telephone');
        return json_encode($this->Telephone->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM telephones'));
    }

    public function telefones_uteis($idioma) {
        $this->autoRender = false;
        $this->loadModel('Usefulphone');
        if ($idioma == 'pt') {
            return json_encode($this->Usefulphone->query(
                            'SELECT id, nome_local_pt as nome, telefone FROM usefulphones WHERE status = 1 AND nome_local_pt <> "" ORDER BY nome ASC'));
        } else if ($idioma == 'en') {
            return json_encode($this->Usefulphone->query(
                            'SELECT id, nome_local_en as nome, telefone FROM usefulphones WHERE status = 1 AND nome_local_en <> "" ORDER BY nome ASC'));
        } else if ($idioma == 'es') {
            return json_encode($this->Usefulphone->query(
                            'SELECT id, nome_local_es as nome, telefone FROM usefulphones WHERE status = 1 AND nome_local_es <> "" ORDER BY nome ASC'));
        }
    }

    public function lista_operadoras() {
        $this->autoRender = false;
        $this->loadModel('Mobileoperator');
        $operadoras = $this->Mobileoperator->query(
                'SELECT * FROM mobileoperators WHERE status = 1 ORDER BY nome ASC');

        foreach ($operadoras as $key => $operadora) {
            $operadoras[$key]['mobileoperators']['img'] = $this->lista_operadoras_imagem($operadora['mobileoperators']['id']);
        }

        return json_encode($operadoras);
    }

    public function lista_operadoras_imagem($id) {
        $this->autoRender = false;

        $diretorio = $_SERVER['DOCUMENT_ROOT'] . $this->config_cidade . '/app/webroot/uploads/operadoras/' . $id;


        $result = array();
        if (file_exists($diretorio)) {
            $arquivos = scandir($diretorio);

            if (false !== $arquivos) {
                foreach ($arquivos as $arquivo) {
                    if ('.' != $arquivo && '..' != $arquivo && 'thumbnail' != $arquivo) {
                        $obj['name'] = $arquivo;
                        $obj['size'] = filesize($diretorio . '/' . $arquivo);
                        $result[] = $obj;
                    }
                }
            }
        }

        if (isset($result[0])) {
            return $result[0]['name'];
        } else {
            return '';
        }
    }

    public function tomadas() {
        $this->autoRender = false;

        $diretorio = $_SERVER['DOCUMENT_ROOT'] . $this->config_cidade . '/app/webroot/uploads/tomadas';

        $result = array();
        if (file_exists($diretorio)) {
            $arquivos = scandir($diretorio);

            if (false !== $arquivos) {
                foreach ($arquivos as $key => $arquivo) {
                    if ('.' != $arquivo && '..' != $arquivo && 'thumbnail' != $arquivo) {
                        $obj['name'] = $arquivo;
                        $obj['size'] = filesize($diretorio . '/' . $arquivo);
                        $result[$key]['tomadas'] = $obj;
                    }
                }
            }
        }

        return json_encode($result);
    }

    public function tipos_ligacao() {
        $this->autoRender = false;
        $this->loadModel('Connectiontype');
        return json_encode($this->Connectiontype->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM connectiontypes'));
    }

    public function visto_passaporte() {
        $this->autoRender = false;
        $this->loadModel('Seenpassport');
        return json_encode($this->Seenpassport->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM seenpassports'));
    }

    public function hospedagem() {
        $this->autoRender = false;
        $this->loadModel('Host');
        return json_encode($this->Host->query(
                        'SELECT * FROM hosts'));
    }

    public function alimentacao() {
        $this->autoRender = false;
        $this->loadModel('Feed');
        return json_encode($this->Feed->query(
                        'SELECT * FROM feeds'));
    }

    public function bicicleta() {
        $this->autoRender = false;
        $this->loadModel('Bicycle');
        return json_encode($this->Bicycle->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM bicycles'));
    }

    public function ape() {
        $this->autoRender = false;
        $this->loadModel('Walk');
        return json_encode($this->Walk->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM walks'));
    }

    public function taxi() {
        $this->autoRender = false;
        $this->loadModel('Cab');
        return json_encode($this->Cab->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM cabs'));
    }

    public function transporte_hidroviario() {
        $this->autoRender = false;
        $this->loadModel('Watertransport');
        return json_encode($this->Watertransport->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM watertransports'));
    }

    public function onibus() {
        $this->autoRender = false;
        $this->loadModel('Publictransport');
        return json_encode($this->Publictransport->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM publictransports'));
    }

    public function onde_fica_floripa() {
        $this->autoRender = false;
        $this->loadModel('Wherei');
        return json_encode($this->Wherei->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM whereis'));
    }

    public function aviao() {
        $this->autoRender = false;
        $this->loadModel('Plane');
        return json_encode($this->Plane->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM planes'));
    }

    public function carro() {
        $this->autoRender = false;
        $this->loadModel('Car');
        return json_encode($this->Car->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM cars'));
    }

    public function ir_onibus() {
        $this->autoRender = false;
        $this->loadModel('Gobybue');
        return json_encode($this->Gobybue->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM gobybues'));
    }

    public function embarcacao() {
        $this->autoRender = false;
        $this->loadModel('Vessel');
        return json_encode($this->Vessel->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM vessels'));
    }

    public function top10() {
        $this->autoRender = false;
        $this->loadModel('Top');
        return json_encode($this->Top->query(
                        'SELECT poie.id, poie.nome_pt, poie.nome_en, poie.nome_es FROM tops top INNER JOIN poies poie ON poie.id = top.poie_id LIMIT 0,10'));
    }

    public function questionario($idioma) {

        $this->autoRender = false;
        $this->loadModel('Question');
        if ($idioma == 'pt') {
            echo json_encode($this->Question->query(
                            "SELECT q.id, q.pergunta_pt as pergunta, q.tipo_resposta, GROUP_CONCAT(a.resposta_pt SEPARATOR ' ; ') resposta, GROUP_CONCAT( a.id
				SEPARATOR  ' ; ' ) AS id_resposta
				FROM questions AS q
				LEFT JOIN answers AS a ON a.pergunta_id = q.id 
				WHERE q.idioma_pt <> '' AND q.status = 1 AND a.idioma_pt  = 1
				GROUP BY q.id"));
        } else if ($idioma == 'en') {
            echo json_encode($this->Question->query(
                            "SELECT q.id, q.pergunta_en as pergunta, q.tipo_resposta, GROUP_CONCAT(a.resposta_en SEPARATOR ' ; ') resposta, GROUP_CONCAT( a.id
				SEPARATOR  ' ; ' ) AS id_resposta
				FROM questions AS q
				LEFT JOIN answers AS a ON a.pergunta_id = q.id 
				WHERE q.idioma_en <> '' AND q.status = 1 AND a.idioma_en  = 1
				GROUP BY q.id"));
        } else if ($idioma == 'es') {
            echo json_encode($this->Question->query(
                            "SELECT q.id, q.pergunta_es as pergunta, q.tipo_resposta, GROUP_CONCAT(a.resposta_es SEPARATOR ' ; ') resposta, GROUP_CONCAT( a.id
				SEPARATOR  ' ; ' ) AS id_resposta
				FROM questions AS q
				LEFT JOIN answers AS a ON a.pergunta_id = q.id 
				WHERE q.idioma_es <> '' AND q.status = 1 AND a.idioma_es  = 1
				GROUP BY q.id"));
        }
    }

    public function resposta() {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $dados = json_decode($this->request->data['dados'], true);
            $language = null;
            $saveSuccess = true;
            $this->loadModel('AnswersUser');
            foreach ($dados as $key => $dado) {
                if ($dado['name'] == 'idioma') {
                    $language = $dado['value'];
                } else {
                    $data['language'] = $language;
                    $data['question_id'] = $dado['name'];
                    $data['answer_id'] = $dado['value'];
                    $data['date'] = date('Y-m-d H:i:s');
                    if (!$this->AnswersUser->saveAll($data)) {
                        $saveSuccess = false;
                    }
                }
            }
            echo json_encode($saveSuccess);
        }
    }

    public function enviarContato() {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $this->loadModel('Contact');
            $this->Contact->create();
            if ($this->Contact->save($this->request->data)) {
                $data['email'] = 'vivendofloripaapp@gmail.com';
                $data['assunto'] = $this->request->data['assunto'];
                $mensagem = 'Nome: ' . $this->request->data['nome'] . '<br>';
                $mensagem .= 'email: ' . $this->request->data['email'] . '<br>';
                $mensagem .= 'Assunto: ' . $this->request->data['assunto'] . '<br>';
                $mensagem .= 'Mensagem: ' . $this->request->data['mensagem'];
                $this->enviarEmail($data, $mensagem);
                echo json_encode('true');
            } else {
                echo json_encode('false');
            }
        }
    }

    public function favoritos_pt() {
        $this->autoRender = false;
        $dados = json_decode($this->request->data['dados'], true);

        $this->loadModel('Poie');
        return json_encode($this->Poie->query(
                        'SELECT id, nome_pt AS nome
			FROM poies
			WHERE idioma_pt =  "pt"
			AND status = 1
			AND id IN (' . implode(',', $dados) . ') '));
    }

    public function favoritos_en() {
        $this->autoRender = false;
        $dados = json_decode($this->request->data['dados'], true);

        $this->loadModel('Poie');
        return json_encode($this->Poie->query(
                        'SELECT id, nome_en AS nome
			FROM poies
			WHERE idioma_en =  "en"
			AND status = 1
			AND id IN (' . implode(',', $dados) . ') '));
    }

    public function favoritos_es() {
        $this->autoRender = false;
        $dados = json_decode($this->request->data['dados'], true);

        $this->loadModel('Poie');
        return json_encode($this->Poie->query(
                        'SELECT id, nome_es AS nome
			FROM poies
			WHERE idioma_es =  "es"
			AND status = 1
			AND id IN (' . implode(',', $dados) . ') '));
    }

    public function coordenadas($idioma) {
        $this->autoRender = false;
        $this->loadModel('Poie');
        if ($idioma == 'pt') {
            $nome = 'nome_pt';
            $where = ' AND idioma_pt = "pt" ';
        } else if ($idioma == 'en') {
            $nome = 'nome_en';
            $where = ' AND idioma_en = "en" ';
        } else if ($idioma == 'es') {
            $nome = 'nome_es';
            $where = ' AND idioma_es = "es" ';
        }
        echo json_encode($this->Poie->query(
                        'SELECT id, latitude, longitude, ' . $nome . ' AS nome
			FROM poies 
			WHERE status = 1
			' . $where . ' '));
    }

    public function string() {
        $this->autoRender = false;
        $this->loadModel('Poie');
        return json_encode($this->Poie->query(
                        'SELECT id, nome, value_pt, value_en, value_es
			FROM strings'));
    }

    public function audio($nome) {
        $this->autoRender = false;

        $diretorio = $_SERVER['DOCUMENT_ROOT'] . $this->config_cidade . '/app/webroot/audio';

        $result = false;
        if (file_exists($diretorio)) {
            $arquivos = scandir($diretorio);

            if (false !== $arquivos) {
                foreach ($arquivos as $key => $arquivo) {
                    if ('.' != $arquivo && '..' != $arquivo) {
                        if ($nome == substr($arquivo, 0, strpos($arquivo, '.'))) {
                            $result = $arquivo;
                        }
                    }
                }
            }
        }

        return json_encode($result);
    }

    public function menu() {
        $this->autoRender = false;

        $this->loadModel('Menu');
        $menus = $this->Menu->query(
                'SELECT id, titulo_pt, titulo_en, titulo_es, descricao_pt, descricao_en, descricao_es, icone_cor, icone
			FROM menus 
			WHERE status = 1 
			ORDER BY ordem ASC');

        foreach ($menus as $key => $menu) {
            $menus[$key]['imagem'] = $this->getImagens($this->config_cidade . '/app/webroot/uploads/menu/', $menu['menus']['id']);
        }

        return json_encode($menus);
    }

    public function listaaudio() {
        $this->autoRender = false;

        $diretorio = $_SERVER['DOCUMENT_ROOT'] . $this->config_cidade . '/app/webroot/audio';

        $result = array();
        if (file_exists($diretorio)) {
            $arquivos = scandir($diretorio);

            if (false !== $arquivos) {
                foreach ($arquivos as $key => $arquivo) {
                    if ('.' != $arquivo && '..' != $arquivo) {
                        preg_match_all('!\d+!', $arquivo, $matches);
                        $result[] = $matches[0][0];
                    }
                }
            }
        }

        $tmp = array();
        foreach ($result as $item) {
            if (!in_array($item, $tmp)) {
                $tmp[] = $item;
            }
        }

        return json_encode($tmp);
    }
    
    public function roteiro_explicacao() {
        $this->autoRender = false;
        $this->loadModel('Selfguieded');
        return json_encode($this->Selfguieded->query(
                        'SELECT descricao_pt, descricao_en, descricao_es FROM selfguiededs'));
    }

}
