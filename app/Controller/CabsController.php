<?php
App::uses('AppController', 'Controller');
/**
 * Cabs Controller
 *
 * @property Cab $Cab
 * @property PaginatorComponent $Paginator
 */
class CabsController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cab->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Cab could not be saved. Please, try again.'));
			}
		}else{
			$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
			$this->js[] = 'lugati/editor-html';

			
			$this->Cab->recursive = 0;
			if(empty($this->Cab->find('first')))
			{
				$cabs['Cab']['id'] = '';
				$cabs['Cab']['descricao_pt'] = '';
				$cabs['Cab']['descricao_en'] = '';
				$cabs['Cab']['descricao_es'] = '';
				$this->set('cabs', $cabs);
			}else{
				$this->set('cabs', $this->Cab->find('first'));
			}			
		}
	}

}
