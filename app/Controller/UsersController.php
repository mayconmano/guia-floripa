<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');	
	
	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {

		$this->css[] = '/js/jquery.datatables/bootstrap-adapter/css/datatables';

		$this->js[] = 'jquery.datatables/jquery.datatables.min';
		$this->js[] = 'jquery.datatables/bootstrap-adapter/js/datatables';
		$this->js[] = 'lugati/users/index';

		$this->User->recursive = 0;
		$options = array('conditions' => array('User.status <> 3'));
		$this->set('users', $this->User->find('all', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		$this->css[] = '/js/jquery.icheck/skins/square/blue';

		$this->js[] = 'jquery.icheck/icheck.min';
		$this->js[] = 'jquery.parsley/src/i18n/pt';
		$this->js[] = 'jquery.parsley/dist/parsley.min';
		$this->js[] = 'jquery.parsley/src/extra/dateiso';
		$this->js[] = 'jquery.maskedinput/jquery.maskedinput';
		$this->js[] = 'lugati/users/add';

		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}

	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
  	
		$this->css[] = '/js/jquery.icheck/skins/square/blue';

		$this->js[] = 'jquery.icheck/icheck.min';
		$this->js[] = 'jquery.parsley/src/i18n/pt';
		$this->js[] = 'jquery.parsley/dist/parsley.min';
		$this->js[] = 'jquery.parsley/src/extra/dateiso';
		$this->js[] = 'jquery.maskedinput/jquery.maskedinput';
		$this->js[] = 'lugati/users/edit';

		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {			
			if(empty($this->request->data['senha']))
				unset($this->request->data['senha']);

			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect('/usuarios');
			} else {
				$this->Session->setFlash(__('Erro ao editar Usuário, tente mais tarde.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->set('user', $this->User->find('first', $options));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;

		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
		} else {
			$this->Session->setFlash(__('Erro ao deletar usuário. Tente mais tarde.'));
		}
		return $this->redirect('/usuarios');
	}


	public function status($id, $status) {

		$this->autoRender = false;

		$data['id'] = $id;
		$data['status'] = $status;

		if($this->User->save($data)) {
			if($status == 1) {
				$return['title'] = 'Sucesso';
				$return['text'] = 'Usuário ativo!';
				$return['class_name'] = 'success';
			}else{
				$return['title'] = 'Sucesso';
				$return['text'] = 'Usuário inativo!';
				$return['class_name'] = 'dark';
			}			
		}else{
			$return['title'] = 'Erro';
			$return['text'] = 'Tente mais tarde!';
			$return['class_name'] = 'red';
		}

		echo json_encode($return);
		
	}
}
