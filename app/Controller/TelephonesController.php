<?php
App::uses('AppController', 'Controller');
/**
 * Telephones Controller
 *
 * @property Telephone $Telephone
 * @property PaginatorComponent $Paginator
 */
class TelephonesController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Telephone->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The telephones could not be saved. Please, try again.'));
			}
		}else{
			$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
			$this->js[] = 'lugati/editor-html';

			
			$this->Telephone->recursive = 0;
			if(empty($this->Telephone->find('first')))
			{
				$telephones['Telephone']['id'] = '';
				$telephones['Telephone']['descricao_pt'] = '';
				$telephones['Telephone']['descricao_en'] = '';
				$telephones['Telephone']['descricao_es'] = '';
				$this->set('telephones', $telephones);
			}else{
				$this->set('telephones', $this->Telephone->find('first'));
			}			
		}
	}

}
