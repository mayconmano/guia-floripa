<?php
App::uses('AppController', 'Controller');
/**
 * planes Controller
 *
 * @property Plane $Plane
 * @property PaginatorComponent $Paginator
 */
class PlanesController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Plane->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Plane could not be saved. Please, try again.'));
			}
		}else{
			$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
			$this->js[] = 'lugati/editor-html';

			
			$this->Plane->recursive = 0;
			if(empty($this->Plane->find('first')))
			{
				$planes['Plane']['id'] = '';
				$planes['Plane']['descricao_pt'] = '';
				$planes['Plane']['descricao_en'] = '';
				$planes['Plane']['descricao_es'] = '';
				$this->set('planes', $planes);
			}else{
				$this->set('planes', $this->Plane->find('first'));
			}			
		}
	}

}
