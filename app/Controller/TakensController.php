<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'uploadHandler', array('file' => 'UploadHandler/UploadHandler.php'));
/**
 * Takens Controller
 *
 * @property Taken $Taken
 * @property PaginatorComponent $Paginator
 */
class TakensController extends AppController {


	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}	

	public function index() {

		$this->css[] = 'file-upload/upload_style';
		$this->css[] = 'file-upload/blueimp-gallery.min';
		$this->css[] = 'file-upload/jquery.fileupload';
		$this->css[] = 'file-upload/jquery.fileupload-ui';
		$this->css[] = '/js/jquery.niftymodals/css/component';		

		$this->js[] = 'file-upload/jquery.ui.widget';
		$this->js[] = 'file-upload/tmpl.min';
		$this->js[] = 'file-upload/load-image.min';
		$this->js[] = 'file-upload/canvas-to-blob.min';
		$this->js[] = 'file-upload/jquery.blueimp-gallery.min';
		$this->js[] = 'file-upload/jquery.iframe-transport';
		$this->js[] = 'file-upload/jquery.fileupload';
		$this->js[] = 'file-upload/jquery.fileupload-process';
		$this->js[] = 'file-upload/jquery.fileupload-image';
		$this->js[] = 'file-upload/jquery.fileupload-validate';
		$this->js[] = 'file-upload/jquery.fileupload-ui';	
		$this->js[] = 'jquery.niftymodals/js/jquery.modalEffects';	
		$this->js[] = 'lugati/takens/index';

		if ($this->request->is(array('post', 'put'))) {									 
			return $this->redirect(array('action' => 'index'));
		}

	}

	public function uploadHandler(){

		$this->autoRender = false;

		$options=array(
			'upload_dir' => dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/tomadas/',
			'upload_url' => $_SERVER['SERVER_NAME'].$this->config_cidade.'/uploads/tomadas/'
		);

		$upload_handler = new UploadHandler($options);

	}


}
