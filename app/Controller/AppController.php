<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::import('Vendor', 'phpmailer', array('file' => 'phpmailer/class.phpmailer.php'));
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	
	public $components = array(
        'Session',
        'Auth' => array(
        	'loginAction' => array('controller' => 'login', 'action' => 'index'),
            'loginRedirect' => array('controller' => 'home', 'action' => 'index'),
            'logoutRedirect' => array('controller' => 'login', 'action' => 'index'),            
        )
    );

    public $config_cidade = '/vivendofloripa';


    public $js = array();
	public $css = array();

	 function beforeFilter() {	 	
        $this->Auth->allow('esqueciminhasenha', 'login', 'enviarRecuperacao', 'recuperacao', 'enviarAlteracao');
        $this->set('config_cidade', '/vivendofloripa');
        $this->js[] = 'lugati/app';
    }

    public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}


	public function enviarEmail($data, $mensagem = null)
	{				
		 		
		$msg = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">";
		$msg .= "<html>";
		$msg .= "<head></head>";
		$msg .= "<body style=\"background-color:#fff;\" >";
		//$msg .= "<strong>MENSAGEM:</strong><br /><br />";
		$msg .= $mensagem;
		$msg .= "</body>";
		$msg .= "</html>";
		 
		$mail = new PHPMailer();
		$mail->Mailer     = "smtp";
		$mail->IsHTML(true);
		$mail->CharSet    = "utf-8";
		$mail->SMTPSecure = "tls";
		//$mail->SMTPDebug  = 1;
		$mail->Host       = "smtp.gmail.com";
		$mail->Port       = "587";                  
		$mail->SMTPAuth   = "true";
		$mail->Username   = "vivendofloripaapp";
		$mail->Password   = "lugati123";
		 
		$mail->From       = "vivendofloripaapp@gmail.com";
		$mail->FromName   = "Vivendo Floripa";

		$mail->AddAddress($data['email']);

		$mail->AddReplyTo("vivendofloripaapp@gmail.com", $mail->FromName);
		 
		$mail->Subject    = $data['assunto'];
		 
		$mail->Body       = $msg;
		 
		if (!$mail->Send())
		{		 
		    //echo "erro";		 
		}
		else
		{		
		    //echo "sucesso";		 
		}

	}

	public function saveLog($tabela, $tabela_id, $filename, $usuario_id){

		$this->loadModel('Log');
		$this->Log->query("INSERT INTO logs (tabela, tabela_id, file_name, usuario_id, data, tipo_log, status)
			VALUES ('".$tabela."', '".$tabela_id."', '".$filename."', '".$usuario_id."', NOW(), 2, 1)");
	}

}
