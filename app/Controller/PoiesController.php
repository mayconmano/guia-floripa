<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'wideimage', array('file' => 'wideimage/lib/WideImage.php'));
App::import('Vendor', 'phpqrcode', array('file' => 'phpqrcode/qrlib.php'));
App::import('Vendor', 'uploadHandler', array('file' => 'UploadHandler/UploadHandler.php'));
/**
 * Poies Controller
 *
 * @property Poie $Poie
 * @property PaginatorComponent $Paginator
 */
class PoiesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');


	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {

		$this->css[] = '/js/jquery.datatables/bootstrap-adapter/css/datatables';

		$this->js[] = 'jquery.datatables/jquery.datatables.min';
		$this->js[] = 'jquery.datatables/bootstrap-adapter/js/datatables';
		$this->js[] = 'lugati/pois/index';
		
		$this->Poie->recursive = -1;
		$options = array('conditions' => array('Poie.status <> 3'));
		$this->set('poies', $this->Poie->find('all', $options));
	}


/**
 * add method
 *
 * @return void
 */
	public function add() {

		$this->css[] = '/js/jquery.icheck/skins/square/blue';
		$this->css[] = '/js/bootstrap.daterangepicker/daterangepicker-bs3';
		$this->css[] = '/js/bootstrap.multiselect/css/bootstrap-multiselect';
		$this->css[] = '/js/jquery.multiselect/css/multi-select';
		//$this->css[] = '/js/bootstrap.wysihtml5/dist/bootstrap3-wysihtml5.min';
		$this->css[] = '/js/fuelux/css/fuelux';
		$this->css[] = '/js/fuelux/css/fuelux-responsive.min';
		$this->css[] = 'file-upload/upload_style';
		$this->css[] = 'file-upload/blueimp-gallery.min';
		$this->css[] = 'file-upload/jquery.fileupload';
		$this->css[] = 'file-upload/jquery.fileupload-ui';
		$this->css[] = '/js/jquery.niftymodals/css/component';
		
		$this->js[] = 'jquery.icheck/icheck.min';
		$this->js[] = 'jquery.parsley/src/i18n/pt';
		$this->js[] = 'jquery.parsley/dist/parsley.min';
		$this->js[] = 'jquery.parsley/src/extra/dateiso';		
		$this->js[] = 'bootstrap.daterangepicker/moment.min';
		$this->js[] = 'bootstrap.daterangepicker/daterangepicker';
		$this->js[] = 'jquery.maskedinput/jquery.maskedinput';
		$this->js[] = 'jquery.maskmoney/src/jquery.maskMoney';
		$this->js[] = 'jquery.maskmoney/src/jquery.maskMoney';
		$this->js[] = 'bootstrap.multiselect/js/bootstrap-multiselect';
		$this->js[] = 'jquery.multiselect/js/jquery.multi-select';
		//$this->js[] = 'bootstrap.wysihtml5/dist/wysihtml5-0.3.0';
		//$this->js[] = 'bootstrap.wysihtml5/dist/bootstrap3-wysihtml5.all.min';
		$this->js[] = 'tinymce/js/tinymce/tinymce.min';
		$this->js[] = 'fuelux/loader';
		$this->js[] = 'file-upload/jquery.ui.widget';
		$this->js[] = 'file-upload/tmpl.min';
		$this->js[] = 'file-upload/load-image.min';
		$this->js[] = 'file-upload/canvas-to-blob.min';
		$this->js[] = 'file-upload/jquery.blueimp-gallery.min';
		$this->js[] = 'file-upload/jquery.iframe-transport';
		$this->js[] = 'file-upload/jquery.fileupload';
		$this->js[] = 'file-upload/jquery.fileupload-process';
		$this->js[] = 'file-upload/jquery.fileupload-image';
		$this->js[] = 'file-upload/jquery.fileupload-validate';
		$this->js[] = 'file-upload/jquery.fileupload-ui';	
		$this->js[] = 'jquery.niftymodals/js/jquery.modalEffects';
		$this->js[] = 'http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places';		
		$this->js[] = 'jquery.geocomplete/jquery.geocomplete.min';
		$this->js[] = 'lugati/pois/add';

		$this->Poie->recursive = -1;
		if ($this->request->is('post')) {
			
			if(!isset($this->request->data['idioma_pt']))
			{
				$this->request->data['idioma_pt'] = '';
			} 
			if(!isset($this->request->data['idioma_en'])){
				$this->request->data['idioma_en'] = '';
			}
			if(!isset($this->request->data['idioma_es'])){
				$this->request->data['idioma_es'] = '';
			}				
			
			$this->Poie->create();
			if ($this->Poie->save($this->request->data)) {	

				if(isset($this->request->data['category_id']) && is_array($this->request->data['category_id'])) {
					$this->loadModel('PoiesCategory');
					foreach ($this->request->data['category_id'] as $key => $categoria) {
						$this->PoiesCategory->saveAll(array('poie_id' => $this->Poie->getLastInsertId(), 'category_id' => $categoria));
					}
				}

				$targetPath = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/uploads/poies/' . $this->Poie->getLastInsertId();
				$file = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/files';
				$thumbnail = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/uploads/poies/' . $this->Poie->getLastInsertId().'/thumbnail';

				if(!file_exists($targetPath)) {
					mkdir($targetPath, 0777, true);
				}

				if(!file_exists($thumbnail)) {
					mkdir($thumbnail, 0777, true);
				}

				if(isset($this->request->data['imagem']) && is_array($this->request->data['imagem'])){
					foreach ($this->request->data['imagem'] as $key => $imagem) {
						copy($file.'/'.$imagem, $targetPath.'/'.$imagem);
						copy($file.'/thumbnail/'.$imagem, $thumbnail.'/'.$imagem);
						unlink($file.'/'.$imagem);
						unlink($file.'/thumbnail/'.$imagem);
						$this->saveLog('poies', $this->Poie->getLastInsertId(), $imagem, $_SESSION['Auth']['User']['User']['id']);
					}
				}

			    if(isset($this->request->data['link_site']) && !empty($this->request->data['link_site'])){

					$targetPath = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/uploads/qrcode';						

					if(!file_exists($targetPath)) {
						mkdir($targetPath, 0777, true);
					}

					$filename = 'qrcode_poie_'.$this->Poie->getLastInsertId().'.png';

					QRcode::png($this->request->data['link_site'], $targetPath.'/'.$filename, 'H', 10, 2);					 

			    }


									 			
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The poie could not be saved. Please, try again.'));
			}
		}
		$categories = $this->Poie->Category->find('list', array('fields' => array('id', 'nome', 'parent_id')));		
		$this->set(compact('categories'));				

	}

	


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		$this->css[] = '/js/jquery.icheck/skins/square/blue';
		$this->css[] = '/js/bootstrap.daterangepicker/daterangepicker-bs3';
		$this->css[] = '/js/bootstrap.multiselect/css/bootstrap-multiselect';
		$this->css[] = '/js/jquery.multiselect/css/multi-select';
		//$this->css[] = '/js/bootstrap.wysihtml5/dist/bootstrap3-wysihtml5.min';
		$this->css[] = '/js/fuelux/css/fuelux';
		$this->css[] = '/js/fuelux/css/fuelux-responsive.min';
		$this->css[] = 'file-upload/upload_style';
		$this->css[] = 'file-upload/blueimp-gallery.min';
		$this->css[] = 'file-upload/jquery.fileupload';
		$this->css[] = 'file-upload/jquery.fileupload-ui';
		$this->css[] = '/js/jquery.niftymodals/css/component';
		
		$this->js[] = 'jquery.icheck/icheck.min';
		$this->js[] = 'jquery.parsley/src/i18n/pt';
		$this->js[] = 'jquery.parsley/dist/parsley.min';
		$this->js[] = 'jquery.parsley/src/extra/dateiso';
		$this->js[] = 'http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places';		
		$this->js[] = 'jquery.geocomplete/jquery.geocomplete.min';
		$this->js[] = 'bootstrap.daterangepicker/moment.min';
		$this->js[] = 'bootstrap.daterangepicker/daterangepicker';
		$this->js[] = 'jquery.maskedinput/jquery.maskedinput';
		$this->js[] = 'jquery.maskmoney/src/jquery.maskMoney';
		$this->js[] = 'jquery.maskmoney/src/jquery.maskMoney';
		$this->js[] = 'bootstrap.multiselect/js/bootstrap-multiselect';
		$this->js[] = 'jquery.multiselect/js/jquery.multi-select';
		//$this->js[] = 'bootstrap.wysihtml5/dist/wysihtml5-0.3.0';
		//$this->js[] = 'bootstrap.wysihtml5/dist/bootstrap3-wysihtml5.all.min';
		$this->js[] = 'tinymce/js/tinymce/tinymce.min';
		$this->js[] = 'fuelux/loader';
		$this->js[] = 'file-upload/jquery.ui.widget';
		$this->js[] = 'file-upload/tmpl.min';
		$this->js[] = 'file-upload/load-image.min';
		$this->js[] = 'file-upload/canvas-to-blob.min';
		$this->js[] = 'file-upload/jquery.blueimp-gallery.min';
		$this->js[] = 'file-upload/jquery.iframe-transport';
		$this->js[] = 'file-upload/jquery.fileupload';
		$this->js[] = 'file-upload/jquery.fileupload-process';
		$this->js[] = 'file-upload/jquery.fileupload-image';
		$this->js[] = 'file-upload/jquery.fileupload-validate';
		$this->js[] = 'file-upload/jquery.fileupload-ui';	
		$this->js[] = 'jquery.niftymodals/js/jquery.modalEffects';
		$this->js[] = 'lugati/pois/edit';

		$this->Poie->recursive = -1;
		if (!$this->Poie->exists($id)) {
			throw new NotFoundException(__('Invalid poie'));
		}
		if ($this->request->is(array('post', 'put'))) {		
			
			if(!isset($this->request->data['idioma_pt']))
			{
				$this->request->data['idioma_pt'] = '';
			} 
			if(!isset($this->request->data['idioma_en'])){
				$this->request->data['idioma_en'] = '';
			}
			if(!isset($this->request->data['idioma_es'])){
				$this->request->data['idioma_es'] = '';
			}				
						
			if ($this->Poie->save($this->request->data)) {
				$this->loadModel('PoiesCategory');
				$this->PoiesCategory->deleteAll(array('poie_id' => $id));	
				if(isset($this->request->data['category_id']) && is_array($this->request->data['category_id'])) {
					if(is_array($this->request->data['category_id'])){											
						foreach ($this->request->data['category_id'] as $key => $categoria) {
							$this->PoiesCategory->saveAll(array('poie_id' => $id, 'category_id' => $categoria));
						}
					}
				}
				
			    if(isset($this->request->data['link_site']) && !empty($this->request->data['link_site'])){
						
					$targetPath = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/uploads/qrcode';											


					if(!file_exists($targetPath)) {
						mkdir($targetPath, 0777, true);
					}

					$filename = 'qrcode_poie_'.$id.'.png';

					QRcode::png($this->request->data['link_site'], $targetPath.'/'.$filename, 'H', 10, 2);					 

			    }else{

					$targetPath = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/uploads/qrcode';						

					$filename = 'qrcode_poie_'.$id.'.png';
					if(file_exists($targetPath.'/'.$filename)) {
						unlink($targetPath.'/'.$filename);
					}

			    }

				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The poie could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Poie.' . $this->Poie->primaryKey => $id));
			$this->set('poie', $this->Poie->find('first', $options));
		}
		$categories = $this->Poie->Category->find('list', array('fields' => array('id', 'nome', 'parent_id')));
		$this->loadModel('PoiesCategory');
		$poie_categorias = $this->PoiesCategory->find('all', array('conditions' => array('poie_id' => $id)));
		$categoria_id = array();
		foreach ($poie_categorias as $key => $poie_categorias) {
			$categoria_id[] = $poie_categorias['PoiesCategory']['category_id'];
		}						
		$this->set(compact('categories', 'categoria_id'));		   
		
	}


/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Poie->id = $id;
		if (!$this->Poie->exists()) {
			throw new NotFoundException(__('Invalid poie'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->Poie->delete()) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
		} else {
			$this->Session->setFlash(__('The poie could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function qrcode($id) {

		$this->autoRender = false;
		echo '<img src="'.$this->config_cidade.'/uploads/qrcode/qrcode_poie_'.$id.'.png" />';

	}

	public function status($id, $status) {

		$this->autoRender = false;

		$data['id'] = $id;
		$data['status'] = $status;

		if($this->Poie->save($data)) {
			if($status == 1) {
				$return['title'] = 'Sucesso';
				$return['text'] = 'POI ativa!';
				$return['class_name'] = 'success';
			}else{
				$return['title'] = 'Sucesso';
				$return['text'] = 'POI inativa!';
				$return['class_name'] = 'dark';
			}			
		}else{
			$return['title'] = 'Erro';
			$return['text'] = 'Tente mais tarde!';
			$return['class_name'] = 'red';
		}

		echo json_encode($return);
		
	}

	public function uploadHandler(){

		$this->autoRender = false;

		$upload_handler = new UploadHandler();

	}

	public function uploadHandlerEdit($id){

		$this->autoRender = false;

		$options=array(
			'upload_dir' => dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/poies/'.$id.'/',
			'upload_url' => $_SERVER['SERVER_NAME'].$this->config_cidade.'/uploads/poies/'.$id.'/'
		);

		$upload_handler = new UploadHandler($options);

	}

}
