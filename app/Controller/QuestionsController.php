<?php
App::uses('AppController', 'Controller');
/**
 * Questions Controller
 *
 * @property Question $Question
 * @property PaginatorComponent $Paginator
 */
class QuestionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->css[] = '/js/jquery.datatables/bootstrap-adapter/css/datatables';

		$this->js[] = 'jquery.datatables/jquery.datatables.min';
		$this->js[] = 'jquery.datatables/bootstrap-adapter/js/datatables';
		$this->js[] = 'lugati/perguntas/index';

		$this->Question->recursive = 0;
		$options = array('conditions' => array('Question.status <> 3'));
		$this->set('perguntas', $this->Question->find('all', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->css[] = '/js/jquery.icheck/skins/square/blue';	
		$this->css[] = '/js/fuelux/css/fuelux';
		$this->css[] = '/js/fuelux/css/fuelux-responsive.min';

		$this->js[] = 'jquery.icheck/icheck.min';
		$this->js[] = 'jquery.parsley/src/i18n/pt';
		$this->js[] = 'jquery.parsley/dist/parsley.min';
		$this->js[] = 'jquery.parsley/src/extra/dateiso';				
		$this->js[] = 'fuelux/loader';
		$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
		$this->js[] = 'lugati/perguntas/add';

		if ($this->request->is('post')) {

			if(!isset($this->request->data['idioma_pt']))
			{
				$this->request->data['idioma_pt'] = '';
			} 
			if(!isset($this->request->data['idioma_en'])){
				$this->request->data['idioma_en'] = '';
			}
			if(!isset($this->request->data['idioma_es'])){
				$this->request->data['idioma_es'] = '';
			}	
			
			$this->Question->create();
			if ($this->Question->save($this->request->data)) {
				$this->loadModel('Answer');
				if($this->request->data['idioma_pt'] != ''){
					if(is_array($this->request->data['resposta_pt'])){						
						foreach ($this->request->data['resposta_pt'] as $key => $resposta) {
							$data[$key]['resposta_pt'] = $resposta; 
							$data[$key]['pergunta_id'] = $this->Question->getLastInsertId(); 
							$data[$key]['idioma_pt'] = 1;
						}
					}	
				}
				if($this->request->data['idioma_en'] != ''){
					if(is_array($this->request->data['resposta_en'])){						
						foreach ($this->request->data['resposta_en'] as $key => $resposta) {
							$data[$key]['resposta_en'] = $resposta; 
							$data[$key]['pergunta_id'] = $this->Question->getLastInsertId(); 
							$data[$key]['idioma_en'] = 1;
						}
					}	
				}
				if($this->request->data['idioma_es'] != ''){
					if(is_array($this->request->data['resposta_es'])){						
						foreach ($this->request->data['resposta_es'] as $key => $resposta) {
							$data[$key]['resposta_es'] = $resposta; 
							$data[$key]['pergunta_id'] = $this->Question->getLastInsertId(); 
							$data[$key]['idioma_es'] = 1;
						}
					}	
				}
				$this->Answer->saveAll($data);
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The question could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->css[] = '/js/jquery.icheck/skins/square/blue';			
		$this->css[] = '/js/fuelux/css/fuelux';
		$this->css[] = '/js/fuelux/css/fuelux-responsive.min';

		$this->js[] = 'jquery.icheck/icheck.min';
		$this->js[] = 'jquery.parsley/src/i18n/pt';
		$this->js[] = 'jquery.parsley/dist/parsley.min';
		$this->js[] = 'jquery.parsley/src/extra/dateiso';		
		$this->js[] = 'tinymce/js/tinymce/tinymce.min';
		$this->js[] = 'fuelux/loader';
		$this->js[] = 'lugati/perguntas/edit';
		
		if (!$this->Question->exists($id)) {
			throw new NotFoundException(__('Invalid question'));
		}
		if ($this->request->is(array('post', 'put'))) {
			
			if(!isset($this->request->data['idioma_pt']))
			{
				$this->request->data['idioma_pt'] = '';
			} 
			if(!isset($this->request->data['idioma_en'])){
				$this->request->data['idioma_en'] = '';
			}
			if(!isset($this->request->data['idioma_es'])){
				$this->request->data['idioma_es'] = '';
			}
			
			if ($this->Question->save($this->request->data)) {				
				
				$this->loadModel('Answer');					
				$this->Answer->deleteAll(array('pergunta_id' => $id));
				$data = array();
				if($this->request->data['idioma_pt'] != ''){
					if(is_array($this->request->data['resposta_pt'])){						
						foreach ($this->request->data['resposta_pt'] as $key => $resposta) {							
							$data[$key]['resposta_pt'] = $resposta; 
							$data[$key]['pergunta_id'] = $id; 
							$data[$key]['idioma_pt'] = 1; 
							if(isset($this->request->data['resposta_id'][$key])){
								$data[$key]['id'] = $this->request->data['resposta_id'][$key];
							}
						}
					}	
				}
				if($this->request->data['idioma_en'] != ''){
					if(is_array($this->request->data['resposta_en'])){						
						foreach ($this->request->data['resposta_en'] as $key => $resposta) {							
							$data[$key]['resposta_en'] = $resposta; 
							$data[$key]['pergunta_id'] = $id; 
							$data[$key]['idioma_en'] = 1; 
							if(isset($this->request->data['resposta_id'][$key])){
								$data[$key]['id'] = $this->request->data['resposta_id'][$key];
							}
						}
					}	
				}
				if($this->request->data['idioma_es'] != ''){
					if(is_array($this->request->data['resposta_es'])){						
						foreach ($this->request->data['resposta_es'] as $key => $resposta) {							
							$data[$key]['resposta_es'] = $resposta; 
							$data[$key]['pergunta_id'] = $id; 
							$data[$key]['idioma_es'] = 1; 
							if(isset($this->request->data['resposta_id'][$key])){
								$data[$key]['id'] = $this->request->data['resposta_id'][$key];
							}
						}
					}	
				}

				$this->Answer->saveAll($data);				
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The question could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
			$questions = $this->Question->find('first', $options);
			$this->loadModel('Answer');
			$respostas_pt = $this->Answer->find('all', array('conditions' => array('pergunta_id' => $id, 'idioma_pt' => 1)));
			$respostas_en = $this->Answer->find('all', array('conditions' => array('pergunta_id' => $id, 'idioma_en' => 1)));
			$respostas_es = $this->Answer->find('all', array('conditions' => array('pergunta_id' => $id, 'idioma_es' => 1)));
			$this->set(compact('questions', 'respostas_pt', 'respostas_en', 'respostas_es'));
		}
	}

	public function status($id, $status) {

		$this->autoRender = false;

		$data['id'] = $id;
		$data['status'] = $status;

		if($this->Question->save($data)) {
			if($status == 1) {
				$return['title'] = 'Sucesso';
				$return['text'] = 'Pergunta ativa!';
				$return['class_name'] = 'success';
			}else{
				$return['title'] = 'Sucesso';
				$return['text'] = 'Pergunta inativa!';
				$return['class_name'] = 'dark';
			}			
		}else{
			$return['title'] = 'Erro';
			$return['text'] = 'Tente mais tarde!';
			$return['class_name'] = 'red';
		}

		echo json_encode($return);
		
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Question->id = $id;
		if (!$this->Question->exists()) {
			throw new NotFoundException(__('Invalid question'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->Question->delete()) {
			$this->loadModel('Answer');					
			$this->Answer->deleteAll(array('pergunta_id' => $id));
			$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
		} else {
			$this->Session->setFlash(__('The question could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function order(){
		$this->autoRender = false;
		$ids = json_decode($_POST['id']);

		foreach ($ids as $i => $id) {
			$data = array();
			$data['id'] = $id;
			$data['ordem'] = $i+1;
			$this->Question->saveAll($data);
		}
		$this->Question->recursive = 0;
		echo json_encode($this->Question->find('all', array('order' => 'ordem ASC')));
	}

	public function relatorio($id){
		
		$dados = $this->Question->query('SELECT q.pergunta_pt, 
								a.resposta_pt, 
								(SELECT COUNT(id) FROM answers_users WHERE question_id = q.id AND answer_id = a.id) as qtd_respondidas, 
								(SELECT COUNT(id) FROM answers_users WHERE question_id = q.id) as qtd_respostas, 
								TRUNCATE(((SELECT COUNT(id) FROM answers_users WHERE question_id = q.id AND answer_id = a.id)*100)/(SELECT COUNT(id) FROM answers_users WHERE question_id = q.id),2) as porcentagem
								FROM questions q
								LEFT JOIN answers a ON a.pergunta_id = q.id
								WHERE q.id = '.$id.' ');

		$this->set('dados', $dados);

	}
}
