<?php
App::uses('AppController', 'Controller');
/**
 * Climates Controller
 *
 * @property Climate $Climate
 * @property PaginatorComponent $Paginator
 */
class SelfguiededsController extends AppController {        

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {
            
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Selfguieded->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The climate could not be saved. Please, try again.'));
			}
		}else{
			$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
			$this->js[] = 'lugati/editor-html';

			
			$this->Selfguieded->recursive = 0;
			if(empty($this->Selfguieded->find('first')))
			{
				$selfguiededs['Selfguieded']['id'] = '';
				$selfguiededs['Selfguieded']['descricao_pt'] = '';
				$selfguiededs['Selfguieded']['descricao_en'] = '';
				$selfguiededs['Selfguieded']['descricao_es'] = '';
				$this->set('Selfguieded', $selfguiededs);
			}else{                            
				$this->set('selfguiededs', $this->Selfguieded->find('first')); 
			}			
		}
	}

}
