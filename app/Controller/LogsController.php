<?php

App::uses('AppController', 'Controller');

class LogsController extends AppController {

	public $js = array();
	public $css = array();

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index()
	{	
		$this->css[] = '/js/jquery.datatables/bootstrap-adapter/css/datatables';

		$this->js[] = 'jquery.datatables/jquery.datatables.min';
		$this->js[] = 'jquery.datatables/bootstrap-adapter/js/datatables';
		$this->js[] = 'spin.min';
		$this->js[] = 'lugati/logs/index';			

		$this->set('logs', json_decode($this->getLogDados('interna'), true));

	}

	public function getLogDados($chamada = null){					

		if($chamada == null){
			$this->autoRender = false;
		}

		$logs = $this->Log->query('SELECT GROUP_CONCAT(id separator ",") id, tabela, GROUP_CONCAT(tabela_id separator ",") tabela_id, usuario_id, DATE_FORMAT(data, "%d/%m/%Y %H:%i:%s") data, restaurado, status FROM logs WHERE tipo_log = 1 GROUP BY tabela, usuario_id, status, restaurado');
		
		$data = array();
		$x = 0;		
		foreach ($logs as $key => $log) {			
			$tabelas_id = explode(',', $log[0]['tabela_id']);
			$ids = array();
			foreach (explode(',', $log[0]['id']) as $key => $log_id) {
				$ids[$tabelas_id[$key]] = $log_id; 	
			}
			
			$sql = $this->Log->query('SELECT * FROM '.$log['logs']['tabela'].' WHERE id IN ('.$log[0]['tabela_id'].') ');
			foreach ($sql as $i => $value) {
				$usuario = $this->Log->query('SELECT nome FROM users WHERE id = '.$log['logs']['usuario_id'].' ');				
				$titulo = array_values($value[$log['logs']['tabela']]);								
				$data[$x]['logId'] = $ids[$value[$log['logs']['tabela']]['id']];				
				$data[$x]['tabelaId'] = $value[$log['logs']['tabela']]['id'];				
				$data[$x]['tabelaPt'] = $this->tableNamePt($log['logs']['tabela']);				
				$data[$x]['tabelaEn'] = $log['logs']['tabela'];				
				$data[$x]['titulo'] = (strlen($titulo[1]) > 100 ? substr($titulo[1], 0, 100).'...' : $titulo[1]);				
				$data[$x]['usuario'] = $usuario[0]['users']['nome'];	
				$data[$x]['data'] = $log[0]['data'];	
				$data[$x]['restaurado'] = $log['logs']['restaurado'];	
				$data[$x]['status'] = $log['logs']['status'];	
				$x++;
			}
			$x++;
		}

		if($chamada == null){
			echo json_encode($data);
		}else{
			return json_encode($data);
		}		
	}

	public function getLogArquivos($chamada = null){

		if($chamada == null){
			$this->autoRender = false;
		}		

		$logs = $this->Log->query('SELECT GROUP_CONCAT(id separator ",") id, tabela, file_name, GROUP_CONCAT(tabela_id separator ",") tabela_id, usuario_id, DATE_FORMAT(data, "%d/%m/%Y %H:%i:%s") data, restaurado, status FROM logs WHERE tipo_log = 2 GROUP BY id');

		$data = array();
		$x = 0;		
		foreach ($logs as $key => $log) {			
			$tabelas_id = explode(',', $log[0]['tabela_id']);
			$ids = array();
			foreach (explode(',', $log[0]['id']) as $key => $log_id) {
				$ids[$tabelas_id[$key]] = $log_id; 	
			}
			
			if($log[0]['tabela_id']){
				$sql = $this->Log->query('SELECT * FROM '.$log['logs']['tabela'].' WHERE id IN ('.$log[0]['tabela_id'].') ');
				foreach ($sql as $i => $value) {
					$targetPath = $_SERVER['DOCUMENT_ROOT'].$this->config_cidade.'/app/webroot/uploads/'.$log['logs']['tabela'].'/'.$value[$log['logs']['tabela']]['id'];
					$uploadsImg = 'http://'.$_SERVER['HTTP_HOST'].$this->config_cidade.'/uploads/'.$log['logs']['tabela'].'/'.$value[$log['logs']['tabela']]['id'];				
					$tmpImg = 'http://'.$_SERVER['HTTP_HOST'].$this->config_cidade.'/tmp/'.$log['logs']['tabela'].'/'.$value[$log['logs']['tabela']]['id'];				

					$usuario = $this->Log->query('SELECT nome FROM users WHERE id = '.$log['logs']['usuario_id'].' ');				
					$titulo = array_values($value[$log['logs']['tabela']]);								
					$data[$x]['logId'] = $ids[$value[$log['logs']['tabela']]['id']];				
					$data[$x]['tabelaId'] = $value[$log['logs']['tabela']]['id'];				
					$data[$x]['tabelaPt'] = $this->tableNamePt($log['logs']['tabela']);				
					$data[$x]['tabelaEn'] = $log['logs']['tabela'];				
					$data[$x]['file_name'] = $log['logs']['file_name'];
					$data[$x]['file_path'] = (file_exists($targetPath.'/'.$log['logs']['file_name']) == 1 ? $uploadsImg.'/'.$log['logs']['file_name'] : $tmpImg.'/'.$log['logs']['file_name']);
					$data[$x]['titulo'] = (strlen($titulo[1]) > 100 ? substr($titulo[1], 0, 100).'...' : $titulo[1]);				
					$data[$x]['usuario'] = $usuario[0]['users']['nome'];	
					$data[$x]['data'] = $log[0]['data'];	
					$data[$x]['restaurado'] = $log['logs']['restaurado'];	
					$data[$x]['status'] = $log['logs']['status'];	
					$x++;
				}
			}else{
				$targetPath = $_SERVER['DOCUMENT_ROOT'].$this->config_cidade.'/app/webroot/uploads/'.$log['logs']['tabela'];
				$uploadsImg = 'http://'.$_SERVER['HTTP_HOST'].$this->config_cidade.'/uploads/'.$log['logs']['tabela'];				
				$tmpImg = 'http://'.$_SERVER['HTTP_HOST'].$this->config_cidade.'/tmp/'.$log['logs']['tabela'];				

				$usuario = $this->Log->query('SELECT nome FROM users WHERE id = '.$log['logs']['usuario_id'].' ');				
				$titulo = $log['logs']['tabela'];								
				$data[$x]['logId'] = $log[0]['id'];				
				$data[$x]['tabelaId'] = 0;				
				$data[$x]['tabelaPt'] = $log['logs']['tabela'];				
				$data[$x]['tabelaEn'] = $log['logs']['tabela'];				
				$data[$x]['file_name'] = $log['logs']['file_name'];
				$data[$x]['file_path'] = (file_exists($targetPath.'/'.$log['logs']['file_name']) == 1 ? $uploadsImg.'/'.$log['logs']['file_name'] : $tmpImg.'/'.$log['logs']['file_name']);
				$data[$x]['titulo'] = (strlen($titulo) > 100 ? substr($titulo, 0, 100).'...' : $titulo);				
				$data[$x]['usuario'] = $usuario[0]['users']['nome'];	
				$data[$x]['data'] = $log[0]['data'];	
				$data[$x]['restaurado'] = $log['logs']['restaurado'];	
				$data[$x]['status'] = $log['logs']['status'];	
				$x++;
			}
			$x++;
		}

		if($chamada == null){
			echo json_encode($data);
		}else{
			return json_encode($data);
		}		
		
	}

	public function restaurar($table, $id, $id_log){
		$this->Log->query('UPDATE '.$table.' SET status = 1 WHERE id = '.$id.' ');
		$this->Log->query('UPDATE logs SET restaurado = 1 WHERE id = '.$id_log.' ');
		return $this->redirect(array('action' => 'index'));
	}

	public function restaurarArquivo($table, $id, $file_name, $id_log){		
		
		$this->Log->query('UPDATE logs SET restaurado = 1 WHERE id = '.$id_log.' ');

		$targetPath = $_SERVER['DOCUMENT_ROOT'].$this->config_cidade.'/app/webroot/uploads/'.$table.'/'.$id.'/'.$file_name;		
		$thumbnailPath = $_SERVER['DOCUMENT_ROOT'].$this->config_cidade.'/app/webroot/uploads/'.$table.'/'.$id.'/thumbnail/'.$file_name;		

		$fileTmpPath = $_SERVER['DOCUMENT_ROOT'].$this->config_cidade.'/app/webroot/tmp/'.$table.'/'.$id.'/'.$file_name;		
		$thumbnailTmpPath = $_SERVER['DOCUMENT_ROOT'].$this->config_cidade.'/app/webroot/tmp/'.$table.'/'.$id.'/thumbnail/'.$file_name;			

		if(is_file($fileTmpPath)){			
			rename($fileTmpPath, $targetPath);
		}

		if(is_file($thumbnailTmpPath)){
			rename($thumbnailTmpPath, $thumbnailPath);
		}

		return $this->redirect(array('action' => 'index'));
	}

	public function tableNamePt($tableEn){		

		$data['agendas'] = 'Agenda';
		$data['answers'] = 'Resposta';
		$data['bicycles'] = 'Bicicleta';
		$data['cabs'] = 'Táxi';
		$data['cars'] = 'Carro';
		$data['categories'] = 'Categoria';
		$data['citystories'] = 'Histórias da cidade';
		$data['climates'] = 'Clima';
		$data['connectiontypes'] = 'Tipo de conexão';
		$data['contacts'] = 'Contato';
		$data['feeds'] = 'Alimentação';
		$data['gobybues'] = 'Ônibus';
		$data['hosts'] = 'Hospedagem';
		$data['locallaws'] = 'Leis locais';
		$data['menus'] = 'Menu';
		$data['mobileoperators'] = 'Operadora de celular';
		$data['news'] = 'Notícia';
		$data['planes'] = 'Avião';
		$data['poies'] = 'POI';
		$data['publictransports'] = 'Transporte publico';
		$data['questions'] = 'Pergunta';
		$data['schedules'] = 'Horário';
		$data['secretaries'] = 'Secretária';
		$data['secures'] = 'Segurança';
		$data['seenpassports'] = 'Visto e passaporte';
		$data['takens'] = 'Tomada';
		$data['telephones'] = 'Telefone';
		$data['timezones'] = 'Fuso horário';
		$data['tops'] = 'Top';
		$data['usefulphones'] = 'Telefones úteis';
		$data['users'] = 'Usuário';
		$data['versions'] = 'Versão';
		$data['vessels'] = 'Navio';
		$data['walks'] = 'A pé';
		$data['watertransports'] = 'Transporte Hidroviário';
		$data['whereis'] = 'Onde fica floripa no mundo';
		
		$namePT = '';		

		foreach ($data as $key => $value) {
			if($key == $tableEn){
				$namePT = $value;
			}
		}

		if($namePT == ''){			
			$this->enviarEmail(array('email' => 'maycon@lugati.com.br', 'assunto' => 'BUG vivnedo floripa'), 'A tabela '.$tableEn.' não foi traduzida no sistema de log!');
			return $tableEn;
		}else{
			return $namePT;
		}		

	}
	
}
