<?php
App::uses('AppController', 'Controller');
App::uses('CakeTime', 'Utility');
App::import('Vendor', 'uploadHandler', array('file' => 'UploadHandler/UploadHandler.php'));

/**
 * News Controller
 *
 * @property News $News
 * @property PaginatorComponent $Paginator
 */
class NewsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {

		$this->css[] = '/js/jquery.datatables/bootstrap-adapter/css/datatables';

		$this->js[] = 'jquery.datatables/jquery.datatables.min';
		$this->js[] = 'jquery.datatables/bootstrap-adapter/js/datatables';
		$this->js[] = 'lugati/news/index';

		$this->News->recursive = 0;
		$options = array('conditions' => array('News.status <> 3'));
		$this->set('news', $this->News->find('all', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		
		$this->css[] = '/js/jquery.icheck/skins/square/blue';		
		$this->css[] = 'file-upload/upload_style';
		$this->css[] = 'file-upload/blueimp-gallery.min';
		$this->css[] = 'file-upload/jquery.fileupload';
		$this->css[] = 'file-upload/jquery.fileupload-ui';
		$this->css[] = '/js/jquery.niftymodals/css/component';			

		$this->js[] = 'jquery.icheck/icheck.min';
		$this->js[] = 'jquery.parsley/src/i18n/pt';
		$this->js[] = 'jquery.parsley/dist/parsley.min';
		$this->js[] = 'jquery.parsley/src/extra/dateiso';
		$this->js[] = 'file-upload/jquery.ui.widget';
		$this->js[] = 'file-upload/tmpl.min';
		$this->js[] = 'file-upload/load-image.min';
		$this->js[] = 'file-upload/canvas-to-blob.min';
		$this->js[] = 'file-upload/jquery.blueimp-gallery.min';
		$this->js[] = 'file-upload/jquery.iframe-transport';
		$this->js[] = 'file-upload/jquery.fileupload';
		$this->js[] = 'file-upload/jquery.fileupload-process';
		$this->js[] = 'file-upload/jquery.fileupload-image';
		$this->js[] = 'file-upload/jquery.fileupload-validate';
		$this->js[] = 'file-upload/jquery.fileupload-ui';	
		$this->js[] = 'jquery.niftymodals/js/jquery.modalEffects';	
		$this->js[] = 'tinymce/js/tinymce/tinymce.min';			
		$this->js[] = 'lugati/news/add';

		if ($this->request->is('post')) {
			
			if(!empty($this->request->data['data_publicacao']))
			{
				$data_publicacao = explode('/', $this->request->data['data_publicacao']);			
				$this->request->data['data_publicacao'] = $data_publicacao[2].'-'.$data_publicacao[1].'-'.$data_publicacao[0];			
			}
			if(!empty($this->request->data['data_expiracao']))
			{
				$data_expiracao = explode('/', $this->request->data['data_expiracao']);			
				$this->request->data['data_expiracao'] = $data_expiracao[2].'-'.$data_expiracao[1].'-'.$data_expiracao[0];			
			}
			$this->News->create();
			if ($this->News->save($this->request->data)) {

				if(is_array($this->request->data['imagem'])) {

					$targetPath = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/uploads/news/' . $this->News->getLastInsertId();
					$file = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/files';
					$thumbnail = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/uploads/news/' . $this->News->getLastInsertId().'/thumbnail';						

					if(!file_exists($targetPath)) {
						mkdir($targetPath, 0777, true);
					}

					if(!file_exists($thumbnail)) {
						mkdir($thumbnail, 0777, true);
					}

					foreach ($this->request->data['imagem'] as $key => $imagem) {
						copy($file.'/'.$imagem, $targetPath.'/'.$imagem);
						copy($file.'/thumbnail/'.$imagem, $thumbnail.'/'.$imagem);
						unlink($file.'/'.$imagem);
						unlink($file.'/thumbnail/'.$imagem);

						$this->saveLog('news', $this->News->getLastInsertId(), $imagem, $_SESSION['Auth']['User']['User']['id']);
					}
				}
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The news could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		$this->css[] = '/js/jquery.icheck/skins/square/blue';		
		$this->css[] = 'file-upload/upload_style';
		$this->css[] = 'file-upload/blueimp-gallery.min';
		$this->css[] = 'file-upload/jquery.fileupload';
		$this->css[] = 'file-upload/jquery.fileupload-ui';
		$this->css[] = '/js/jquery.niftymodals/css/component';
		//$this->css[] = '/js/bootstrap.wysihtml5/dist/bootstrap3-wysihtml5.min';			

		$this->js[] = 'jquery.icheck/icheck.min';
		$this->js[] = 'jquery.parsley/src/i18n/pt';
		$this->js[] = 'jquery.parsley/dist/parsley.min';
		$this->js[] = 'jquery.parsley/src/extra/dateiso';
		$this->js[] = 'file-upload/jquery.ui.widget';
		$this->js[] = 'file-upload/tmpl.min';
		$this->js[] = 'file-upload/load-image.min';
		$this->js[] = 'file-upload/canvas-to-blob.min';
		$this->js[] = 'file-upload/jquery.blueimp-gallery.min';
		$this->js[] = 'file-upload/jquery.iframe-transport';
		$this->js[] = 'file-upload/jquery.fileupload';
		$this->js[] = 'file-upload/jquery.fileupload-process';
		$this->js[] = 'file-upload/jquery.fileupload-image';
		$this->js[] = 'file-upload/jquery.fileupload-validate';
		$this->js[] = 'file-upload/jquery.fileupload-ui';	
		$this->js[] = 'jquery.niftymodals/js/jquery.modalEffects';	
		$this->js[] = 'tinymce/js/tinymce/tinymce.min';
		//$this->js[] = 'bootstrap.wysihtml5/dist/wysihtml5-0.3.0';
		//$this->js[] = 'bootstrap.wysihtml5/dist/bootstrap3-wysihtml5.all.min';
		$this->js[] = 'lugati/news/edit';

		if (!$this->News->exists($id)) {
			throw new NotFoundException(__('Invalid news'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if(!empty($this->request->data['data_publicacao']))
			{
				$data_publicacao = explode('/', $this->request->data['data_publicacao']);			
				$this->request->data['data_publicacao'] = $data_publicacao[2].'-'.$data_publicacao[1].'-'.$data_publicacao[0];
			}	
			if(!empty($this->request->data['data_expiracao']))
			{
				$data_expiracao = explode('/', $this->request->data['data_expiracao']);			
				$this->request->data['data_expiracao'] = $data_expiracao[2].'-'.$data_expiracao[1].'-'.$data_expiracao[0];			
			}		
			if ($this->News->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The news could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('News.' . $this->News->primaryKey => $id));
			$this->set('news', $this->News->find('first', $options));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->News->id = $id;
		if (!$this->News->exists()) {
			throw new NotFoundException(__('Invalid news'));
		}
				
		if ($this->News->delete()) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
		} else {
			$this->Session->setFlash(__('The news could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function status($id, $status) {

		$this->autoRender = false;

		$data['id'] = $id;
		$data['status'] = $status;

		if($this->News->save($data)) {
			if($status == 1) {
				$return['title'] = 'Sucesso';
				$return['text'] = 'Notícia ativa!';
				$return['class_name'] = 'success';
			}else{
				$return['title'] = 'Sucesso';
				$return['text'] = 'Notícia inativa!';
				$return['class_name'] = 'dark';
			}			
		}else{
			$return['title'] = 'Erro';
			$return['text'] = 'Tente mais tarde!';
			$return['class_name'] = 'red';
		}

		echo json_encode($return);
		
	}

	public function uploadHandler(){

		$this->autoRender = false;

		$upload_handler = new UploadHandler();

	}

	public function uploadHandlerEdit($id){

		$this->autoRender = false;

		$options=array(
			'upload_dir' => dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/news/'.$id.'/',
			'upload_url' => $_SERVER['SERVER_NAME'].$this->config_cidade.'/uploads/news/'.$id.'/'
		);

		$upload_handler = new UploadHandler($options);

	}
	
}
