<?php
App::uses('AppController', 'Controller');
/**
 * Walks Controller
 *
 * @property Walk $Walk
 * @property PaginatorComponent $Paginator
 */
class WalksController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Walk->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Walk could not be saved. Please, try again.'));
			}
		}else{
			$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
			$this->js[] = 'lugati/editor-html';

			
			$this->Walk->recursive = 0;
			if(empty($this->Walk->find('first')))
			{
				$walks['Walk']['id'] = '';
				$walks['Walk']['descricao_pt'] = '';
				$walks['Walk']['descricao_en'] = '';
				$walks['Walk']['descricao_es'] = '';
				$this->set('walks', $walks);
			}else{
				$this->set('walks', $this->Walk->find('first'));
			}			
		}
	}

}
