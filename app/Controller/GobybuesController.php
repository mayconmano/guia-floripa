<?php
App::uses('AppController', 'Controller');
/**
 * Gobybues Controller
 *
 * @property Gobybue $Gobybue
 * @property PaginatorComponent $Paginator
 */
class GobybuesController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Gobybue->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Gobybue could not be saved. Please, try again.'));
			}
		}else{
			$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
			$this->js[] = 'lugati/editor-html';

			
			$this->Gobybue->recursive = 0;
			if(empty($this->Gobybue->find('first')))
			{
				$gobybues['Gobybue']['id'] = '';
				$gobybues['Gobybue']['descricao_pt'] = '';
				$gobybues['Gobybue']['descricao_en'] = '';
				$gobybues['Gobybue']['descricao_es'] = '';
				$this->set('gobybues', $gobybues);
			}else{
				$this->set('gobybues', $this->Gobybue->find('first'));
			}			
		}
	}

}
