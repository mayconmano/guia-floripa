<?php
App::uses('AppController', 'Controller');
/**
 * Connectiontypes Controller
 *
 * @property Connectiontype $Connectiontype
 * @property PaginatorComponent $Paginator
 */
class ConnectiontypesController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Connectiontype->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The connectiontypes could not be saved. Please, try again.'));
			}
		}else{
			$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
			$this->js[] = 'lugati/editor-html';

			
			$this->Connectiontype->recursive = 0;
			if(empty($this->Connectiontype->find('first')))
			{
				$connectiontypes['Connectiontype']['id'] = '';
				$connectiontypes['Connectiontype']['descricao_pt'] = '';
				$connectiontypes['Connectiontype']['descricao_en'] = '';
				$connectiontypes['Connectiontype']['descricao_es'] = '';
				$this->set('connectiontypes', $connectiontypes);
			}else{
				$this->set('connectiontypes', $this->Connectiontype->find('first'));
			}			
		}
	}

}
