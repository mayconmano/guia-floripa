<?php
App::uses('AppController', 'Controller');

class LoginController extends AppController {

	public function index()
	{	
		$this->layout = "login";

	}	

	public function esqueciMinhaSenha()
	{
		$this->layout = "login";
	}

	public function login() {
		
		if ($this->request->is('post')) {						
		    $this->request->data['senha'] = AuthComponent::password($this->request->data['senha']);
		    $this->loadModel('User');
		    $usuario = $this->User->find('first', array(
	        	'conditions' => array(
		            'email' => $this->request->data['email'],
		            'senha' => $this->request->data['senha']
	    		)
	        )); 

		    if ($this->Auth->login($usuario)) {
		        return $this->redirect('/home');
		    } else {
		    	$this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><div class="icon"><i class="fa fa-times-circle"></i></div><strong>Erro!</strong> E-mail ou senha invalido(s).</div>'));
		    	return $this->redirect('/');
		    			    
		    }
		}	
	}	

	public function logout() {
		$this->Session->destroy();
	    return $this->redirect($this->Auth->logout());
	}
	
	public function enviarRecuperacao()
	{
		
		if ($this->request->is('post')) {
			$this->loadModel('User');
			$this->loadModel('Recovery');

			$user = $this->User->find('first', array(
				'conditions' => array(
					'email' => $this->request->data['email']
				)
			));

			if(!empty($user))
			{
				$data_recovery = array();
				$data_recovery['email'] = $this->request->data['email'];
				$data_recovery['chave'] = sha1(uniqid( mt_rand(), true));				
				$this->Recovery->save($data_recovery);
				$data_recovery['assunto'] = "Recuperação de senha";
				$mensagem = "Olá ".$user['User']['nome']."<br> Visite este link http://".$_SERVER["HTTP_HOST"].$this->config_cidade."/recuperacao?email=".$data_recovery['email']."&chave=".$data_recovery['chave'];
				$this->enviarEmail($data_recovery, $mensagem);
			}
			else
			{
				echo 'email_nao_existe';
			}
			
		}


		$this->autoRender = false;
	}

	public function recuperacao()
	{
		
		$this->layout = "login";
		
		$email = $this->params['url']['email'];
		$chave = $this->params['url']['chave'];	

		$this->loadModel('Recovery');
		$verify_recovery = $this->Recovery->find('first', array(
			'conditions' => array(
				'email' => $email,
				'chave' => $chave
			)
		));

		if(!empty($verify_recovery))
		{			
			$this->Recovery->delete($verify_recovery['Recovery']['id']);
			$this->set('sucesso', true);
		}
		else
		{
			$this->set('sucesso', false);
		}

	}

	public function enviarAlteracao()
	{

		if ($this->request->is('post')) {
			$this->loadModel('User');
			$this->loadModel('Recovery');

			$user = $this->User->find('first', array(
				'conditions' => array(
					'email' => $this->request->data['email']
				)
			));

			if(!empty($user))			
			{
				$salvar = array();
				$salvar['senha'] = $this->request->data['senha'];
				$this->User->id = $user['User']['id'];
				if($this->User->save($salvar))
				{
					$data_user = array();
					$data_user['email'] = $this->request->data['email'];								
					$mensagem = "Olá ".$user['User']['nome']."<br>Sua senha foi alterada com sucesso!";
					$this->enviarEmail($data_user, $mensagem);					
				}
				else
				{
					echo 'erro_salvar';	
				}
				
			}
			else
			{
				echo 'user_nao_existe';
			}
						
		}


		$this->autoRender = false;

	}


}
