<?php
App::uses('AppController', 'Controller');
/**
 * Citystories Controller
 *
 * @property Citystory $Citystory
 * @property PaginatorComponent $Paginator
 */
class CitystoriesController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Citystory->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The citystories could not be saved. Please, try again.'));
			}
		}else{
			$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
			$this->js[] = 'lugati/editor-html';

			
			$this->Citystory->recursive = 0;
			if(empty($this->Citystory->find('first')))
			{
				$citystories['Citystory']['id'] = '';
				$citystories['Citystory']['descricao_pt'] = '';
				$citystories['Citystory']['descricao_en'] = '';
				$citystories['Citystory']['descricao_es'] = '';
				$this->set('citystories', $citystories);
			}else{
				$this->set('citystories', $this->Citystory->find('first'));
			}			
		}
	}

}
