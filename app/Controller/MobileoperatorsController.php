<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'uploadHandler', array('file' => 'UploadHandler/UploadHandler.php'));
/**
 * Mobileoperators Controller
 *
 * @property Mobileoperator $Mobileoperator
 * @property PaginatorComponent $Paginator
 */
class MobileoperatorsController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {

		$this->css[] = '/js/jquery.datatables/bootstrap-adapter/css/datatables';

		$this->js[] = 'jquery.datatables/jquery.datatables.min';
		$this->js[] = 'jquery.datatables/bootstrap-adapter/js/datatables';
		$this->js[] = 'lugati/mobileoperators/index';

		$this->Mobileoperator->recursive = 0;
		$options = array('conditions' => array('Mobileoperator.status <> 3'));
		$this->set('mobileoperators', $this->Mobileoperator->find('all', $options));
	}


/**
 * add method
 *
 * @return void
 */
	public function add() {

		$this->css[] = '/js/jquery.icheck/skins/square/blue';
		$this->css[] = 'file-upload/upload_style';
		$this->css[] = 'file-upload/blueimp-gallery.min';
		$this->css[] = 'file-upload/jquery.fileupload';
		$this->css[] = 'file-upload/jquery.fileupload-ui';
		$this->css[] = '/js/jquery.niftymodals/css/component';			

		$this->js[] = 'jquery.icheck/icheck.min';
		$this->js[] = 'file-upload/jquery.ui.widget';
		$this->js[] = 'file-upload/tmpl.min';
		$this->js[] = 'file-upload/load-image.min';
		$this->js[] = 'file-upload/canvas-to-blob.min';
		$this->js[] = 'file-upload/jquery.blueimp-gallery.min';
		$this->js[] = 'file-upload/jquery.iframe-transport';
		$this->js[] = 'file-upload/jquery.fileupload';
		$this->js[] = 'file-upload/jquery.fileupload-process';
		$this->js[] = 'file-upload/jquery.fileupload-image';
		$this->js[] = 'file-upload/jquery.fileupload-validate';
		$this->js[] = 'file-upload/jquery.fileupload-ui';	
		$this->js[] = 'jquery.niftymodals/js/jquery.modalEffects';	
		$this->js[] = 'lugati/mobileoperators/add';	

		if ($this->request->is('post')) {			
			$this->Mobileoperator->create();
			if ($this->Mobileoperator->save($this->request->data)) {	
				if(is_array($this->request->data['imagem'])) {

					$targetPath = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/uploads/mobileoperators/' . $this->Mobileoperator->getLastInsertId();
					$file = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/files';
					$thumbnail = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/uploads/mobileoperators/' . $this->Mobileoperator->getLastInsertId().'/thumbnail';						

					if(!file_exists($targetPath)) {
						mkdir($targetPath, 0777, true);
					}

					if(!file_exists($thumbnail)) {
						mkdir($thumbnail, 0777, true);
					}

					foreach ($this->request->data['imagem'] as $key => $imagem) {
						copy($file.'/'.$imagem, $targetPath.'/'.$imagem);
						copy($file.'/thumbnail/'.$imagem, $thumbnail.'/'.$imagem);
						unlink($file.'/'.$imagem);
						unlink($file.'/thumbnail/'.$imagem);
						$this->saveLog('mobileoperators', $this->Mobileoperator->getLastInsertId(), $imagem, $_SESSION['Auth']['User']['User']['id']);
					}
				}			
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mobileoperator could not be saved. Please, try again.'));
			}
		}
		unset($_SESSION['operadoras']);
	}

	
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		$this->css[] = '/js/jquery.icheck/skins/square/blue';		
		$this->css[] = 'file-upload/upload_style';
		$this->css[] = 'file-upload/blueimp-gallery.min';
		$this->css[] = 'file-upload/jquery.fileupload';
		$this->css[] = 'file-upload/jquery.fileupload-ui';
		$this->css[] = '/js/jquery.niftymodals/css/component';		

		$this->js[] = 'jquery.icheck/icheck.min';
		$this->js[] = 'file-upload/jquery.ui.widget';
		$this->js[] = 'file-upload/tmpl.min';
		$this->js[] = 'file-upload/load-image.min';
		$this->js[] = 'file-upload/canvas-to-blob.min';
		$this->js[] = 'file-upload/jquery.blueimp-gallery.min';
		$this->js[] = 'file-upload/jquery.iframe-transport';
		$this->js[] = 'file-upload/jquery.fileupload';
		$this->js[] = 'file-upload/jquery.fileupload-process';
		$this->js[] = 'file-upload/jquery.fileupload-image';
		$this->js[] = 'file-upload/jquery.fileupload-validate';
		$this->js[] = 'file-upload/jquery.fileupload-ui';	
		$this->js[] = 'jquery.niftymodals/js/jquery.modalEffects';	
		$this->js[] = 'lugati/mobileoperators/edit';

		if (!$this->Mobileoperator->exists($id)) {
			throw new NotFoundException(__('Invalid mobileoperator'));
		}
		if ($this->request->is(array('post', 'put'))) {			
			
			if ($this->Mobileoperator->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mobileoperator could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Mobileoperator.' . $this->Mobileoperator->primaryKey => $id));
			$this->set('mobileoperators', $this->Mobileoperator->find('first', $options));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Mobileoperator->id = $id;
		if (!$this->Mobileoperator->exists()) {
			throw new NotFoundException(__('Invalid mobileoperator'));
		}
		
		if ($this->Mobileoperator->delete()) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
		} else {
			$this->Session->setFlash(__('The mobileoperator could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function status($id, $status) {

		$this->autoRender = false;

		$data['id'] = $id;
		$data['status'] = $status;

		if($this->Mobileoperator->save($data)) {
			if($status == 1) {
				$return['title'] = 'Sucesso';
				$return['text'] = 'Operadora ativa!';
				$return['class_name'] = 'success';
			}else{
				$return['title'] = 'Sucesso';
				$return['text'] = 'Operadora inativa!';
				$return['class_name'] = 'dark';
			}			
		}else{
			$return['title'] = 'Erro';
			$return['text'] = 'Tente mais tarde!';
			$return['class_name'] = 'red';
		}

		echo json_encode($return);
		
	}

	public function uploadHandler(){

		$this->autoRender = false;

		$upload_handler = new UploadHandler();

	}

	public function uploadHandlerEdit($id){

		$this->autoRender = false;

		$options=array(
			'upload_dir' => dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/mobileoperators/'.$id.'/',
			'upload_url' => $_SERVER['SERVER_NAME'].$this->config_cidade.'/uploads/mobileoperators/'.$id.'/'
		);

		$upload_handler = new UploadHandler($options);

	}
}
