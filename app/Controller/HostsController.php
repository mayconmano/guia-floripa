<?php
App::uses('AppController', 'Controller');
/**
 * Hosts Controller
 *
 * @property Host $Host
 * @property PaginatorComponent $Paginator
 */
class HostsController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Host->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The host could not be saved. Please, try again.'));
			}
		}else{
			
			$this->Host->recursive = 0;
			if(empty($this->Host->find('first')))
			{
				$hosts['Host']['id'] = '';
				$hosts['Host']['link'] = '';				
				$this->set('hosts', $hosts);
			}else{
				$this->set('hosts', $this->Host->find('first'));
			}			
		}
	}

}
