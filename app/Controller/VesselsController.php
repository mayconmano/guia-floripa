<?php
App::uses('AppController', 'Controller');
/**
 * Vessels Controller
 *
 * @property Vessel $Vessel
 * @property PaginatorComponent $Paginator
 */
class VesselsController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Vessel->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Vessel could not be saved. Please, try again.'));
			}
		}else{
			$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
			$this->js[] = 'lugati/editor-html';

			
			$this->Vessel->recursive = 0;
			if(empty($this->Vessel->find('first')))
			{
				$vessels['Vessel']['id'] = '';
				$vessels['Vessel']['descricao_pt'] = '';
				$vessels['Vessel']['descricao_en'] = '';
				$vessels['Vessel']['descricao_es'] = '';
				$this->set('vessels', $vessels);
			}else{
				$this->set('vessels', $this->Vessel->find('first'));
			}			
		}
	}

}
