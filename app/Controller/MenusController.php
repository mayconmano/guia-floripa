<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'uploadHandler', array('file' => 'UploadHandler/UploadHandler.php'));
/**
 * Menus Controller
 *
 * @property Menu $Menu
 * @property PaginatorComponent $Paginator
 */
class MenusController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->css[] = '/js/jquery.datatables/bootstrap-adapter/css/datatables';

		$this->js[] = 'jquery.datatables/jquery.datatables.min';
		$this->js[] = 'jquery.datatables/bootstrap-adapter/js/datatables';
		$this->js[] = 'lugati/menus/index';

		$this->Menu->recursive = 0;
		$options = array('conditions' => array('Menu.status <> 3'));
		$this->set('menus', $this->Menu->find('all', $options));
	}


/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->css[] = '/js/jquery.icheck/skins/square/blue';		
		$this->css[] = '/js/fuelux/css/fuelux';
		$this->css[] = '/js/fuelux/css/fuelux-responsive.min';
		$this->css[] = '/js/colorpicker/css/colorpicker';
		$this->css[] = 'file-upload/upload_style';
		$this->css[] = 'file-upload/blueimp-gallery.min';
		$this->css[] = 'file-upload/jquery.fileupload';
		$this->css[] = 'file-upload/jquery.fileupload-ui';
		$this->css[] = '/js/jquery.niftymodals/css/component';	

		$this->js[] = 'jquery.icheck/icheck.min';
		$this->js[] = 'jquery.parsley/src/i18n/pt';
		$this->js[] = 'jquery.parsley/dist/parsley.min';
		$this->js[] = 'jquery.parsley/src/extra/dateiso';		
		$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
		$this->js[] = 'fuelux/loader';
		$this->js[] = 'colorpicker/js/colorpicker';	
		$this->js[] = 'file-upload/jquery.ui.widget';
		$this->js[] = 'file-upload/tmpl.min';
		$this->js[] = 'file-upload/load-image.min';
		$this->js[] = 'file-upload/canvas-to-blob.min';
		$this->js[] = 'file-upload/jquery.blueimp-gallery.min';
		$this->js[] = 'file-upload/jquery.iframe-transport';
		$this->js[] = 'file-upload/jquery.fileupload';
		$this->js[] = 'file-upload/jquery.fileupload-process';
		$this->js[] = 'file-upload/jquery.fileupload-image';
		$this->js[] = 'file-upload/jquery.fileupload-validate';
		$this->js[] = 'file-upload/jquery.fileupload-ui';		
		$this->js[] = 'jquery.niftymodals/js/jquery.modalEffects';
		$this->js[] = 'lugati/menus/add';

		if ($this->request->is('post')) {

			if(!isset($this->request->data['idioma_pt']))
			{
				$this->request->data['idioma_pt'] = '';
			} 
			if(!isset($this->request->data['idioma_en'])){
				$this->request->data['idioma_en'] = '';
			}
			if(!isset($this->request->data['idioma_es'])){
				$this->request->data['idioma_es'] = '';
			}	

			$this->Menu->create();
			if ($this->Menu->save($this->request->data)) {
				if(is_array($this->request->data['imagem'])) {

					$targetPath = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/uploads/menus/' . $this->Menu->getLastInsertId();
					$file = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/files';
					$thumbnail = $_SERVER['DOCUMENT_ROOT'] .$this->config_cidade. '/app/webroot/uploads/menus/' . $this->Menu->getLastInsertId().'/thumbnail';						

					if(!file_exists($targetPath)) {
						mkdir($targetPath, 0777, true);
					}

					if(!file_exists($thumbnail)) {
						mkdir($thumbnail, 0777, true);
					}

					foreach ($this->request->data['imagem'] as $key => $imagem) {
						copy($file.'/'.$imagem, $targetPath.'/'.$imagem);
						copy($file.'/thumbnail/'.$imagem, $thumbnail.'/'.$imagem);
						unlink($file.'/'.$imagem);
						unlink($file.'/thumbnail/'.$imagem);
						$this->saveLog('menus', $this->Menu->getLastInsertId(), $imagem, $_SESSION['Auth']['User']['User']['id']);
					}
				}
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));				
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The menu could not be saved. Please, try again.'));
			}
		}

		$icones = [" fa-angellist"," fa-area-chart"," fa-at"," fa-bell-slash"," fa-bell-slash-o"," fa-bicycle"," fa-binoculars"," fa-birthday-cake"," fa-bus"," fa-calculator"," fa-cc"," fa-cc-amex"," fa-cc-discover"," fa-cc-mastercard"," fa-cc-paypal"," fa-cc-stripe"," fa-cc-visa"," fa-copyright"," fa-eyedropper"," fa-futbol-o"," fa-google-wallet"," fa-ils"," fa-ioxhost"," fa-lastfm"," fa-lastfm-square"," fa-line-chart"," fa-meanpath"," fa-newspaper-o"," fa-paint-brush"," fa-paypal"," fa-pie-chart"," fa-plug"," fa-shekel (alias)"," fa-sheqel (alias)"," fa-slideshare"," fa-soccer-ball-o (alias)"," fa-toggle-off"," fa-toggle-on"," fa-trash"," fa-tty"," fa-twitch"," fa-wifi"," fa-yelp"," fa-adjust"," fa-anchor"," fa-archive"," fa-area-chart"," fa-arrows"," fa-arrows-h"," fa-arrows-v"," fa-asterisk"," fa-at"," fa-automobile "," fa-ban"," fa-bank "," fa-bar-chart"," fa-bar-chart-o "," fa-barcode"," fa-bars"," fa-beer"," fa-bell"," fa-bell-o"," fa-bell-slash"," fa-bell-slash-o"," fa-bicycle"," fa-binoculars"," fa-birthday-cake"," fa-bolt"," fa-bomb"," fa-book"," fa-bookmark"," fa-bookmark-o"," fa-briefcase"," fa-bug"," fa-building"," fa-building-o"," fa-bullhorn"," fa-bullseye"," fa-bus"," fa-cab "," fa-calculator"," fa-calendar"," fa-calendar-o"," fa-camera"," fa-camera-retro"," fa-car"," fa-caret-square-o-down"," fa-caret-square-o-left"," fa-caret-square-o-right"," fa-caret-square-o-up"," fa-cc"," fa-certificate"," fa-check"," fa-check-circle"," fa-check-circle-o"," fa-check-square"," fa-check-square-o"," fa-child"," fa-circle"," fa-circle-o"," fa-circle-o-notch"," fa-circle-thin"," fa-clock-o"," fa-close "," fa-cloud"," fa-cloud-download"," fa-cloud-upload"," fa-code"," fa-code-fork"," fa-coffee"," fa-cog"," fa-cogs"," fa-comment"," fa-comment-o"," fa-comments"," fa-comments-o"," fa-compass"," fa-copyright"," fa-credit-card"," fa-crop"," fa-crosshairs"," fa-cube"," fa-cubes"," fa-cutlery"," fa-dashboard "," fa-database"," fa-desktop"," fa-dot-circle-o"," fa-download"," fa-edit "," fa-ellipsis-h"," fa-ellipsis-v"," fa-envelope"," fa-envelope-o"," fa-envelope-square"," fa-eraser"," fa-exchange"," fa-exclamation"," fa-exclamation-circle"," fa-exclamation-triangle"," fa-external-link"," fa-external-link-square"," fa-eye"," fa-eye-slash"," fa-eyedropper"," fa-fax"," fa-female"," fa-fighter-jet"," fa-file-archive-o"," fa-file-audio-o"," fa-file-code-o"," fa-file-excel-o"," fa-file-image-o"," fa-file-movie-o "," fa-file-pdf-o"," fa-file-photo-o "," fa-file-picture-o "," fa-file-powerpoint-o"," fa-file-sound-o "," fa-file-video-o"," fa-file-word-o"," fa-file-zip-o "," fa-film"," fa-filter"," fa-fire"," fa-fire-extinguisher"," fa-flag"," fa-flag-checkered"," fa-flag-o"," fa-flash "," fa-flask"," fa-folder"," fa-folder-o"," fa-folder-open"," fa-folder-open-o"," fa-frown-o"," fa-futbol-o"," fa-gamepad"," fa-gavel"," fa-gear "," fa-gears "," fa-gift"," fa-glass"," fa-globe"," fa-graduation-cap"," fa-group "," fa-hdd-o"," fa-headphones"," fa-heart"," fa-heart-o"," fa-history"," fa-home"," fa-image "," fa-inbox"," fa-info"," fa-info-circle"," fa-institution "," fa-key"," fa-keyboard-o"," fa-language"," fa-laptop"," fa-leaf"," fa-legal "," fa-lemon-o"," fa-level-down"," fa-level-up"," fa-life-bouy "," fa-life-buoy "," fa-life-ring"," fa-life-saver "," fa-lightbulb-o"," fa-line-chart"," fa-location-arrow"," fa-lock"," fa-magic"," fa-magnet"," fa-mail-forward "," fa-mail-reply "," fa-mail-reply-all "," fa-male"," fa-map-marker"," fa-meh-o"," fa-microphone"," fa-microphone-slash"," fa-minus"," fa-minus-circle"," fa-minus-square"," fa-minus-square-o"," fa-mobile"," fa-mobile-phone "," fa-money"," fa-moon-o"," fa-mortar-board "," fa-music"," fa-navicon "," fa-newspaper-o"," fa-paint-brush"," fa-paper-plane"," fa-paper-plane-o"," fa-paw"," fa-pencil"," fa-pencil-square"," fa-pencil-square-o"," fa-phone"," fa-phone-square"," fa-photo "," fa-picture-o"," fa-pie-chart"," fa-plane"," fa-plug"," fa-plus"," fa-plus-circle"," fa-plus-square"," fa-plus-square-o"," fa-power-off"," fa-print"," fa-puzzle-piece"," fa-qrcode"," fa-question"," fa-question-circle"," fa-quote-left"," fa-quote-right"," fa-random"," fa-recycle"," fa-refresh"," fa-remove "," fa-reorder "," fa-reply"," fa-reply-all"," fa-retweet"," fa-road"," fa-rocket"," fa-rss"," fa-rss-square"," fa-search"," fa-search-minus"," fa-search-plus"," fa-send "," fa-send-o "," fa-share"," fa-share-alt"," fa-share-alt-square"," fa-share-square"," fa-share-square-o"," fa-shield"," fa-shopping-cart"," fa-sign-in"," fa-sign-out"," fa-signal"," fa-sitemap"," fa-sliders"," fa-smile-o"," fa-soccer-ball-o "," fa-sort"," fa-sort-alpha-asc"," fa-sort-alpha-desc"," fa-sort-amount-asc"," fa-sort-amount-desc"," fa-sort-asc"," fa-sort-desc"," fa-sort-down "," fa-sort-numeric-asc"," fa-sort-numeric-desc"," fa-sort-up "," fa-space-shuttle"," fa-spinner"," fa-spoon"," fa-square"," fa-square-o"," fa-star"," fa-star-half"," fa-star-half-empty "," fa-star-half-full "," fa-star-half-o"," fa-star-o"," fa-suitcase"," fa-sun-o"," fa-support "," fa-tablet"," fa-tachometer"," fa-tag"," fa-tags"," fa-tasks"," fa-taxi"," fa-terminal"," fa-thumb-tack"," fa-thumbs-down"," fa-thumbs-o-down"," fa-thumbs-o-up"," fa-thumbs-up"," fa-ticket"," fa-times"," fa-times-circle"," fa-times-circle-o"," fa-tint"," fa-toggle-down "," fa-toggle-left "," fa-toggle-off"," fa-toggle-on"," fa-toggle-right "," fa-toggle-up "," fa-trash"," fa-trash-o"," fa-tree"," fa-trophy"," fa-truck"," fa-tty"," fa-umbrella"," fa-university"," fa-unlock"," fa-unlock-alt"," fa-unsorted "," fa-upload"," fa-user"," fa-users"," fa-video-camera"," fa-volume-down"," fa-volume-off"," fa-volume-up"," fa-warning "," fa-wheelchair"," fa-wifi"," fa-wrench"," fa-file"," fa-file-archive-o"," fa-file-audio-o"," fa-file-code-o"," fa-file-excel-o"," fa-file-image-o"," fa-file-movie-o "," fa-file-o"," fa-file-pdf-o"," fa-file-photo-o "," fa-file-picture-o "," fa-file-powerpoint-o"," fa-file-sound-o "," fa-file-text"," fa-file-text-o"," fa-file-video-o"," fa-file-word-o"," fa-file-zip-o "," fa-circle-o-notch"," fa-cog"," fa-gear "," fa-refresh"," fa-spinner"," fa-check-square"," fa-check-square-o"," fa-circle"," fa-circle-o"," fa-dot-circle-o"," fa-minus-square"," fa-minus-square-o"," fa-plus-square"," fa-plus-square-o"," fa-square"," fa-square-o"," fa-cc-amex"," fa-cc-discover"," fa-cc-mastercard"," fa-cc-paypal"," fa-cc-stripe"," fa-cc-visa"," fa-credit-card"," fa-google-wallet"," fa-paypal"," fa-area-chart"," fa-bar-chart"," fa-bar-chart-o "," fa-line-chart"," fa-pie-chart"," fa-bitcoin "," fa-btc"," fa-cny "," fa-dollar "," fa-eur"," fa-euro "," fa-gbp"," fa-ils"," fa-inr"," fa-jpy"," fa-krw"," fa-money"," fa-rmb "," fa-rouble "," fa-rub"," fa-ruble "," fa-rupee "," fa-shekel "," fa-sheqel "," fa-try"," fa-turkish-lira "," fa-usd"," fa-won "," fa-yen "," fa-align-center"," fa-align-justify"," fa-align-left"," fa-align-right"," fa-bold"," fa-chain "," fa-chain-broken"," fa-clipboard"," fa-columns"," fa-copy "," fa-cut "," fa-dedent "," fa-eraser"," fa-file"," fa-file-o"," fa-file-text"," fa-file-text-o"," fa-files-o"," fa-floppy-o"," fa-font"," fa-header"," fa-indent"," fa-italic"," fa-link"," fa-list"," fa-list-alt"," fa-list-ol"," fa-list-ul"," fa-outdent"," fa-paperclip"," fa-paragraph"," fa-paste "," fa-repeat"," fa-rotate-left "," fa-rotate-right "," fa-save "," fa-scissors"," fa-strikethrough"," fa-subscript"," fa-superscript"," fa-table"," fa-text-height"," fa-text-width"," fa-th"," fa-th-large"," fa-th-list"," fa-underline"," fa-undo"," fa-unlink "," fa-angle-double-down"," fa-angle-double-left"," fa-angle-double-right"," fa-angle-double-up"," fa-angle-down"," fa-angle-left"," fa-angle-right"," fa-angle-up"," fa-arrow-circle-down"," fa-arrow-circle-left"," fa-arrow-circle-o-down"," fa-arrow-circle-o-left"," fa-arrow-circle-o-right"," fa-arrow-circle-o-up"," fa-arrow-circle-right"," fa-arrow-circle-up"," fa-arrow-down"," fa-arrow-left"," fa-arrow-right"," fa-arrow-up"," fa-arrows"," fa-arrows-alt"," fa-arrows-h"," fa-arrows-v"," fa-caret-down"," fa-caret-left"," fa-caret-right"," fa-caret-square-o-down"," fa-caret-square-o-left"," fa-caret-square-o-right"," fa-caret-square-o-up"," fa-caret-up"," fa-chevron-circle-down"," fa-chevron-circle-left"," fa-chevron-circle-right"," fa-chevron-circle-up"," fa-chevron-down"," fa-chevron-left"," fa-chevron-right"," fa-chevron-up"," fa-hand-o-down"," fa-hand-o-left"," fa-hand-o-right"," fa-hand-o-up"," fa-long-arrow-down"," fa-long-arrow-left"," fa-long-arrow-right"," fa-long-arrow-up"," fa-toggle-down "," fa-toggle-left "," fa-toggle-right "," fa-toggle-up "," fa-arrows-alt"," fa-backward"," fa-compress"," fa-eject"," fa-expand"," fa-fast-backward"," fa-fast-forward"," fa-forward"," fa-pause"," fa-play"," fa-play-circle"," fa-play-circle-o"," fa-step-backward"," fa-step-forward"," fa-stop"," fa-youtube-play"];
		$this->set('icones', $icones);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->css[] = '/js/jquery.icheck/skins/square/blue';			
		$this->css[] = '/js/fuelux/css/fuelux';
		$this->css[] = '/js/fuelux/css/fuelux-responsive.min';
		$this->css[] = '/js/colorpicker/css/colorpicker';
		$this->css[] = 'file-upload/upload_style';
		$this->css[] = 'file-upload/blueimp-gallery.min';
		$this->css[] = 'file-upload/jquery.fileupload';
		$this->css[] = 'file-upload/jquery.fileupload-ui';
		$this->css[] = '/js/jquery.niftymodals/css/component';	

		$this->js[] = 'jquery.icheck/icheck.min';
		$this->js[] = 'jquery.parsley/src/i18n/pt';
		$this->js[] = 'jquery.parsley/dist/parsley.min';
		$this->js[] = 'jquery.parsley/src/extra/dateiso';		
		$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
		$this->js[] = 'fuelux/loader';
		$this->js[] = 'colorpicker/js/colorpicker';
		$this->js[] = 'file-upload/jquery.ui.widget';
		$this->js[] = 'file-upload/tmpl.min';
		$this->js[] = 'file-upload/load-image.min';
		$this->js[] = 'file-upload/canvas-to-blob.min';
		$this->js[] = 'file-upload/jquery.blueimp-gallery.min';
		$this->js[] = 'file-upload/jquery.iframe-transport';
		$this->js[] = 'file-upload/jquery.fileupload';
		$this->js[] = 'file-upload/jquery.fileupload-process';
		$this->js[] = 'file-upload/jquery.fileupload-image';
		$this->js[] = 'file-upload/jquery.fileupload-validate';
		$this->js[] = 'file-upload/jquery.fileupload-ui';
		$this->js[] = 'jquery.niftymodals/js/jquery.modalEffects';	
		$this->js[] = 'lugati/menus/edit';

		if (!$this->Menu->exists($id)) {
			throw new NotFoundException(__('Invalid menu'));
		}
		if ($this->request->is(array('post', 'put'))) {

			if(!isset($this->request->data['idioma_pt']))
			{
				$this->request->data['idioma_pt'] = '';
			} 
			if(!isset($this->request->data['idioma_en'])){
				$this->request->data['idioma_en'] = '';
			}
			if(!isset($this->request->data['idioma_es'])){
				$this->request->data['idioma_es'] = '';
			}

			if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));				
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The menu could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Menu.' . $this->Menu->primaryKey => $id));
			$this->set('menus', $this->Menu->find('first', $options));
			$icones = [" fa-angellist"," fa-area-chart"," fa-at"," fa-bell-slash"," fa-bell-slash-o"," fa-bicycle"," fa-binoculars"," fa-birthday-cake"," fa-bus"," fa-calculator"," fa-cc"," fa-cc-amex"," fa-cc-discover"," fa-cc-mastercard"," fa-cc-paypal"," fa-cc-stripe"," fa-cc-visa"," fa-copyright"," fa-eyedropper"," fa-futbol-o"," fa-google-wallet"," fa-ils"," fa-ioxhost"," fa-lastfm"," fa-lastfm-square"," fa-line-chart"," fa-meanpath"," fa-newspaper-o"," fa-paint-brush"," fa-paypal"," fa-pie-chart"," fa-plug"," fa-shekel (alias)"," fa-sheqel (alias)"," fa-slideshare"," fa-soccer-ball-o (alias)"," fa-toggle-off"," fa-toggle-on"," fa-trash"," fa-tty"," fa-twitch"," fa-wifi"," fa-yelp"," fa-adjust"," fa-anchor"," fa-archive"," fa-area-chart"," fa-arrows"," fa-arrows-h"," fa-arrows-v"," fa-asterisk"," fa-at"," fa-automobile "," fa-ban"," fa-bank "," fa-bar-chart"," fa-bar-chart-o "," fa-barcode"," fa-bars"," fa-beer"," fa-bell"," fa-bell-o"," fa-bell-slash"," fa-bell-slash-o"," fa-bicycle"," fa-binoculars"," fa-birthday-cake"," fa-bolt"," fa-bomb"," fa-book"," fa-bookmark"," fa-bookmark-o"," fa-briefcase"," fa-bug"," fa-building"," fa-building-o"," fa-bullhorn"," fa-bullseye"," fa-bus"," fa-cab "," fa-calculator"," fa-calendar"," fa-calendar-o"," fa-camera"," fa-camera-retro"," fa-car"," fa-caret-square-o-down"," fa-caret-square-o-left"," fa-caret-square-o-right"," fa-caret-square-o-up"," fa-cc"," fa-certificate"," fa-check"," fa-check-circle"," fa-check-circle-o"," fa-check-square"," fa-check-square-o"," fa-child"," fa-circle"," fa-circle-o"," fa-circle-o-notch"," fa-circle-thin"," fa-clock-o"," fa-close "," fa-cloud"," fa-cloud-download"," fa-cloud-upload"," fa-code"," fa-code-fork"," fa-coffee"," fa-cog"," fa-cogs"," fa-comment"," fa-comment-o"," fa-comments"," fa-comments-o"," fa-compass"," fa-copyright"," fa-credit-card"," fa-crop"," fa-crosshairs"," fa-cube"," fa-cubes"," fa-cutlery"," fa-dashboard "," fa-database"," fa-desktop"," fa-dot-circle-o"," fa-download"," fa-edit "," fa-ellipsis-h"," fa-ellipsis-v"," fa-envelope"," fa-envelope-o"," fa-envelope-square"," fa-eraser"," fa-exchange"," fa-exclamation"," fa-exclamation-circle"," fa-exclamation-triangle"," fa-external-link"," fa-external-link-square"," fa-eye"," fa-eye-slash"," fa-eyedropper"," fa-fax"," fa-female"," fa-fighter-jet"," fa-file-archive-o"," fa-file-audio-o"," fa-file-code-o"," fa-file-excel-o"," fa-file-image-o"," fa-file-movie-o "," fa-file-pdf-o"," fa-file-photo-o "," fa-file-picture-o "," fa-file-powerpoint-o"," fa-file-sound-o "," fa-file-video-o"," fa-file-word-o"," fa-file-zip-o "," fa-film"," fa-filter"," fa-fire"," fa-fire-extinguisher"," fa-flag"," fa-flag-checkered"," fa-flag-o"," fa-flash "," fa-flask"," fa-folder"," fa-folder-o"," fa-folder-open"," fa-folder-open-o"," fa-frown-o"," fa-futbol-o"," fa-gamepad"," fa-gavel"," fa-gear "," fa-gears "," fa-gift"," fa-glass"," fa-globe"," fa-graduation-cap"," fa-group "," fa-hdd-o"," fa-headphones"," fa-heart"," fa-heart-o"," fa-history"," fa-home"," fa-image "," fa-inbox"," fa-info"," fa-info-circle"," fa-institution "," fa-key"," fa-keyboard-o"," fa-language"," fa-laptop"," fa-leaf"," fa-legal "," fa-lemon-o"," fa-level-down"," fa-level-up"," fa-life-bouy "," fa-life-buoy "," fa-life-ring"," fa-life-saver "," fa-lightbulb-o"," fa-line-chart"," fa-location-arrow"," fa-lock"," fa-magic"," fa-magnet"," fa-mail-forward "," fa-mail-reply "," fa-mail-reply-all "," fa-male"," fa-map-marker"," fa-meh-o"," fa-microphone"," fa-microphone-slash"," fa-minus"," fa-minus-circle"," fa-minus-square"," fa-minus-square-o"," fa-mobile"," fa-mobile-phone "," fa-money"," fa-moon-o"," fa-mortar-board "," fa-music"," fa-navicon "," fa-newspaper-o"," fa-paint-brush"," fa-paper-plane"," fa-paper-plane-o"," fa-paw"," fa-pencil"," fa-pencil-square"," fa-pencil-square-o"," fa-phone"," fa-phone-square"," fa-photo "," fa-picture-o"," fa-pie-chart"," fa-plane"," fa-plug"," fa-plus"," fa-plus-circle"," fa-plus-square"," fa-plus-square-o"," fa-power-off"," fa-print"," fa-puzzle-piece"," fa-qrcode"," fa-question"," fa-question-circle"," fa-quote-left"," fa-quote-right"," fa-random"," fa-recycle"," fa-refresh"," fa-remove "," fa-reorder "," fa-reply"," fa-reply-all"," fa-retweet"," fa-road"," fa-rocket"," fa-rss"," fa-rss-square"," fa-search"," fa-search-minus"," fa-search-plus"," fa-send "," fa-send-o "," fa-share"," fa-share-alt"," fa-share-alt-square"," fa-share-square"," fa-share-square-o"," fa-shield"," fa-shopping-cart"," fa-sign-in"," fa-sign-out"," fa-signal"," fa-sitemap"," fa-sliders"," fa-smile-o"," fa-soccer-ball-o "," fa-sort"," fa-sort-alpha-asc"," fa-sort-alpha-desc"," fa-sort-amount-asc"," fa-sort-amount-desc"," fa-sort-asc"," fa-sort-desc"," fa-sort-down "," fa-sort-numeric-asc"," fa-sort-numeric-desc"," fa-sort-up "," fa-space-shuttle"," fa-spinner"," fa-spoon"," fa-square"," fa-square-o"," fa-star"," fa-star-half"," fa-star-half-empty "," fa-star-half-full "," fa-star-half-o"," fa-star-o"," fa-suitcase"," fa-sun-o"," fa-support "," fa-tablet"," fa-tachometer"," fa-tag"," fa-tags"," fa-tasks"," fa-taxi"," fa-terminal"," fa-thumb-tack"," fa-thumbs-down"," fa-thumbs-o-down"," fa-thumbs-o-up"," fa-thumbs-up"," fa-ticket"," fa-times"," fa-times-circle"," fa-times-circle-o"," fa-tint"," fa-toggle-down "," fa-toggle-left "," fa-toggle-off"," fa-toggle-on"," fa-toggle-right "," fa-toggle-up "," fa-trash"," fa-trash-o"," fa-tree"," fa-trophy"," fa-truck"," fa-tty"," fa-umbrella"," fa-university"," fa-unlock"," fa-unlock-alt"," fa-unsorted "," fa-upload"," fa-user"," fa-users"," fa-video-camera"," fa-volume-down"," fa-volume-off"," fa-volume-up"," fa-warning "," fa-wheelchair"," fa-wifi"," fa-wrench"," fa-file"," fa-file-archive-o"," fa-file-audio-o"," fa-file-code-o"," fa-file-excel-o"," fa-file-image-o"," fa-file-movie-o "," fa-file-o"," fa-file-pdf-o"," fa-file-photo-o "," fa-file-picture-o "," fa-file-powerpoint-o"," fa-file-sound-o "," fa-file-text"," fa-file-text-o"," fa-file-video-o"," fa-file-word-o"," fa-file-zip-o "," fa-circle-o-notch"," fa-cog"," fa-gear "," fa-refresh"," fa-spinner"," fa-check-square"," fa-check-square-o"," fa-circle"," fa-circle-o"," fa-dot-circle-o"," fa-minus-square"," fa-minus-square-o"," fa-plus-square"," fa-plus-square-o"," fa-square"," fa-square-o"," fa-cc-amex"," fa-cc-discover"," fa-cc-mastercard"," fa-cc-paypal"," fa-cc-stripe"," fa-cc-visa"," fa-credit-card"," fa-google-wallet"," fa-paypal"," fa-area-chart"," fa-bar-chart"," fa-bar-chart-o "," fa-line-chart"," fa-pie-chart"," fa-bitcoin "," fa-btc"," fa-cny "," fa-dollar "," fa-eur"," fa-euro "," fa-gbp"," fa-ils"," fa-inr"," fa-jpy"," fa-krw"," fa-money"," fa-rmb "," fa-rouble "," fa-rub"," fa-ruble "," fa-rupee "," fa-shekel "," fa-sheqel "," fa-try"," fa-turkish-lira "," fa-usd"," fa-won "," fa-yen "," fa-align-center"," fa-align-justify"," fa-align-left"," fa-align-right"," fa-bold"," fa-chain "," fa-chain-broken"," fa-clipboard"," fa-columns"," fa-copy "," fa-cut "," fa-dedent "," fa-eraser"," fa-file"," fa-file-o"," fa-file-text"," fa-file-text-o"," fa-files-o"," fa-floppy-o"," fa-font"," fa-header"," fa-indent"," fa-italic"," fa-link"," fa-list"," fa-list-alt"," fa-list-ol"," fa-list-ul"," fa-outdent"," fa-paperclip"," fa-paragraph"," fa-paste "," fa-repeat"," fa-rotate-left "," fa-rotate-right "," fa-save "," fa-scissors"," fa-strikethrough"," fa-subscript"," fa-superscript"," fa-table"," fa-text-height"," fa-text-width"," fa-th"," fa-th-large"," fa-th-list"," fa-underline"," fa-undo"," fa-unlink "," fa-angle-double-down"," fa-angle-double-left"," fa-angle-double-right"," fa-angle-double-up"," fa-angle-down"," fa-angle-left"," fa-angle-right"," fa-angle-up"," fa-arrow-circle-down"," fa-arrow-circle-left"," fa-arrow-circle-o-down"," fa-arrow-circle-o-left"," fa-arrow-circle-o-right"," fa-arrow-circle-o-up"," fa-arrow-circle-right"," fa-arrow-circle-up"," fa-arrow-down"," fa-arrow-left"," fa-arrow-right"," fa-arrow-up"," fa-arrows"," fa-arrows-alt"," fa-arrows-h"," fa-arrows-v"," fa-caret-down"," fa-caret-left"," fa-caret-right"," fa-caret-square-o-down"," fa-caret-square-o-left"," fa-caret-square-o-right"," fa-caret-square-o-up"," fa-caret-up"," fa-chevron-circle-down"," fa-chevron-circle-left"," fa-chevron-circle-right"," fa-chevron-circle-up"," fa-chevron-down"," fa-chevron-left"," fa-chevron-right"," fa-chevron-up"," fa-hand-o-down"," fa-hand-o-left"," fa-hand-o-right"," fa-hand-o-up"," fa-long-arrow-down"," fa-long-arrow-left"," fa-long-arrow-right"," fa-long-arrow-up"," fa-toggle-down "," fa-toggle-left "," fa-toggle-right "," fa-toggle-up "," fa-arrows-alt"," fa-backward"," fa-compress"," fa-eject"," fa-expand"," fa-fast-backward"," fa-fast-forward"," fa-forward"," fa-pause"," fa-play"," fa-play-circle"," fa-play-circle-o"," fa-step-backward"," fa-step-forward"," fa-stop"," fa-youtube-play"];
			$this->set('icones', $icones);
		}
	}


	public function status($id, $status) {

		$this->autoRender = false;

		$data['id'] = $id;
		$data['status'] = $status;

		if($this->Menu->save($data)) {
			if($status == 1) {
				$return['title'] = 'Sucesso';
				$return['text'] = 'Menu ativo!';
				$return['class_name'] = 'success';
			}else{
				$return['title'] = 'Sucesso';
				$return['text'] = 'Menu inativo!';
				$return['class_name'] = 'dark';
			}			
		}else{
			$return['title'] = 'Erro';
			$return['text'] = 'Tente mais tarde!';
			$return['class_name'] = 'red';
		}

		echo json_encode($return);
		
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Menu->id = $id;
		if (!$this->Menu->exists()) {
			throw new NotFoundException(__('Invalid menu'));
		}

		if ($this->Menu->delete()) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));			
		} else {
			$this->Session->setFlash(__('The menu could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function order(){
		$this->autoRender = false;
		$ids = json_decode($_POST['id']);

		foreach ($ids as $i => $id) {
			$data = array();
			$data['id'] = $id;
			$data['ordem'] = $i+1;
			$this->Menu->saveAll($data);
		}
		$this->Menu->recursive = 0;
		echo json_encode($this->Menu->find('all', array('order' => 'ordem ASC')));
	}

	public function uploadHandler(){

		$this->autoRender = false;

		$upload_handler = new UploadHandler();

	}

	public function uploadHandlerEdit($id){

		$this->autoRender = false;
		
		$options=array(
			'upload_dir' => dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/menus/'.$id.'/',
			'upload_url' => $_SERVER['SERVER_NAME'].$this->config_cidade.'/uploads/menus/'.$id.'/'
		);

		$upload_handler = new UploadHandler($options);

	}
}
