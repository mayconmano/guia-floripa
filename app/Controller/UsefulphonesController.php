<?php
App::uses('AppController', 'Controller');
/**
 * Usefulphones Controller
 *
 * @property Usefulphone $Usefulphone
 * @property PaginatorComponent $Paginator
 */
class UsefulphonesController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		$this->css[] = '/js/jquery.datatables/bootstrap-adapter/css/datatables';

		$this->js[] = 'jquery.datatables/jquery.datatables.min';
		$this->js[] = 'jquery.datatables/bootstrap-adapter/js/datatables';
		$this->js[] = 'lugati/usefulphones/index';
		
		$this->Usefulphone->recursive = 0;
		$options = array('conditions' => array('Usefulphone.status <> 3'));
		$this->set('usefulphones', $this->Usefulphone->find('all', $options));

	}

	public function add() {

		$this->css[] = '/js/jquery.icheck/skins/square/blue';

		$this->js[] = 'jquery.icheck/icheck.min';
		$this->js[] = 'jquery.maskedinput/jquery.maskedinput';

		if ($this->request->is('post')) {
			if($this->request->data['nome_local_pt'] != '')
			{
				$this->request->data['idioma_pt'] = 'pt';
			}
			if($this->request->data['nome_local_en'] != '')
			{
				$this->request->data['idioma_en'] = 'en';	
			}
			if($this->request->data['nome_local_es'] != '')
			{
				$this->request->data['idioma_es'] = 'es';		
			}
						
			$this->Usefulphone->create();
			if ($this->Usefulphone->save($this->request->data)) {				
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The usefulphone could not be saved. Please, try again.'));
			}
		}
	}

	public function edit($id) {

		$this->css[] = '/js/jquery.icheck/skins/square/blue';

		$this->js[] = 'jquery.icheck/icheck.min';
		$this->js[] = 'jquery.maskedinput/jquery.maskedinput';

		if ($this->request->is(array('post', 'put'))) {			
			if ($this->Usefulphone->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The usefulphone could not be saved. Please, try again.'));
			}
		}else{

			$options = array('conditions' => array('Usefulphone.' . $this->Usefulphone->primaryKey => $id));
			$this->set('usefulphones', $this->Usefulphone->find('first', $options));
						
		}

	}

	public function delete($id = null) {
		$this->Usefulphone->id = $id;
		if (!$this->Usefulphone->exists()) {
			throw new NotFoundException(__('Invalid Usefulphone'));
		}
				
		if ($this->Usefulphone->delete()) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
		} else {
			$this->Session->setFlash(__('The Usefulphone could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function status($id, $status) {

		$this->autoRender = false;

		$data['id'] = $id;
		$data['status'] = $status;

		if($this->Usefulphone->save($data)) {
			if($status == 1) {
				$return['title'] = 'Sucesso';
				$return['text'] = 'Telefone ativo!';
				$return['class_name'] = 'success';
			}else{
				$return['title'] = 'Sucesso';
				$return['text'] = 'Telefone inativo!';
				$return['class_name'] = 'dark';
			}			
		}else{
			$return['title'] = 'Erro';
			$return['text'] = 'Tente mais tarde!';
			$return['class_name'] = 'red';
		}

		echo json_encode($return);
		
	}

}
