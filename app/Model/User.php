<?php
App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');
/**
 * User Model
 *
 * @property Secretary $Secretary
 */
class User extends AppModel {

	public function beforeSave($options = array()) {
	    if (isset($this->data[$this->alias]['senha'])) {
	        $this->data[$this->alias]['senha'] = AuthComponent::password($this->data[$this->alias]['senha']);
	    }
	    return true;
	}

}
