<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

	/*public function afterSave($created, $options = array()){
		$notTabel = array('answers', 'poies_categories');
		if(!in_array($this->useTable, $notTabel)){
			$usuario_id = $_SESSION['Auth']['User']['User']['id'];		
			if($created){
				$this->query('INSERT INTO logs (tabela, tabela_id, usuario_id, data, tipo_log, status) VALUES ("'.$this->useTable.'", "'.$this->id.'", "'.$usuario_id.'" , "'.date('Y-m-d H:i:s').'" , 1, 1)');
			}else{
				$this->query('INSERT INTO logs (tabela, tabela_id, usuario_id, data, tipo_log, status) VALUES ("'.$this->useTable.'", "'.$this->id.'", "'.$usuario_id.'" , "'.date('Y-m-d H:i:s').'" , 1, 2)');
			}
		}		
	}

	public function delete($id = NULL, $cascade = true){		
		$this->query('UPDATE '.$this->useTable.' SET status = 3 WHERE id = '.$this->id.' ');
		$usuario_id = $_SESSION['Auth']['User']['User']['id'];		
	    $this->query('INSERT INTO logs (tabela, tabela_id, usuario_id, data, tipo_log, status) VALUES ("'.$this->useTable.'", "'.$this->id.'", "'.$usuario_id.'" , "'.date('Y-m-d H:i:s').'" , 1, 3)');
		return true;
	}*/

}
