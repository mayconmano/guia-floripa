<?php
App::uses('AppModel', 'Model');
/**
 * Top Model
 *
 * @property Poie $Poie
 */
class Top extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Poie' => array(
			'className' => 'Poie',
			'foreignKey' => 'poie_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
