<?php
App::uses('AppModel', 'Model');
/**
 * Agenda Model
 *
 * @property News $News
 */
class Agenda extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'News' => array(
			'className' => 'News',
			'foreignKey' => 'news_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
